module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  snapshotSerializers: ["enzyme-to-json/serializer"],
  setupTestFrameworkScriptFile: "<rootDir>/test/setupTests.ts",
  automock: false,
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
  moduleNameMapper: {
    "\\.(css|scss)$": "identity-obj-proxy"
  },
  setupFiles: [
    "<rootDir>/setupJest.js"
  ],
  roots: [
    "<rootDir>/test"
  ],
  testResultsProcessor: "jest-bamboo-formatter",
  transform: {
    "^.+\\.tsx": "<rootDir>/node_modules/react-native/jest/preprocessor.js"
  },
  verbose: false
};
