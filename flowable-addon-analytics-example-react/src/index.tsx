import * as React from "react";
import ReactDOM from "react-dom";
import Standalone from "./Standalone";
import Forms from "./Forms";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(<Standalone/>, document.getElementById("standalone"));
ReactDOM.render(<Forms/>, document.getElementById("forms"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
