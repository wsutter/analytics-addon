import * as React from "react";
import { Component } from "react";
import "./Standalone.css";
import { PieChart } from "@flowable/addon-analytics-react/pieChart";
import "whatwg-fetch";

let pieChartDefinition = {
  description: "simple pie chart",
  extraSettings: {
    transition: {
      isEnabled: false
    },
    caption: {
      mapping: { items: [{ level: 0, mapping: "category" }] }
    },
    chartContainerType: "plain-chart-container",
    chartType: "pie-chart",
    dataResources: {
      items: [
        {
          level: 0,
          dataResource: {
            isEnabled: true,
            mode: "cors",
            queryUrl: "pieCharts.json",
            type: "rest"
          }
        },
        {
          level: 0,
          dataResource: {
            isEnabled: true,
            mode: "cors",
            queryUrl: "pieCharts2.json",
            type: "rest"
          }
        }
      ]
    },
    id: "simplePieChart",
    innerRadius: 0.5,
    legend: {
      mapping: { items: [{ level: 0, mapping: "caption" }] }
    },
    outerRadius: 4,
    ticks: { textAnchor: "start" },
    userResource: { mode: "cors", queryUrl: "currentUser.json" },
    value: {
      angle: 0,
      baseLine: "central",
      dateFormat: "%Y-%m-%d",
      dateParser: "%d.%m.%Y",
      domain: { items: [{ domain: "numeric", level: 0 }] },
      font: {
        color: "#000000",
        customColorEnabled: true,
        family: "'Helvetica Neue',Helvetica,Arial,sans-serif",
        size: 18
      },
      isVisible: true,
      mapping: { items: [{ level: 0, mapping: "amount" }] },
      numberFormat: ".0f",
      position: "top",
      textAnchor: "start",
      timeFormat: "%H:%M:%S.%L",
      usePercentage: false
    }
  },
  id: "simplePieChart",
  label: "simple",
  type: "pieChart",
  value: "{{simplePieChart}}"
};

export class Standalone extends Component {
  render() {
    return (
      <div className="Standalone" data-test-wrapper="pieChart">
        <h1>If you can see a simple pie chart with a legend below, the build "dist-react" build works fine standalone!</h1>
        <PieChart
          config={pieChartDefinition}
        />
      </div>
    );
  }
}

export default Standalone;
