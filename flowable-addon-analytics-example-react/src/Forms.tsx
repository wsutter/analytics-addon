import React, { Component } from "react";
import "./Forms.css";
import pieChart from "@flowable/addon-analytics-react/pieChart";
import { FormControlled } from "@flowable/forms-react";
import "whatwg-fetch";

let formLayout = {
  rows: [
    {
      cols: [
        {
          description: "simple pie chart",
          extraSettings: {
            transition: {
              isEnabled: false
            },
            caption: {
              mapping: { items: [{ level: 0, mapping: "category" }] }
            },
            chartContainerType: "plain-chart-container",
            chartType: "pie-chart",
            dataResources: {
              items: [
                {
                  level: 0,
                  dataResource: {
                    isEnabled: true,
                    mode: "cors",
                    queryUrl: "pieCharts.json",
                    type: "rest"
                  }
                },
                {
                  level: 0,
                  dataResource: {
                    isEnabled: true,
                    mode: "cors",
                    queryUrl: "pieCharts2.json",
                    type: "rest"
                  }
                }
              ]
            },
            id: "simplePieChart",
            innerRadius: 0.5,
            legend: {
              mapping: { items: [{ level: 0, mapping: "caption" }] }
            },
            outerRadius: 4,
            ticks: { textAnchor: "start" },
            userResource: { mode: "cors", queryUrl: "currentUser.json" },
            value: {
              angle: 0,
              baseLine: "central",
              dateFormat: "%Y-%m-%d",
              dateParser: "%d.%m.%Y",
              domain: { items: [{ domain: "numeric", level: 0 }] },
              font: {
                color: "#000000",
                customColorEnabled: true,
                family: "'Helvetica Neue',Helvetica,Arial,sans-serif",
                size: 18
              },
              isVisible: true,
              mapping: { items: [{ level: 0, mapping: "amount" }] },
              numberFormat: ".0f",
              position: "top",
              textAnchor: "start",
              timeFormat: "%H:%M:%S.%L",
              usePercentage: false
            }
          },
          id: "simplePieChart",
          label: "simple",
          type: "pieChart",
          value: "{{simplePieChart}}"
        }
      ]
    }
  ]
};

class Forms extends Component {
  render() {
    return (
      <div className="Forms">
        <h1>If you can see a simple pie chart with a legend below, the build "dist-react" build works fine with Flowable Forms!</h1>
        <FormControlled
          config={formLayout}
          Components={pieChart}
        />
      </div>
    );
  }
}

export default Forms;
