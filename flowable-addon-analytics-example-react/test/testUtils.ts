import { ShallowWrapper } from "enzyme";

/**
 * Returns ShallowWrapper containing the node(s) with the given 'data-test-<...>' attribute.
 *
 * @param wrapper {ShallowWrapper}
 * @param element {string} - the DOM node name
 * @param dataTestAttribute {string} - name of the 'data-test-<...>' attribute, e.g. if looking for 'data-test-foo', pass 'foo' into this function
 * @param dataTestValue {string} - the value of the data attribute
 */
export const findDataProperty: any = (
  wrapper: ShallowWrapper,
  element: string,
  dataTestAttribute: string,
  dataTestValue: string
) => {
  return wrapper.find(`${element}[data-test-${dataTestAttribute}="${dataTestValue}"]`);
};
