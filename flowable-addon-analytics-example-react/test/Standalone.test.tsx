import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import { Standalone } from "../src/Standalone";
import { findDataProperty } from "./testUtils";

describe("Standalone component", () => {

  it("should render a Standalone component without errors", () => {
    shallow(<Standalone/>);
  });

  it("should have a wrapper DOM element with data attribute 'data-test-wrapper=pieChart'", () => {
    let wrapper = shallow(<Standalone/>);
    let dom: ShallowWrapper = findDataProperty(wrapper, "div", "wrapper", "pieChart");
    expect(dom.length).toBe(1);
  });

  it("should have two DOM elements of type h1 and PieChartFactory resp. within the wrapper", () => {
    let wrapper = shallow(<Standalone/>);
    let dom: ShallowWrapper = findDataProperty(wrapper, "div", "wrapper", "pieChart");
    expect(dom.find("h1").length).toBe(1);
    expect(dom.find("PieChartFactory").length).toBe(1);
  });

});