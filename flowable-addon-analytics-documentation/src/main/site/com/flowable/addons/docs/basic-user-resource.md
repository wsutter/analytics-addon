---
id: basic-user-resource
title: User resources
---

Flowable Addon Analytics is capable of formatting any values that are to be displayed within the chart, may it be captions as ticks on an axis or values within tooltips.
In most cases, you want to automatically format those values according to the locale of your user.

<a name="example"></a>
## Example user resource

Throughout this documentation, we are going to use the subsequent user source.
This example shows you the bare minimum needed, so that charts can be rendered. It stands to reason that this object can be enriched with arbitrary properties.

```json
{
  "language": "en",
  "locale": "en-US"
}
```

As you can see, this data source is of content type ``application/json; charset=utf-8``.

<a name="properties"></a>
## Properties

The following table aims to give you an idea on the usage of each property you would have to map to your data source so that it can be used by Flowable Addon Analytics.

Please bear in mind that only properties that are marked as \***propertyName** are mandatory.

| Name              | Description
| ---               | ---
| \***language**    | The language used by the user 
| \***locale**      | The locale used by the user

A list of supported languages / locales can be found [here](https://github.com/d3/d3-format/tree/master/locale) or [here](https://github.com/d3/d3-time-format/tree/master/locale), resp.

## Example use cases

Please refer to the [user resource use cases](usecases-user-resources.md) to see some examples of the user resource.