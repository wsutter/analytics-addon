---
id: guides-usage
title: Usage
---
Depending on the package of Flowable Addon Analytics you chose, using it is slightly different.

For each possible integration, you can find examples in the [Git repository](https://stash.edorasware.com/scm/solutions/flowable-addon-analytics.git).
Check out modules ```flowable-addon-analytics-example-react``` and ```flowable-addon-analytics-example-vanilla```.

## Using react build (@flowable/addon-analytics-react)
Since you have installed the react build with

```
yarn add @flowable/analytics-addon-react
```

the javascript files are available at ```node_modules/@flowable/addon-analytics-react``` and can be imported into your project.

In addition, you will have to have ```react``` as well as ```react-dom````installed.
You would have to import React together with one of the options below.

```javascript
import React from "react";
```

### Usage standalone

#### import of single charts

##### area charts
```typescript jsx
import { AreaChart } from "@flowable/addon-analytics-react/areaChart";
```

##### bar charts
```typescript jsx
import { BarChart } from "@flowable/addon-analytics-react/barChart";
```

##### gauge charts
```typescript jsx
import { GaugeChart } from "@flowable/addon-analytics-react/gaugeChart";
```

##### heatmap charts
```typescript jsx
import { HeatmapChart } from "@flowable/addon-analytics-react/heatmapChart";
```

##### line charts
```typescript jsx
import { LineChart } from "@flowable/addon-analytics-react/lineChart";
```

##### pie charts
```typescript jsx
import { PieChart } from "@flowable/addon-analytics-react/pieChart";
```

##### radar charts
```typescript jsx
import { RadarChart } from "@flowable/addon-analytics-react/radarChart";
```

##### scatter charts
```typescript jsx
import { ScatterChart } from "@flowable/addon-analytics-react/scatterChart";
```

#### Example

If you e.g. would like to implement a pie chart in a simple App component, you can use this code snippet:

```typescript jsx
import React from "react";
import { PieChart } from "@flowable/addon-analytics-react/pieChart"
class App extends React.Component {
  render() {
    return (
      <div className="App">
        <PieChart
          config={{
            // the definition of the chart
          }}
          additionalData={{
            // optional: any functions you need in your form definition, e.g.
            formatTime: date => date.toLocaleTimeString().substr(0, 5)
          }}
          payload={{
            // optional: the initial state of the chart(s), e.g.
            simplePieChart: { // ID of the chart!
              shouldUpdate: false,
              currentLevel: 0,
              drillings: [],
              selection: {}
            }
          }}
        />
      </div>
    );
  }
}
```

### Usage with Flowable Forms

#### import of all charts
```typescript jsx
import * as Analytics from "@flowable/addon-analytics-react";
```
**Caution:** This will pull in a very large file that might cause performance issues! We recommend using single charts only.

#### import of single charts

**Please note that you must not wrap the name of the chart in brackets as with the standalone import!**

Correct:
```typescript jsx
import PieChart from "@flowable/addon-analytics-react/areaChart";
```

**Incorrect** when integrating into Flowable Forms:
```typescript jsx
import { PieChart } from "@flowable/addon-analytics-react/areaChart";
```

##### area charts
```typescript jsx
import AreaChart from "@flowable/addon-analytics-react/areaChart";
```

##### bar charts
```typescript jsx
import BarChart from "@flowable/addon-analytics-react/barChart";
```

##### gauge charts
```typescript jsx
import GaugeChart from "@flowable/addon-analytics-react/gaugeChart";
```

##### heatmap charts
```typescript jsx
import HeatmapChart from "@flowable/addon-analytics-react/heatmapChart";
```

##### line charts
```javascript
import LineChart from "@flowable/addon-analytics-react/lineChart";
```

##### pie charts
```typescript jsx
import PieChart from "@flowable/addon-analytics-react/pieChart";
```

##### radar charts
```typescript jsx
import RadarChart from "@flowable/addon-analytics-react/radarChart";
```

##### scatter charts
```typescript jsx
import ScatterChart from "@flowable/addon-analytics-react/scatterChart";
```

#### Example

```typescript jsx
import React from "react";
import PieChart from "@flowable/addon-analytics-react/pieChart"
import { FormControlled } from "@flowable/forms-react";
class App extends React.Component {
  render() {
    return (
      <div>
        <FormControlled
          config={{
            // the definition of the form
          }}
          additionalData={{
            // optional: any functions you need in your form definition, e.g.
            formatTime: date => date.toLocaleTimeString().substr(0, 5)
          }}
          payload={{
            // optional: the initial state of the chart(s), e.g.
            simplePieChart: { // ID of the chart!
              shouldUpdate: false,
              currentLevel: 0,
              drillings: [],
              selection: {}
            }
          }}
          Components={PieChart}
        />
      </div>
    );
  }
}
```

## Using vanilla build (@flowable/addon-analytics-vanilla)

Since you have installed the vanilla build with

```
yarn add @flowable/analytics-addon-vanilla
```

the javascript files are available at ```node_modules/@flowable/addon-analytics-vanilla```. You will have to either manually or automatically copy **all the 
javascript files incl. all sub folders** to your distribution root folder e.g. by means of [webpack](https://webpack.js.org).

In addition to the script tags mentioned below, you will have to download and install bundled versions of ```react``` as well as ```react-dom```.
```html
  <script src="/react.production.min.js"></script>
  <script src="/react-dom.production.min.js"></script>
```

Alternatively, you can use the libraries available at ```cdnjs.cloudflare.com```.
```html
  <script src="https://cdnjs.cloudflare.com/ajax/libs/react/16.6.3/umd/react.production.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/react-dom/16.6.3/umd/react-dom.production.min.js"></script>
```

### Usage standalone

#### Import of all charts
Copy ```flwAddonAnalytics.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/flwAddonAnalytics.js" type="text/javascript"></script>
```

**Caution:** This is a very large file that might cause performance issues! We recommend using single charts only.

#### Import of single charts

#### Area charts
Copy ```areaChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/areaChart/index.js" type="text/javascript"></script>
```

#### Bar charts
Copy ```barChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/barChart/index.js" type="text/javascript"></script>
```

#### Gauge charts
Copy ```gaugeChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/gaugeChart/index.js" type="text/javascript"></script>
```

#### Heatmap charts
Copy ```heatmapChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/heatmapChart/index.js" type="text/javascript"></script>
```

#### Line charts
Copy ```lineChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/lineChart/index.js" type="text/javascript"></script>
```

#### Pie charts
Copy ```pieChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/pieChart/index.js" type="text/javascript"></script>
```

#### Radar charts
Copy ```radarChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/radarChart/index.js" type="text/javascript"></script>
```

#### Scatter charts
Copy ```scatterChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/scatterChart/index.js" type="text/javascript"></script>
```

#### Example

If you e.g. would like to implement a pie chart, you can use this code snippet:

```javascript
const chart = React.createElement(
  flwAddonAnalytics.PieChart,
  {
    config: {
      // the definition of the chart
    },
    additionalData: {
      // optional: any functions you need in your form definition, e.g.
      formatTime: date => date.toLocaleTimeString().substr(0, 5)
    },
    payload: {
      // optional: the initial state of the chart(s), e.g.
      simplePieChart: { // ID of the chart!
        shouldUpdate: false,
        currentLevel: 0,
        drillings: [],
        selection: {}
      }
    }
  }
);
ReactDOM.render(
  chart,
  document.getElementById("id-of-the-output-element")
);
```

```React```, ```ReactDOM``` as well as ```flwAddonAnalytics``` are available globally.

### Usage with Flowable Forms

#### Import of all charts
Copy ```flwAddonAnalytics.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/flwAddonAnalytics.js" type="text/javascript"></script>
```

**Caution:** This is a very large file that might cause performance issues! We recommend using single charts only.

#### Import of single charts

#### Area charts
Copy ```areaChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/areaChart/index.js" type="text/javascript"></script>
```

#### Bar charts
Copy ```barChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/barChart/index.js" type="text/javascript"></script>
```

#### Gauge charts
Copy ```gaugeChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/gaugeChart/index.js" type="text/javascript"></script>
```

#### Heatmap charts
Copy ```heatmapChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/heatmapChart/index.js" type="text/javascript"></script>
```

#### Line charts
Copy ```lineChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/lineChart/index.js" type="text/javascript"></script>
```

#### Pie charts
Copy ```pieChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/pieChart/index.js" type="text/javascript"></script>
```

#### Radar charts
Copy ```radarChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/radarChart/index.js" type="text/javascript"></script>
```

#### Scatter charts
Copy ```scatterChart/index.js``` to the root folder, then copy the subsequent script tag into your main HTML's header.

```html
<script src="/scatterChart/index.js" type="text/javascript"></script>
```

#### Example

If you e.g. would like to implement a pie chart, you can use this code snippet:

```javascript
flwformsVanilla.render(
  document.getElementById("id-of-the-output-element"),
  {
    config: {
      // the definition of the form incl. one or multiple chart definitions
    }, 
    additionalData: { 
      // optional: any functions you need in your form definition, e.g.
      formatTime: date => date.toLocaleTimeString().substr(0, 5)
    },
    payload: {
      // optional: the initial state of the chart(s), e.g.
      simplePieChart: { // ID of the chart!
        shouldUpdate: false,
        currentLevel: 0,
        drillings: [],
        selection: {}
      }
    },
    Components: flwAddonAnalytics.default
    // any other props for Forms which are, however, irrelevant for Analytics
  }
);
```

```flwformsVanilla``` as well as ```flwAddonAnalytics``` are available globally.
