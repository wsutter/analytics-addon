---
id: basic-chart-definition
title: Chart definition
---

Flowable Addon Analytics provides you with an abundance of properties which allow you to create fairly intricate charts. The subsequent sections 
list all [properties](#properties) available.

Please bear in mind that only properties that are marked as \***propertyName** are mandatory. All the other properties are optional and if omitted, the pre-defined 
default value will be taken.

## Sample simple charts
Please refer to `simple` charts in the storybook if you would like to see samples of simple chart definitions of each respective chart.

* [Area charts](https://analytics-addon-storybook.flowable.i/index.html?selectedKind=3%29%20charts%2Farea%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybook%2Fnotes%2Fpanel)
* [Bar charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fbar%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Gauge charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fgauge%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Heatmap charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fheatmap%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Line charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fline%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Pie charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fpie%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Radar charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fradar%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Scatter charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fscatter%20charts&selectedStory=simple&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)

## Sample fully fledged charts
Please refer to `fully fledged` charts in the storybook if you would like to see samples of fully fledged chart definitions of each respective chart.

* [Area charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Farea%20charts&selectedStory=fully%20fledged&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Bar charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fbar%20charts&selectedStory=fully%20fledged&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Gauge charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fgauge%20charts&selectedStory=fully%20fledged&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Line charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fline%20charts&selectedStory=fully%20fledged&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Pie charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fpie%20charts&selectedStory=fully%20fledged&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Radar charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fradar%20charts&selectedStory=fully%20fledged&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)
* [Scatter charts](https://analytics-addon-storybook.flowable.io/?selectedKind=3%29%20charts%2Fscatter%20charts&selectedStory=fully%20fledged&full=0&addons=1&stories=1&panelRight=0&addonPanel=REACT_STORYBOOK%2Freadme%2Fpanel)

## Glossary
**Please make yourself familiar with the vocabulary used within the documentation before diving deep into it**

| Name                                              | Description
| ---                                               | ---
| <a name="glossaryDatum">datum, item</a>           | refers to a single object in the array retrieved from the data resource; in general the `datum` is used throughout the documentation which is a.k.a. `item`
| <a name="glossaryAggregations">aggregations</a>   | refers to any child datums of the datum in context (whose type is Array\<DatumInterface\>)
| <a name="glossaryLevel">level, currentLevel</a>   | refers to the highest shown level (base level) of the shown chart
| <a name="glossaryItemsArray">items array</a>      | an array of arbitrary objects (datums) that is assigned to a property called `items`
| <a name="glossaryElements">elements</a>           | a generic term to describe a graphical element such as a bar, an arc, a pie, a line and the like, depending on the respective chart
| <a name="glossaryTransition">transition</a>       | refers to any algorithm that is used for animations
| <a name="glossaryMappedObject">mapped object</a>  | a mapped object is an object that can be mapped to e.g. an axis as in the `caption` property is by default mapped to the x-axis; mappable objects are: `caption`, `value`, `aggregations`, `group`, `weight`, `colors`
| <a name="glossaryDomain">domain</a>               | refers to the type of data a specific property holds which is especially important if you would like to automatically format properties; domains are `string`, `numeric`, `timestamp`, `date`, `time`, `dateTime`
| <a name="glossaryScale">scale, scale type</a>     | defines how data is mapped onto the chart; for details, see <a href="https://github.com/d3/d3-scale">d3-scale</a>

<a name="properties"></a>
## Properties

Properties marked as \***propertyName** are mandatory. Almost all properties have reasonable default values, so that you only have to define the mandatory properties manually.

<a name="aggregations"></a>
### aggregations

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| \***mapping**         | Any property of your datums. Defaults to `aggregations`.                      | +             | +             | +             | +             | +             | +             | +

<a name="alignment"></a>
### alignment

Either `horizontal` or `vertical`. Currently, only `horizontal` is supported.

<!---
TODO: Support vertical charts
-->

<a name="axes"></a>
### axes

This is a map of objects that all hold the following properties:

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| \***mapping**         | Any [mapped object](#glossaryMappedObject)                                    | +             | +             | +             | +             | -             | +             | +
| domain                | An [items array](#glossaryItemsArray) that holds a level-based map of [domains](#glossaryDomain); Defaults to the domain of the mapped object; See example below | + | + | + | +  | + | + | +
| logBase               | Any number which serves as the logarithmic base for numeric scales            | -             | -             | -             | -             | -             | -             | -
| powerExponent         | Any number which serves as the power exponent for numeric scales              | -             | -             | -             | -             | -             | -             | -
| orient                | The orient of the axis; any of `top`, `left`, `bottom` and `right`            | +             | +             | +             | -             | -             | +             | +
| ticks                 | See separate section [ticks](#ticks)                                          | +             | +             | +             | -             | -             | +             | +
| referenceLines        | See separate section [reference lines](#referenceLines)                       | +             | +             | +             | -             | -             | +             | +
| lowestValuePosition   | The position of the lowest value; any of `top`, `left`, `bottom` and `right`  | +             | +             | +             | -             | -             | +             | +
| scaleType             | The [scale type](#glossaryScale) used for this axis; any of `band`, `linear`, `time`, `log`, `power`, `point`, `category`; defaults to `linear` | + | + | + | - | - | +       | + 
| titles                | See separate section [titles](#titles)                                        | +             | +             | +             | -             | -             | -             | +
| interval              | See separate section [interval](#interval)                                    | +             | +             | +             | -             | -             | +             | +
| unit                  | Any string that serves as a measuring unit, such as `kg`, `sq`, etc.; this will be appended to the tick label | + | + | +     | -             | -             | +             | + 

The mapping of the x-axis defaults to [`caption`](#caption), the one of the y-axis default to [`value`](#value) and the z-axis (only applicable to 
scatter charts) defaults to [`weight`](#weight). That said, it stands to reason that you are free to map e.g. the `caption` property to the y-axis and the 
`value` property to the x-axis. Also, bear in mind that you have a [`group`](#group) and a [`colors`](#colors) object that can be mapped to any axis.
    
Check out our [storybook](https://analytics-addon-storybook.flowable.i/index.html) for some intricate examples.

example: domain object
```json
{
  "domain": {
    "items": [
      {
        "domain": "string",
        "level": 0
      }
    ],
    "totalCount": 1
  }
}
```

<a name="backgroundColor"></a>
### backgroundColor

Any RGB color in hex notation. E.g. #ff0000 for red.

<a name="caption"></a>
### caption

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| \***mapping**         | Any property of your datums. Defaults to `caption`.                           | +             | +             | +             | +             | +             | +             | +
| domain                | An [items array](#glossaryItemsArray) that holds a level-based map of [domains](#glossaryDomain); Defaults to `string`; See example below | + | + | + | +  | + | +            | +
| angle                 | The rotation angle of the caption label                                       | +             | +             | +             | +             | +             | +             | +
| textAnchor            | Sets the origin coordinates of a label; Any of `start`, `middle`, `end`; Defaults to `start`    | + | +       | +             | +             | +             | +             | +
| font                  | See separate section [font](#font)                                            | +             | +             | +             | +             | +             | +             | +
| dateParser            | A pattern which describes the format in which date strings are formatted that were retrieved from the data resource. This pattern is then used to parse the date string and cast it as a Date object. See separate section [dateParser](#dateParser). | + | + | + | + | + | + | +
| timeParser            | A pattern which describes the format in which time strings are formatted that were retrieved from the data resource. This pattern is then used to parse the time string and cast it as a Date object. See separate section [timeParser](#timeParser). | + | + | + | + | + | + | +
| stringFormats         | An [items array](#glossaryItemsArray) that holds a map of objects with properties ''searchPattern'' and ''replacePattern'' (regular expressions). | + | + | + | + | + | + | +
| numberFormat          | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [numberFormat](#numberFormat). | + | + | + | + | + | + | +
| dateFormat            | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [dateFormat](#dateFormat). | + | + | + | + | + | + | +
| timeFormat            | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [timeFormat](#timeFormat). | + | + | + | + | + | + | +

example: domain object
```json
{
  "domain": {
    "items": [
      {
        "domain": "string",
        "level": 0
      }
    ],
    "totalCount": 1
  }
}
```
Mapping the colors to a [mapped object](#glossaryMappedObject) means that a distinct color will be determined per property value. For example, if you map the colors 
to the `caption` property, it means that for each caption in your datums, a color will be determined. 

<a name="chartContainerType"></a>
### chartContainerType

Either `plain-chart-container` for charts without axes or `axes-chart-container` for charts with axes.

<a name="chartMode"></a>
### chartMode

Either `single` or `stacked` depending on whether you would like to have a chart with single bars, arcs etc. or stacked bars, arcs etc.

<a name="chartType"></a>
### chartType

Any of `area-chart`, `bar-chart`, `pie-chart`, `gauge-chart`, `line-chart`, `radar-chart` or `scatter-chart`.

<a name="color"></a>
### color

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| customColorEnabled    | Flag which defines whether the color shall be determined using the color strategy or a custom color | + | +   | +             | +             | +             | +             | +
| colorStrategy         | Any of `colorScheme`, `backgroundColor`, `negativeColorScheme` and `negativeBackgroundColor` | + | +          | +             | +             | +             | +             | +
| color                 | Any RGB color in hex notation, e.g. #ff0000 for red; only applied if `customColorEnabled` is set to `true`; Defaults to `#000000` | + | + | + | +   | +             | +             | +

<a name="colors"></a>
### colors

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| \***mapping**         | Any [mapped object](#glossaryMappedObject)                                    | +             | +             | +             | +             | +             | +             | +
| scheme                | An [items array](#glossaryItemsArray) that holds a level-based map of color scheme a/o interpolation colors; See separate section [scheme](#scheme). | + | + | + | + | + | +  | +
| k                     | Any number; Factor to brighten (x < 1) or darken the given color (x > 1); only used in stacked charts | - | + | -             | -             | +             | -             | -

<a name="dataResources"></a>
### dataResources

This is an [items array](#glossaryItemsArray) of data resources that all have the subsequent properties.

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the dataResource is enabled. Defaults to `false`.             | +             | +             | +             | +             | +             | +             | +
| \***queryUrl**        | Any string; the URL of the resource where the data is retrieved from.         | +             | +             | +             | +             | +             | +             | +
| contentType           | Any string; the content type; defaults to ``application/json; charset=utf-8`` | +             | +             | +             | +             | +             | +             | +
| headers               | Any HTTP headers                                                              | +             | +             | +             | +             | +             | +             | +
| mode                  | Any HTTP request mode; Might be ``navigate``, ``same-origin``, ``no-cors``, ``cors``| +       | +             | +             | +             | +             | +             | +


<a name="dateFormat"></a>
### dateFormat

Please refer to [d3-time-format](https://github.com/d3/d3-time-format#locale_format) for a comprehensive list of specifiers that can be used within the pattern.

Possible dateFormat patterns are:
* %Y-%m-%d (year-month-day; ISO 8601 date)
* %d.%m.%Y (day.month.year)
* %d. %B %Y (day. month name year)
* %d %B %Y (day month name year)
* %B %Y (month name year)
* %Y %B (year month name)
* %m %Y (month as a decimal number year)
* %Y %m (year month as a decimal number)
* %B (full month name)
* %b (abbreviated month name)
* %d/%m/%Y (day/month/year)
* %Y/%m/%d (year/month/day)
* %A (weekday)

<a name="dateParser"></a>
### dateParser

Please refer to [d3-time-format](https://github.com/d3/d3-time-format#locale_format) for a comprehensive list of specifiers that can be used within the pattern.

Possible dateParser patterns are:
* %Y-%m-%d (year-month-day; ISO 8601 date)
* %d.%m.%Y (day.month.year)
* %d. %B %Y (day. month name year)
* %d %B %Y (day month name year)
* %B %Y (month name year)
* %Y %B (year month name)
* %m %Y (month as a decimal number year)
* %Y %m (year month as a decimal number)
* %B (full month name)
* %b (abbreviated month name)
* %d/%m/%Y (day/month/year)
* %Y/%m/%d (year/month/day)
* %A (weekday)

<a name="drilling"></a>
### drilling

Please refer to the dedicated sections below if you would like to see detailed information about any of the subsequent properties.

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| drillDown             | See separate section [drillDown](#drillDown)                                  | +*            | +             | +*            | +*            | +             | +*            | +
| drillUp               | See separate section [drillUp](#drillUp)                                      | +*            | +             | +*            | +*            | +             | +*            | +
| drillAcross           | See separate section [drillAcross](#drillAcross)                              | +             | +             | +             | +             | +             | +             | +

\* only via legend items

<a name="drillDown"></a>
#### drillDown

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| selectOnly            | Boolean; if set to ``true``, the chart will not be re-rendered, however, the state will be updated; | + | +   | +             | +             | +             | +             | +
| needsGrandChildren    | Boolean; if set to ``true``, a successful drill down action needs to know not only one but the lower levels of a particular datum. | + | + | + | + | +        | +             | + 

<a name="drillUp"></a>
#### drillUp

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| selectOnly            | Boolean; if set to ``true``, the chart will not be re-rendered, however, the state will be updated; | + | +   | +             | +             | +             | +             | +
| label                 | An [items array](#glossaryItemsArray) that holds a level-based map of drill up button labels in one or multiple languages | + | + | + | +     | +             | +             | +

<a name="drillAcross"></a>
#### drillAcross

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| redirectUrl           | The URL to be redirected once the lowest level of a datum is reached. The URL might include expressions. | + | + | +          | +             | +             | +             | + 

<a name="elements"></a>
### elements

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| strokes               | See separate section [strokes](#strokes)                                      | +             | +             | +             | +             | +             | +             | +
| tooltip               | Object which only holds the `isEnabled` property; If that property is set to `true`, context-dependent tooltips are rendered | + | + | + | +  | +             | +             | +
| opacity               | The opacity of the element, between `0.0` and `1.0`; Defaults to `1.0`        | +             | +             | +             | +             | +             | +             | +

<a name="exportButtons"></a>
### exportButtons

This is a map of objects that hold the subsequent properties. By default, this map has two entries: `jpg` and `png`.

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the export button is enabled. Defaults to `false`.            | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the export button is visible. Defaults to `false`.            | +             | +             | +             | +             | +             | +             | +
| label                 | An [items array](#glossaryItemsArray) that holds a level-based map of labels in one or multiple languages | + | + | +         | +             | +             | +             | +

<a name="font"></a>
### font

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| color                 | See separate section [color](#color)                                          | +             | +             | +             | +             | +             | +             | +
| size                  | Any number; Defaults to `18`                                                  | +             | +             | +             | +             | +             | +             | +
| family                | Any font family; Default to `'"Helvetica Neue",Helvetica,Arial,sans-serif'`   | +             | +             | +             | +             | +             | +             | +

<a name="group"></a>
### group

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| \***mapping**         | Any property of your datums. Defaults to `group`.                             | +             | +             | +             | +             | +             | +             | +
| domain                | An [items array](#glossaryItemsArray) that holds a level-based map of [domains](#glossaryDomain); Defaults to `string`; See example below | + | + | + | +  | + | +            | +
| dateParser            | A pattern which describes the format in which date strings are formatted that were retrieved from the data resource. This pattern is then used to parse the date string and cast it as a Date object. See separate section [dateParser](#dateParser). | + | + | + | + | + | + | +
| timeParser            | A pattern which describes the format in which time strings are formatted that were retrieved from the data resource. This pattern is then used to parse the time string and cast it as a Date object. See separate section [timeParser](#timeParser). | + | + | + | + | + | + | +
| stringFormats         | An [items array](#glossaryItemsArray) that holds a map of objects with properties ''searchPattern'' and ''replacePattern'' (regular expressions). | + | + | + | + | + | + | +
| numberFormat          | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [numberFormat](#numberFormat). | + | + | + | + | + | + | +
| dateFormat            | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [dateFormat](#dateFormat). | + | + | + | + | + | + | +
| timeFormat            | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [timeFormat](#timeFormat). | + | + | + | + | + | + | +

example: domain object
```json
{
  "domain": {
    "items": [
      {
        "domain": "string",
        "level": 0
      }
    ],
    "totalCount": 1
  }
}
```

<a name="height"></a>
### height

Any number; The height of the chart

<a name="id"></a>
### id

Any identifying string. The ID of the chart. Must be unique for the entire website.

<a name="radius"></a>
### innerRadius and outerRadius

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| innerRadius           | Any number                                                                    | -             | -             | -             | +             | +             | -             | -
| outerRadius           | Any number                                                                    | -             | -             | -             | +             | +             | -             | -

Please not that with stacked charts, the radii are determined automatically. If you decide to manually set radii, you must make sure that the difference between the two numbers is not larger than 1. Otherwise the result is undefined.

<a name="interval"></a>
### interval

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| level                 | Any number; See [level](#level)                                               | +             | +             | +             | -             | -             | +             | +
| type                  | Any of `default`, `suggestedNumber`, `secondsInMinute`, `minutesInHour`, `minutesInHour`, `hoursInDay`, `daysInWeek`, `daysInMonth`, `daysInYear`, `weeksInMonth`, `weeksInYear`, `monthsInYear` and `years`; Defaults to `default` which means 10 ticks | + | + | + | - | - | + | +
| suggestedNumber       | Any number; If `type` is set to `suggestedNumber`, this number is taken as a proposal fot the number of ticks; The effective number of ticks is decided by D3 | + | + | + | - | - | + | +  

<a name="legend"></a>
### legend

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the legend is enabled. Defaults to `true`.                    | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the legend is visible. Defaults to `true`.                    | +             | +             | +             | +             | +             | +             | +
| \***mapping**         | Any property of your datums. Defaults to `caption`.                           | +             | +             | +             | +             | +             | +             | +
| domain                | An [items array](#glossaryItemsArray) that holds a level-based map of [domains](#glossaryDomain); Defaults to the domain of the mapped object; See example below | + | + | + | +  | + | + | +
| titles                | See separate section [titles](#titles)                                        | +             | +             | +             | +             | +             | +             | +
| swatchType            | Defines the shape of the swatch in the legend; Currently only `rect` and `circle` supported; Defaults to `circle` | + | + | + | +             | +             | +             | + 
| swatchSize            | Defines the size of the swatch (diameter when `circle` is chosen as `swatchType`); Defaults to ` 20` | + | +  | +             | +             | +             | +             | +
| orient                | The orient of the legend; any of `top`, `left`, `bottom` and `right`          | +             | +             | +             | +             | +             | +             | +
| alignment             | The alignment of the entire legend, but not its position within the legend container; Any of `horizontal` or `vertical`; Defaults to `horizontal` | + | + | + | + | + | +     | +
| offset                | The offset relative to the origin                                             | +             | +             | +             | +             | +             | +             | +
| lowestValuePosition   | The position of the lowest value; any of `top`, `left`, `bottom` and `right`  | +             | +             | +             | +             | +             | +             | +
| textAnchor            | Sets the origin coordinates of a label; Any of `start`, `middle`, `end`; Defaults to `start`  | + | +         | +             | +             | +             | +             | +
| angle                 | The rotation angle of the legend item labels; Defaults to `0` with an orient set to `left` or `right` and to `45` with an orient set to `top` or `bottom` | + | + | + | + | + | + | +
| horizontalAlignment   | Defines the horizontal alignment within the legend container; Any of `left`, `center` or `right`; Defaults to `center` | + | + | + | +        | +             | +             | +
| verticalAlignment     | Defines the vertical alignment within the legend container; Any of `top`, `middle` or `bottom`; Defaults to `middle` | + | + | + | +          | +             | +             | +
| font                  | See separate section [font](#font)                                            | +             | +             | +             | +             | +             | +             | +
| tooltip               | Object which only holds the `isEnabled` property; If that property is set to `true`, context-dependent tooltips are rendered | + | + | + | +  | +             | +             | + 

example: domain object
```json
{
  "domain": {
    "items": [
      {
        "domain": "string",
        "level": 0
      }
    ],
    "totalCount": 1
  }
}
```

<a name="margin"></a>
### margin

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| top                   | Any number                                                                    | +             | +             | +             | +             | +             | +             | +
| left                  | Any number                                                                    | +             | +             | +             | +             | +             | +             | +
| bottom                | Any number                                                                    | +             | +             | +             | +             | +             | +             | +
| right                 | Any number                                                                    | +             | +             | +             | +             | +             | +             | +

<a name="numberFormat"></a>
### numberFormat

Please refer to [d3-format](https://github.com/d3/d3-format#locale_format) for a comprehensive list of specifiers that can be used within the pattern.

* .0f (Fixed point (e.g. 1))
* .1f (Fixed point number with one significant digit (e.g. 1.2))
* .2f (Fixed point number with two significant digits (e.g. 1.23))
* .3f (Fixed point number with two significant digits (e.g. 1.234))
* .0% (Rounded percentage (e.g. 12%))
* .1% (Rounded percentage with one significant digit (e.g. 12.3%))
* .2% (Rounded percentage with two significant digits (e.g. 12.34%))
* .3% (Rounded percentage with three significant digits (e.g. 12.345%))
* .2f'' (Seconds)
* .2f' (Minutes)
* .0s (SI-prefixed number (e.g. 1M = 1 million))
* .1s (SI-prefixed number with one significant digit (e.g. 1.2M = 1.2 million))
* .2s (SI-prefixed number with two significant digits (e.g. 1.23M = 1.23 million))
* .3s (SI-prefixed number with three significant digits (e.g. 1.234M = 1.234 million))
* b (Binary notation)
* e (Exponent notation)
* \#x (Hexadecimal notation)

<a name="padding"></a>
### padding

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| top                   | Any number                                                                    | +             | +             | +             | +             | +             | +             | +
| left                  | Any number                                                                    | +             | +             | +             | +             | +             | +             | +
| bottom                | Any number                                                                    | +             | +             | +             | +             | +             | +             | +
| right                 | Any number                                                                    | +             | +             | +             | +             | +             | +             | +

<a name="scheme"></a>
### scheme

The scheme object either holds a pre-defined color scheme such as the ones from [d3-scale-chromatic](https://github.com/d3/d3-scale-chromatic) or a combination of custom interpolation colors (used if any only if scheme is set to `grouped` or `threshold`). If no scheme is declared, the flowable colors are taken.

example: scheme object
```json
{
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "caption"
        }
      ],
      "totalCount": 1
    },
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [],
            "totalCount": 0
          },
          "level": 0,
          "scheme": "viridis"
        }
      ],
      "totalCount": 1
    },
    "k": 0.3
  }
}
```

<a name="shownLevels"></a>
### shownLevels

Any number. The number of shown levels on a particular chart. This is needed if you would like to have a chart with stacked elements since this number defines the 
number of stacked elements or in other words, the depth of shown data.

<a name="strokes"></a>
### strokes

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| width                 | Any number; Defaults to `1`.                                                  | +             | +             | +             | +             | +             | +             | +
| radius                | Any number or `undefined`; Defaults to 0.                                     | +             | +             | +             | +             | +             | +             | +
| isEnabled             | Defines whether the strokes are enabled. Defaults to `true`.                  | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the strokes are visible. Defaults to `true`.                  | +             | +             | +             | +             | +             | +             | +
| customColorEnabled    | Defines whether the subsequent color shall be used as the strokes' color.     | +             | +             | +             | +             | +             | +             | +
| color                 | Any RGB color in hex notation (e.g. \#ff0000 for red).                        | +             | +             | +             | +             | +             | +             | +

<a name="ticks"></a>
### ticks

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the ticks are enabled. Defaults to `true`.                    | +             | +             | +             | -             | -             | +             | +
| isVisible             | Defines whether the ticks are visible. Defaults to `true`.                    | +             | +             | +             | -             | -             | +             | +
| offset                | The offset relative to the axis or element, resp.                             | +             | +             | +             | +             | +             | +             | +
| angle                 | The rotation angle of the tick labels                                         | +             | +             | +             | -             | -             | +             | +
| textAnchor            | Sets the origin coordinates of a label; Any of `start`, `middle`, `end`; Defaults to `start`  | + | +         | +             | -             | -             | +             | +
| interval              | See separate section [interval](#interval)                                    | +             | +             | +             | -             | -             | +             | +

<a name="timeFormat"></a>
### timeFormat

Please refer to [d3-time-format](https://github.com/d3/d3-time-format#locale_format) for a comprehensive list of specifiers that can be used within the pattern.

Possible timeFormat patterns are:
* %H:%M:%S (24-hour:minute:second)
* %H:%M (24-hour:minute)
* %H:%M:%S.%L (24-hour:minute:second.millisecond)
* %I:%M:%S (12-hour:minute:second)
* %I:%M (12-hour:minute)
* %I:%M:%S.%L (12-hour:minute:second.millisecond)

<a name="timeParser"></a>
### timeParser

Please refer to [d3-time-format](https://github.com/d3/d3-time-format#locale_format) for a comprehensive list of specifiers that can be used within the pattern.

Possible timeParser patterns are:
* %H:%M:%S (24-hour:minute:second)
* %H:%M (24-hour:minute)
* %H:%M:%S.%L (24-hour:minute:second.millisecond)
* %I:%M:%S (12-hour:minute:second)
* %I:%M (12-hour:minute)
* %I:%M:%S.%L (12-hour:minute:second.millisecond)

<a name="timestamp"></a>
### timestamp

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the timestamp field is enabled. Defaults to `false`.          | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the timestamp field is visible. Defaults to `false`.          | +             | +             | +             | +             | +             | +             | +

<a name="titles"></a>
### titles

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the titles are enabled. Defaults to `false`.                  | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the titles are visible. Defaults to `false`.                  | +             | +             | +             | +             | +             | +             | +
| \***mapping**         | An [items array](#glossaryItemsArray) that holds a level-based map; See example below | +     | +             | +             | +             | +             | +             | +
| offset                | The offset relative to the origin                                             | +             | +             | +             | +             | +             | +             | +
| orient                | The orient of the chart titles; any of `top`, `left`, `bottom` and `right`    | +             | +             | +             | -             | -             | +             | +
| angle                 | The rotation angle of the title                                               | +             | +             | +             | +             | +             | +             | +
| font                  | See separate section [font](#font)                                            | +             | +             | +             | +             | +             | +             | +
| textAnchor            | Sets the origin coordinates of a label; Any of `start`, `middle`, `end`; Defaults to `start`  | + | +         | +             | +             | +             | +             | +

example: mapping object
```json
{
  "titles": {
    "isEnabled": true,
    "isVisible": true,
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": {
            "items": [
              {
                "language": "en",
                "title": "This is my title"
              },
              {
                "language": "de",
                "title": "Das ist mein Titel"
              }
            ],
            "totalCount": 2
          }
        },
        {
          "level": 1,
          "mapping": {
            "items": [
              {
                "language": "en",
                "title": "This is my title from level 2 onwards"
              },
              {
                "language": "de",
                "title": "Das ist mein Titel ab Level 2"
              }
            ],
            "totalCount": 2
          }
        }
      ],
      "totalCount": 2
    }
  }
}
```

<a name="tooltips"></a>
### tooltips

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the tooltips are enabled. Defaults to `true`.                 | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the tooltips are visible. Defaults to `true`.                 | +             | +             | +             | +             | +             | +             | +
| \***mapping**         | An [items array](#glossaryItemsArray) that holds a level-based map; See example below | +     | +             | +             | +             | +             | +             | +
| padding               | See separate section [padding](#padding)                                      | +             | +             | +             | +             | +             | +             | +
| margin                | See separate section [margin](#margin)                                        | +             | +             | +             | +             | +             | +             | +
| backgroundColor       | Any RGB color in hex notation (e.g. \#ff0000 for red)                         | +             | +             | +             | +             | +             | +             | +
| strokes               | See separate section [strokes](#strokes)                                      | +             | +             | +             | +             | +             | +             | +
| offset                | The offset relative to the mouse pointer                                      | +             | +             | +             | +             | +             | +             | +
| direction             | The direction where the tooltip is to be placed (in the sense of a cardinal orientation). Possible values are `n`, `ne`, `e`, `se`, `s`, `sw`, `w` and `nw` | + | + | + | + | + | + | +
| font                  | See separate section [font](#font)                                            | +             | +             | +             | +             | +             | +             | +

example: mapping object
```json
{
  "titles": {
    "isEnabled": true,
    "isVisible": true,
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": {
            "items": [
              {
                "language": "en",
                "title": "This is my title"
              },
              {
                "language": "de",
                "title": "Das ist mein Titel"
              }
            ],
            "totalCount": 2
          }
        },
        {
          "level": 1,
          "mapping": {
            "items": [
              {
                "language": "en",
                "title": "This is my title from level 2 onwards"
              },
              {
                "language": "de",
                "title": "Das ist mein Titel ab Level 2"
              }
            ],
            "totalCount": 2
          }
        }
      ],
      "totalCount": 2
    }
  }
}
```

<a name="transition"></a>
### transition

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the transition is enabled. Defaults to `false`.               | +             | +             | +             | +             | +             | +             | +
| algorithm             | The transition algorithm used; Any of `linear`, `ease`, `ease-in`, `ease-out`, `ease-in-out`; See [D3 transition](https://github.com/d3/d3-transition) | + | + | + | + | + | + | +            
| duration              | The duration in seconds; Defaults to `1`                                      | +             | +             | +             | +             | +             | +             | +
| delay                 | The delay in seconds; Defaults to `0`                                         | +             | +             | +             | +             | +             | +             | +

<a name="user"></a>
### user

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the user field is enabled. Defaults to `false`.               | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the user field is visible. Defaults to `false`.               | +             | +             | +             | +             | +             | +             | +

<a name="userResource"></a>
### userResource

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the userResource is enabled. Defaults to `true`.              | +             | +             | +             | +             | +             | +             | +
| \***queryUrl**        | Any string; the URL of the resource where the user is retrieved from.         | +             | +             | +             | +             | +             | +             | +
| contentType           | Any string; the content type; defaults to ``application/json; charset=utf-8`` | +             | +             | +             | +             | +             | +             | +
| headers               | Any HTTP headers                                                              | +             | +             | +             | +             | +             | +             | +
| mode                  | Any HTTP request mode; Might be ``navigate``, ``same-origin``, ``no-cors``, ``cors``| +       | +             | +             | +             | +             | +             | +

If the user resource is disabled, the default locale `en-US` will be taken for any formatting.

<a name="value"></a>
### value

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isVisible             | Defines whether the label for the value within the chart is visible. Defaults to `false`. | + | +             | +             | +             | +             | +             | +
| \***mapping**         | Any property of your datums. Defaults to `value`.                             | +             | +             | +             | +             | +             | +             | +
| domain                | An [items array](#glossaryItemsArray) that holds a level-based map of [domains](#glossaryDomain); Defaults to `numeric`; See example below | + | + | + | +  | + | +           | +
| position              | The position of the value label. Possible values are `top`, `left`, `right`, `bottom`, `inside`, `outside`, `insideLeft`, `insideRight`, `insideTop`, `insideBottom`, `insideTopLeft`, `insideBottomLeft`, `insideTopRight`, `insideBottomRight`, `insideStart`, `insideEnd`, `end` and `center`. See [recharts Label](http://recharts.org/en-US/api/Label) for examples. | + | + | + | + | + | + | + 
| angle                 | The rotation angle of the value label                                         | +             | +             | +             | +             | +             | +             | +
| textAnchor            | Sets the origin coordinates of a label; Any of `start`, `middle`, `end`; Defaults to `start`  | + | +         | +             | +             | +             | +             | +
| font                  | See separate section [font](#font)                                            | +             | +             | +             | +             | +             | +             | +
| usePercentage         | Flag which determines whether the percentage shall be taken rather than the absolute value; Defaults to `false` | + | + | +   | +             | +             | +             | +
| dateParser            | A pattern which describes the format in which date strings are formatted that were retrieved from the data resource. This pattern is then used to parse the date string and cast it as a Date object. See separate section [dateParser](#dateParser). | + | + | + | + | + | + | +
| timeParser            | A pattern which describes the format in which time strings are formatted that were retrieved from the data resource. This pattern is then used to parse the time string and cast it as a Date object. See separate section [timeParser](#timeParser). | + | + | + | + | + | + | +
| stringFormats         | An [items array](#glossaryItemsArray) that holds a map of objects with properties ''searchPattern'' and ''replacePattern'' (regular expressions). | + | + | + | + | + | + | +
| numberFormat          | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [numberFormat](#numberFormat). | + | + | + | + | + | + | +
| dateFormat            | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [dateFormat](#dateFormat). | + | + | + | + | + | + | +
| timeFormat            | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [timeFormat](#timeFormat). | + | + | + | + | + | + | +

example: domain object
```json
{
  "domain": {
    "items": [
      {
        "domain": "string",
        "level": 0
      }
    ],
    "totalCount": 1
  }
}
```

<a name="version"></a>
### version

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isEnabled             | Defines whether the version field is enabled. Defaults to `false`.            | +             | +             | +             | +             | +             | +             | +
| isVisible             | Defines whether the version is visible. Defaults to `false`.                  | +             | +             | +             | +             | +             | +             | +
| content

<a name="weight"></a>
### weight

| Name                  | Description                                                                   | Area charts   | Bar charts    | Line charts   | Gauge charts  | Pie charts    | Radar charts  | Scatter charts
| ---                   | ---                                                                           | ---           | ---           | ---           | ---           | ---           | ---           | ---
| isVisible             | Defines whether the label for the weight is visible. Defaults to `true`.      | +             | +             | +             | +             | +             | +             | +
| \***mapping**         | Any property of your datums. Defaults to `weight`.                            | +             | +             | +             | +             | +             | +             | +
| domain                | An [items array](#glossaryItemsArray) that holds a level-based map of [domains](#glossaryDomain); Defaults to `numeric`; See example below | + | + | + | +  | + | +           | +
| position              | The position of the weight label. Possible values are `top`, `left`, `right`, `bottom`, `inside`, `outside`, `insideLeft`, `insideRight`, `insideTop`, `insideBottom`, `insideTopLeft`, `insideBottomLeft`, `insideTopRight`, `insideBottomRight`, `insideStart`, `insideEnd`, `end` and `center`. See [recharts Label](http://recharts.org/en-US/api/Label) for examples. | + | + | + | + | + | + | +
| angle                 | The rotation angle of the weight label                                        | +             | +             | +             | +             | +             | +             | +
| textAnchor            | Sets the origin coordinates of a label; Any of `start`, `middle`, `end`; Defaults to `start`  | + | +         | +             | +             | +             | +             | +
| font                  | See separate section [font](#font)                                            | +             | +             | +             | +             | +             | +             | +
| usePercentage         | Flag which determines whether the percentage shall be taken rather than the absolute value; Defaults to `false` | + | + | +   | +             | +             | +             | + 
| numberFormat          | A pattern which describes the format that shall be used when rendering numbers within a chart. See separate section [numberFormat](#numberFormat). | + | + | + | + | + | + | +

example: domain object
```json
{
  "domain": {
    "items": [
      {
        "domain": "string",
        "level": 0
      }
    ],
    "totalCount": 1
  }
}
```