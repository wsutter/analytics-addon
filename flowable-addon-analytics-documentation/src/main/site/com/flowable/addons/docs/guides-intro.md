---
id: guides-intro
title: Introduction
---

Flowable Addon Analytics is a declarative charts library. It is possible to superimpose [Flowable Forms](https://forms.flowable.io/) by adding one more multiple 
charts as components to the component store of Flowable Forms. This way, you can use them just as any other component within a form. The components of Flowable 
Addon Analytics, however, are intended to be used standalone and therefore simply take a chart definition (JSON) as well as one or multiple data sources (JSON) 
and render themselves.

Flowable Addon Analytics was designed with the subsequent features in mind: 

- Drill down (in the same chart as well as into other form components) and drill across (redirection) capabilities
- Interactivity through the use of expressions
- Hover effects on all chart elements (incl. tooltips)
- Easily customizable
- Easy styling
- Animations

The form definition is simply an object with very few mandatory properties as well as a large number of optional properties. 

For example:

[flw-addon-analytics]
{
  "chartContainerType": "plain-chart-container",
  "chartType": "pie-chart",
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/pieCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 400,
  "id": "introChart",
  "innerRadius": 0.1,
  "legend": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$caption"
        }
      ]
    }
  },
  "padding": {
    "bottom": 50
  },
  "outerRadius": 1,
  "ticks": {
    "textAnchor": "start"
  },
  "userResource": {
    "mode": "cors",
    "queryUrl": "currentUser.json"
  }
}
[/flw-addon-analytics]
