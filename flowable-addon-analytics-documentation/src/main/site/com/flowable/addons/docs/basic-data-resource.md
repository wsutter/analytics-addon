---
id: basic-data-resource
title: Data resources
---

Flowable Addon Analytics converts arbitrary data into a chart. In general, anything can serve as a data source, as long as it is structured as an array of objects (datums). 

## Glossary

**Please make yourself familiar with the vocabulary used within this documentation before diving deep into it**

| Name                                              | Description
| ---                                               | ---
| <a name="glossaryDataResource">data resource</a>  | Usually the URL where data is retrieved from
| <a name="glossaryDataSource">data source</a>      | Refers to the actual data retrieved from a data resource; the data source must be of content type ``application/json; charset=utf-8`` and hold an array of arbitrary objects (datums).

## Configuration

The content type of such a data source must therefore be ``application/json; charset=utf-8``. You will have to decide which properties of those datums are going to serve as
the ``caption``, ``value``, ``aggregations`` and optionally also as ``group`` and ``weight`` parameters.

For now, however, this is what a data resource configuration in your chart definition looks like:
```json
{
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/areaCharts/response.json",
          "type": "rest"
        }
      },
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/areaCharts2/response.json",
          "type": "rest"
        }
      }
    ]
  }
}
```

As you can see, you can have multiple data resources on the same level which means that both data sources are going to be merged together, where duplicates are going 
to be overridden consecutively. This implies that you can also have one or multiple data resources for each level separately. It stands to reason that each data source must be an array of objects (datums) and that each datum feature the same set of properties.

## Example data source and mapping

The above data resource might retrieve a data source that looks like this:

```json
[
  {
    "category": "lorem",
    "amount": 90,
    "gravity": 16,
    "group": "Group 1",
    "children": [
      {
        "category": "lorem 1",
        "amount": 20,
        "gravity": 2,
        "group": "Group 1",
        "children": [],
        "date": "21.01.2016"
      },
      {
        "category": "lorem 2",
        "amount": 30,
        "gravity": 10,
        "group": "Group 1",
        "children": [],
        "date": "26.02.2016"
      },
      {
        "category": "lorem 3",
        "amount": 40,
        "gravity": 4,
        "group": "Group 1",
        "children": [],
        "date": "07.03.2016"
      }
    ],
    "date": "21.01.2016"
  },
  ...
]
```

As you can see, the data source consists of arbitrary objects that can have multiple dimensions (or levels; can be used for stacking a/o drillings). The properties of 
this data source have to be mapped to the ``caption``, ``value``, ``aggregations`` and optionally also as ``group`` and ``weight`` parameters. This is also done in the 
chart definition.

**All of these objects are called mappable objects**

This is how you would create a mapping for any mappable object:

```json
{
  "<the mappable object>": {
    "domain": {
      "items": [
        {
          "domain": "<a domain>",
          "level": <the level>
        }
      ]
    },
    "mapping": {
      "items": [
        {
          "level": <the level>,
          "mapping": "<the property in the data source>"
        }
      ]
    }
  }
}
```
The domain is actually optional and might not be needed. The domain describes the data type of the concerning property. It usually has a default value, e.g. in case of the ```caption```, it is a string. However, if you want to create a time axis and your captions are of domain type ```date```, you might actually want to override the domain.
So in case of the ```caption```, if it shall map to the above example data source, it would look like this:

```json
{
  "caption": {
    "domain": {
      "items": [
        {
          "domain": "string", // not needed
          "level": 0
        }
      ]
    },
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "category"
        }
      ]
    }
  }
}
```

Or if your caption shall be the ```date``` property:

```json
{
  "caption": {
    "domain": {
      "items": [
        {
          "domain": "date",
          "level": 0
        }
      ]
    },
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "date"
        }
      ]
    }
  }
}
```

## Referring to mappings in your chart configuration

You are likely going to need to refer to a mappable object like the ```caption``` in your chart configuration.

Whenever you find yourself in such a situation, you can use pre-defined [expressions](basic-expressions.md).

For example, if you want to map your mappable object ```caption``` to the x-axis of your chart, you can refer to the mappable object through the ```$caption``` 
expression.

```json
{
  "axes": {
    "x": {
      "mapping": {
        "items": [
          {
            "level": 0,
            "mapping": "$caption"
          }
        ]
      }
    }
  }
}
```

References like that can be used for all other mappable objects as well.

## Properties of a data resource

The following tables show you the properties of the data resource configuration which you have to set up within your chart definition.

Please bear in mind that only properties that are marked as \***propertyName** are mandatory.

Data resources consists of an object that holds an array called ```items```. The top level properties of the objects within that array are:

| Name              | Description
| ---               | ---
| **level**         | The level this data resource is bound to
| **dataResource**  | Object that describes a single data resource

The subsequent properties are properties of a single ```dataResource``` object.

| Name              | Description
| ---               | ---
| isEnabled         | Boolean; default: false; If set to true, the data resource is requested and the retrieved data array is going to be part of the resulting data source
| contentType       | In case your data resource does not emit ```application/json; charset=utf-8```, you would have to configure this here; however, any other format is not currently supported by Flowable Addon Analytics
| type              | This property defines what architecture is going to be used. Currently, only ```rest``` is supported.
| queryUrl          | the URL of the data resource
| mode              | a RequestMode (```same-origin```, ```no-cors```, ```cors```)

## Properties of the mapped objects

Please refer to the chapter [Chart definition](basic-chart-definition.md).

## Example use cases

Please refer to the [data resource use cases](usecases-data-resources.md) to see some examples of the data resource.

