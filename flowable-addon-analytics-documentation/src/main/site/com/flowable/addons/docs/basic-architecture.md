---
id: basic-architecture
title: Architecture
---

This page shows you the basic architecture of Flowable Addon Analytics.

## Entry points

There are several entry points to Flowable Addon Analytics. The one entry point that is probably most evident can be found at ```flowable-addon-analytics/src/main/js/com/flowable/addons/widgets/src/index.tsx```.

It is a file that lists the main exports of the projects, like this:

```typescript jsx
export const barChart = BarChartFactory;
export const lineChart = LineChartFactory;
export const pieChart = PieChartFactory;
export const areaChart = AreaChartFactory;
export const radarChart = RadarChartFactory;
export const scatterChart = ScatterChartFactory;
export const gaugeChart = GaugeChartFactory;
export const heatmapChart = HeatmapChartFactory;
```

If you import the addon into your project with

```typescript jsx
import * as Analytics from "@flowable/addon-analytics-react"
```

or through the vanilla build with

```javascript
flwAddonAnalytics.default
```

you will get an object that looks like this:

```json
{
  barChart: class / function,
  lineChart: class / function,
  pieChart: class / function,
  areaChart: class / function,
  radarChart: class / function,
  scatterChart: class / function,
  gaugeChart: class / function,
  heatmapChart: class / function
}
```

This is the structure needed by Flowable Forms since it resembles a Component Store.

However, since Flowable Addon Analytics also makes it possible to import only single components, there are more, less obvious entry points.

Every non-abstract factory holds two additional exports like this:

```typescript jsx
export const AreaChart = AreaChartFactory;

export default { areaChart: AreaChartFactory };
```

This allows you to import a specific component (in this case the area chart) in two different ways. 

```typescript jsx
import AreaChart from "@flowable/addon-analytics-react/areaChart"
```

and

```typescript jsx
import { AreaChart } from "@flowable/addon-analytics-react/areaChart"
```

or through the vanilla build with

```javascript
flwAddonAnalytics.AreaChart
```

The way you have to import the addon varies strongly on your particular use case and we suggest that you have a look into the [Usage](guides-usage.md) chapter.

## Inversion of Control

Flowable Addon Analytics makes use of ```inversify``` which is an inversion of control container for Javascript.

The configuration can be found at ```flowable-addon-analytics/src/main/js/com/flowable/addons/widgets/src/config```.

## Factories

As mentioned before, factories feature some of the main entry points. Apart from that, they are also React components through their super class ```AnalyticsChartFactory``` and 
therefore feature a render function.

Factories can be found ar ```flowable-addon-analytics/src/main/js/com/flowable/addons/widgets/src/factories```.

If you import e.g. a ```PieChart``` component and use it like this

```typescript jsx
<PieChart
  config={config}
  payload={payload}
  additionalData={additionalData}
  />
```

the render function of the PieChartFactory is going to be called. Before that, however, the passed props are deserialized from a plain JSON object to an actual 
JavaScript object tree. This is done recursively in the super class ```AnalyticsChartFactory```. To be precise, the configuration is deserialized and made available 
as a Javascript object tree through the factory's ```deserializedExtraSettings``` properties. _That very object_ is going to be the props ```extraSettings``` of the 
corresponding chart component, in this case ```PieChartComponent```.

You can pass 3 different props into each factory:

| Name              | Description
| ---               | ---
| \***config**      | This is either an entire form definition with rows and cols (as received from Flowable Forms) or only the plain chart definition (which is the ```extraSettings``` object of the form definition).
| payload           | The initial state of the chart
| additionalData    | An object holding functions for formatting e.g. the tooltips

***Caution:*** It is very important to understand that the the aforementioned JavaScript object tree is a tree consisting mostly of entities (see below)

Some factories can be injected by means of ```inversify```.

## Components

The components (except for the heatmap) incorporate the components of ***from recharts***, but also some of Flowable Forms. They are React components through their common super class ```AnalyticsChart```.
Since they massively enrich the standard recharts components, they are also the most complex pieces of code you will find in all of Flowable Addon Analytics. The heatmap is insofar special as it doessn't feature 
any recharts components, however, is structurally exactly the same as any other chart component.

You can find all the components at ```flowable-addon-analytics/src/main/js/com/flowable/addons/widgets/src/components```.

At this point, we suggest you have a look into the docs of [recharts](http://recharts.org/en-US/api).

## Entities

To put it as simply as possible, it is in many cases the glue which puts the data resource, user resource, chart definition as chart state together, often by means of ***lodash*** or ***D3*** functions.

You can find the entities at ```flowable-addon-analytics/src/main/js/com/flowable/addons/widgets/src/entities```.

There is an entity for almost each entry in the chart configuration. For example:

```json
{
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "level": 0,
          "scheme": "Category10"
        }
      ]
    }
  }
}
```

This is a colors configuration provided through the ```config``` props of a chart factory. As mentioned earlier, this configuration is transformed into an actual JavaScript object 
which among intrinsic (default) properties also feature functions. There is an entity called ```colors.tsx``` (one of the more complex entities) that is responsible for creating 
color scales according to the configuration above. It does so by calling **D3 functions**.

Entities are called by components when needed and do call services for logic they share among one another.

As you can imagine, apart from the entity ```colors.tsx```, there is likewise an entity called ```mapping.tsx``` etc.

Another heavily used entity is ```datum.tsx```. It is the entity that represents the either a single datum or an entire tree of datums (called data). As you can 
imagine retrieving as well as preparing datums so that they can be consumed by the chart component rendering is fairly complex and therefore outsourced to the 
```dataSourceService.tsx```. 

## Services

The services consist of some commonly used logic throughout components and entities.

You can find the services at ```flowable-addon-analytics/src/main/js/com/flowable/addons/widgets/src/services```.

As an example, one of the most commonly used services is the ```hashService.tsx``` which is responsible for creating hashes of JavaScript objects to map a JavaScript object 
to a rendered DOM object. That is useful, for example if you want to display a particular tooltip for a particular element within the chart.

Another heavily used service is the ```dataSourceService.tsx``` which is responsible to not only retrieve the datums from a data resource but also to prepare and 
enhance them so that they later can be consumed by the chart component rendering.