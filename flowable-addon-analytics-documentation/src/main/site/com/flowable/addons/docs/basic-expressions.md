---
id: basic-expressions
title: Expressions
---

Flowable Addon Analytics knows additional expressions along with the ones already available trough Flowable Forms. These expressions can therefore solely be used by 
the chart rendering engine. 

You can use these expressions within the chart definition, e.g. within tooltip contents. In order to use them, you will have to wrap any expression in double curly braces (``{{`` and ``}}``).

| Name              | Description
| ---               | ---
| $id               | The ID of the current chart.
| $location         | The URL of the page into which the current chart is integrated.
| $level            | The level of a particular datum, that is the depth within the data source tree where that datum is placed.
| $datum, datum     | The datum itself.
| $item, item       | This is an alias for $datum.
| $caption, caption | The caption of the current datum.
| $value, value     | The value of the current datum.
| $group, group     | The group of the current group.
| $weight, weight   | The weight of the current weight.
| $index            | The index of the datum, that is the index relative to the siblings of the datum.
| $percentage       | The percentage of the datum's value, relative to all the siblings' values.
