---
id: guides-overview
title: Overview
---

<a name="dependencies"></a>
## Dependencies of the addon

Flowable addon analytics pulls in a large number of dependencies which might maintaining the addon a bit tricky. The subsequent list gives you an overview all the 
dependencies that are pulled in and the reason why they are necessary:

### D3 dependencies

D3 functions are used to enhance the capabilities of recharts, e.g. to create customized color scales or to format the ticks of an axis.

| Dependency                                        | Purpose
| ---                                               | ---
| d3-array                                          | Manipulation of data arrays, especially to determine minimums or maximums
| d3-axis                                           | Creation of non-default axes, such as time axes
| d3-color                                          | Creation of non-default color scales
| d3-ease                                           | Animations
| d3-format                                         | Formatting of data properties such as currencies
| d3-interpolate                                    | Used in conjunction with d3-color to interpolate color ranges for arbitrary color scales
| d3-scale                                          | Used in conjunction with d3-color to create arbitrary color scales
| d3-scale-chromatic                                | A set of pre-defined color scales such as Viridis which are mainly used for demo-purposes
| d3-selection                                      | Selection of SVG elements
| d3-svg-legend                                     | Used to create legends within the chart containers
| d3-time-format                                    | Formatting of data properties particularly dates and times

### Lodash dependencies

Lodash functions are used to manipulate the datums from the data source in order to render them as elements of a chart.

| Dependency                                        | Purpose
| ---                                               | ---
| lodash.clonedeep                                  | Deep cloning of objects
| lodash.compact                                    | Creating arrays with all falsey values removed
| lodash.concat                                     | Concatenation of two or multiple arrays.
| lodash.filter                                     | Filtering values out of an arbitrary array
| lodash.flatten                                    | Flattening a multi-dimensional array to a depth of only one level
| lodash.groupby                                    | Groups datums according to the group parameter
| lodash.isdate                                     | Checks whether a value is actually a date
| lodash.isempty                                    | Checks for empty values
| lodash.isnumber                                   | Checks whether a value is a number
| lodash.isobject                                   | Checks whether a value is an object
| lodash.isstring                                   | Checks whether a value is a string
| lodash.isundefined                                | Checks whether a value is undefined
| lodash.map                                        | Creates an array of values by running each element in collection thru iteratee
| lodash.maxby                                      | Determines the maximum value of an array by applying a condition
| lodash.merge                                      | Merges two or more objects
| lodash.reduce                                     | Reduces collection to a value which is the accumulated result of running each element in collection through an iteratee
| lodash.sortby                                     | Sorts an array by applying a condition
| lodash.uniq                                       | Deletes any duplicates from an array
| lodash.without                                    | Creates an array excluding all given values

### Other dependencies

| Dependency                                        | Purpose
| ---                                               | ---
| @loadable/component                               | To split the resulting bundle into loadable components
| ajv                                               | JSON Schema validation
| file-saver                                        | Used to render charts into files, such as PNG or JPEG
| inversify                                         | Inversion of control container for JavaScript
| moment-range                                      | Extends the capabilites of Moment; Creates date and time ranges
| recharts                                          | Creates charts as React components
| simpleheat                                        | Drawing heatmaps into canvases
| spin.js                                           | Creating fancy spinners while loading a chart component
| svg-path-properties                               | Reading out the properties of an SVG path (used for heatmaps)
| svg-points                                        | Creates an SVG path out of an array of points
| wilderness-dom-node                               | A set of functions to convert between SVG DOM nodes, Plain Shape Objects and Frame Shapes (used for heatmaps)
| xxhashjs                                          | Extremely fast hash algorithm to identify datums and their DOM counterparts

<a name="devDependencies"></a>
## Dev dependencies of the addon

### Webpack dependencies

| Dev dependency                                    | Purpose
| ---                                               | ---
| babel-loader                                      | Transpiling JavaScript files using Babel
| css-loader                                        | Transpiling CSS files
| file-loader                                       | Transpiling of arbitrary files
| html-loader                                       | Transpiling of HTML files
| sass-loader                                       | Transpiling of scss files
| style-loader                                      | Injecting <style> tags into the DOM
| ts-loader                                         | Transpiling TypeScript files
| babel-plugin-transform-promise-to-bluebird        | Transforming Promise to bluebird
| circular-dependency-plugin                        | Plugin to check for circular dependencies
| clean-webpack-plugin                              | Plugin that deletes files and folders prior to building
| copy-webpack-plugin                               | Plugin that copies files and folders
| dts-generator-webpack-plugin                      | Generates a .d.ts TypeScript definition file
| generate-package-json-webpack-plugin              | Generates a package.json file
| license-webpack-plugin                            | Accumulates all licences of all dependencies into one large LICENCE.md
| mini-css-extract-plugin                           | Extracts (minified) CSS
| tsconfig-paths-webpack-plugin                     | Allows for having multiple .tsconfig files to be used by webpack
| uglifyjs-webpack-plugin                           | Uglifies the resulting bundle
| webpack-event-plugin                              | Event-hooking (for cleaning up after the bundle has been created)
| webpack-stats-plugin                              | Plugin which creates a stats.json (for the analyzer)
| webpack-bundle-analyzer                           | Analyzes the resulting bundle and creates an analyze.html
| webpack                                           | Well, it's Webpack
| webpack-cli                                       | CLI for webpack (for maintenance purposes)
| dts-generator                                     | Creating a .d.ts Typescript definition
| fs-extra                                          | To manipulate files and folders on the file system
| ts-node                                           | TypeScript execution environment for node.js


### Babel dependencies

| Dev dependency                                    | Purpose
| ---                                               | ---
| @babel/core                                       | Babel compiler core
| @babel/helper-module-imports                      | Allows dynamic imports
| @babel/plugin-syntax-dynamic-import               | Plugin for transpiling dynamic imports
| @babel/polyfill                                   | Provides polyfills necessary for a full ES2015+ environment
| @babel/preset-env                                 | Babel preset for each environment
| @babel/preset-react                               | Babel preset for all React plugins
| @babel/runtime                                    | Babel's modular runtime helpers

### Testing dependencies

| Dev dependency                                    | Purpose
| ---                                               | ---
| jest                                              | Well, it's Jest
| babel-jest                                        | Jest plugin to use babel for transformation
| jest-bamboo-formatter                             | A reporter for jest which produces a report compatible with Atlassian Bamboo Mocha Test Parser
| jest-enzyme                                       | Testing matchers
| jest-fetch-mock                                   | Mocks window.fetch
| ts-jest                                           | Enables TypeScript with Jest
| enzyme                                            | Testing utilities for React components
| enzyme-adapter-react-16                           | Enzyme adapter for React 16
| identity-obj-proxy                                | Mocking objects
| json-server                                       | Server that hosts JSON files

### Other dependencies

| Dev dependency                                    | Purpose
| ---                                               | ---
| @flowable/diagram-draw                            | Customized Flowable Diagram Draw for creating heatmaps
| @flowable/forms-react                             | Flowable Forms as React components
| bestzip                                           | Zipping distributables
| bluebird                                          | Promise library
| esnext                                            | Next generation ECMA script (needed in conjunction with code splitting)|
| node-sass                                         | Sass compiler
| path                                              | Resolving absolute paths
| prettier                                          | Styling the source code according to a set of rules
| reflect-metadata                                  | Polyfill for Metadata Reflection API (e.g. interpreting annotations)
| tslint                                            | Styling the source code according to a set of rules
| tslint-config-prettier                            | Prettier integration into tslint
| tslint-react                                      | React integration into tslint
| typescript                                        | Well, it's Typescript...
| tslib                                             | runtime library for TypeScript that contains all of the TypeScript helper functions

<a name="peerDependencies"></a>
## Peer dependencies of the addon

| Peer dependency                                   | Purpose
| ---                                               | ---
| moment                                            | Parsing, validating, manipulating, and formatting dates
| react                                             | Well, it's React...
| react-dom                                         | React library to manipulate the DOM
| whatwg-fetch                                      | A window.fetch polyfill to retrieve arbitrary data e.g. from a REST endpoint
