---
id: usecases-coloring
title: Coloring
---

Flowable Addon Analytics allows you to generate custom color scales which then can be used within the charts.

To do so, you have to provide an object called ```colors``` in your chart definition, like this:

```json
{
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "level": 0,
          "scheme": "Category10"
        }
      ]
    }
  }
}
```

The configuration above simply states that colors shall be distributed by groups and that the scheme ```Category10``` shall be used, where the later is a scheme defined in [D3-chromatic](https://github.com/d3/d3-scale-chromatic). 
If you don't use a predefined scheme, the Flowable color scheme (an interpolation between #1F2A44 and #E1523D) will be used. Likewise, if you don't use a mapping, 
```caption``` will be mapped automatically. If you skip the ```colors``` object entirely in your configuration, the Flowable color scale will be mapped to your ```caption``` 
object and the colors will be automatically distributed accordingly.

You can map any mappable object to colors. Mappable objects are ```caption```, ```value```, ```group```, ```weight``` and ```aggregations```.

For details, please refer to the [Colors section](basic-chart-definition.md#colors).

## Custom color scale
In order to create a custom color scale, all you have to do is defining an interpolation object, like below:

```json
{
  "colors": {
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#ff0000"
              },
              {
                "color": "#00ff00"
              },
              {
                "color": "#0000ff"
              },
              {
                "color": "#ff00ff"
              },
              {
                "color": "#ffff00"
              },
              {
                "color": "#00ffff"
              }
            ]
          },
          "level": 0,
          "scheme": "Custom"
        }
      ]
    }
  }
}
```

Note that you will also have to define the color scheme as ```Custom```.

Unless you define a mapping, the colors will be distributed evenly over the captions.

For example

[flw-addon-analytics]
{
  "chartContainerType": "plain-chart-container",
  "chartType": "pie-chart",
  "colors": {
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#ff0000"
              },
              {
                "color": "#00ff00"
              },
              {
                "color": "#0000ff"
              },
              {
                "color": "#ff00ff"
              },
              {
                "color": "#ffff00"
              },
              {
                "color": "#00ffff"
              }
            ]
          },
          "level": 0,
          "scheme": "Custom"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/pieCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 500,
  "id": "colorsCustomChart",
  "innerRadius": 0.1,
  "legend": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$caption"
        }
      ]
    }
  },
  "padding": {
    "bottom": 50
  },
  "outerRadius": 1,
  "ticks": {
    "textAnchor": "start"
  },
  "userResource": {
    "mode": "cors",
    "queryUrl": "currentUser.json"
  }
}
[/flw-addon-analytics]

## Threshold color scale
In case you would like to create a color scale that is conditional, you would have to define a threshold interpolation, like below:

```json
{
  "colors": {
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#9400D3",
                "condition": "{{$value > 0}}",
                "discriminator": "Fantastic"
              },
              {
                "color": "#4B0082",
                "condition": "{{$value > 0.1}}",
                "discriminator": "Great"
              },
              {
                "color": "#0000FF",
                "condition": "{{$value > 0.3}}",
                "discriminator": "Good"
              },
              {
                "color": "#00FF00",
                "condition": "{{$value > 0.5}}",
                "discriminator": "Still all right"
              },
              {
                "color": "#FFFF00",
                "condition": "{{$value > 0.7}}",
                "discriminator": "Need to track this"
              },
              {
                "color": "#FF7F00",
                "condition": "{{$value > 0.8}}",
                "discriminator": "What's going on?"
              },
              {
                "color": "#FF0000",
                "condition": "{{$value > 0.9}}",
                "discriminator": "Holy Moly!!!"
              }
            ]
          },
          "level": 0,
          "scheme": "Threshold"
        }
      ]
    }
  }
}
```

Note that you will have to define the color scheme as ```Threshold```.

The condition is a boolean expression. Thresholds expressions can only resolve numerical mapping objects, like ```$value```. You have to make sure that the 
conditions are in ascending order, otherwise the result is undefined. That means that the expression that compares to the lowest value must be first in the list 
and the expression that compares the largest value must be last in the list..

The discriminator is a string that describes the condition. Discriminators can be displayed e.g. as legend items along with their respective colors.

If you would like to use the discriminator in the legend, you would also have to change the mapping of the legend itself to map the colors object, like this:

```json
"legend": {
  "mapping": {
    "items": [
      {
        "level": 0,
        "mapping": "colors"
      }
    ]
  }
}
```

For example

[flw-addon-analytics]
{
  "chartContainerType": "axes-chart-container",
  "chartType": "bar-chart",
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$value"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#339966",
                "condition": "{{$value > -100}}",
                "discriminator": "Fantastic"
              },
              {
                "color": "#53C653",
                "condition": "{{$value > 0}}",
                "discriminator": "Great"
              },
              {
                "color": "#FFCC00",
                "condition": "{{$value > 20}}",
                "discriminator": "Still all right"
              },
              {
                "color": "#FF8533",
                "condition": "{{$value > 50}}",
                "discriminator": "Uh-oh"
              },
              {
                "color": "#CC0000",
                "condition": "{{$value > 100}}",
                "discriminator": "What's going on?"
              }
            ]
          },
          "level": 0,
          "scheme": "Threshold"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/barCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "id": "colorsThresholdChart",
  "height": 500,
  "legend": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "colors"
        }
      ]
    }
  },
  "userResource": {
    "mode": "cors",
    "queryUrl": "rest/user/response.json"
  }
}
[/flw-addon-analytics]

### Value color scale
In case you would like to create a color scale that distributes colors according to the value property, you would have to create a color definition like this:

```json
{
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$value"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#ff0000"
              },
              {
                "color": "#00ff00"
              },
              {
                "color": "#0000ff"
              },
              {
                "color": "#ff00ff"
              },
              {
                "color": "#ffff00"
              },
              {
                "color": "#00ffff"
              }
            ]
          },
          "level": 0,
          "scheme": "Custom"
        }
      ]
    }
  }
}
```

Note that you will have to define the color scheme as ```Custom```.

Also, you have to make sure that you define a mapping that refers to the value property of your datums.

For example

[flw-addon-analytics]
{
  "chartContainerType": "plain-chart-container",
  "chartType": "pie-chart",
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#ff0000",
                "discriminator": "Group 1"
              },
              {
                "color": "#00ff00",
                "discriminator": "Group 2"
              },
              {
                "color": "#0000ff",
                "discriminator": "Group 3"
              },
              {
                "color": "#ff00ff",
                "discriminator": "Group 4"
              },
              {
                "color": "#ffff00",
                "discriminator": "Group 5"
              },
              {
                "color": "#00ffff",
                "discriminator": "Group 6"
              }
            ]
          },
          "level": 0,
          "scheme": "Grouped"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/pieCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 500,
  "id": "colorsGroupedChart",
  "innerRadius": 0.1,
  "legend": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$caption"
        }
      ]
    }
  },
  "padding": {
    "bottom": 50
  },
  "outerRadius": 1,
  "ticks": {
    "textAnchor": "start"
  },
  "userResource": {
    "mode": "cors",
    "queryUrl": "currentUser.json"
  }
}
[/flw-addon-analytics]

## Grouped color scale
In case you would like to create a color scale that distributes colors according to the group property, you would have to create a color definition like this:

```json
{
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#ff0000",
                "discriminator": "Group 1"
              },
              {
                "color": "#00ff00",
                "discriminator": "Group 2"
              },
              {
                "color": "#0000ff",
                "discriminator": "Group 3"
              },
              {
                "color": "#ff00ff",
                "discriminator": "Group 4"
              },
              {
                "color": "#ffff00",
                "discriminator": "Group 5"
              },
              {
                "color": "#00ffff",
                "discriminator": "Group 6"
              }
            ]
          },
          "level": 0,
          "scheme": "Grouped"
        }
      ]
    }
  }
}
```

Note that you will have to define the color scheme as ```Grouped```.

Also, you have to make sure that you define a mapping that refers to the group property of your datums.

The discriminator is a string that describes the condition. Discriminators can be displayed e.g. as legend items along with their respective colors.

For example

[flw-addon-analytics]
{
  "chartContainerType": "plain-chart-container",
  "chartType": "pie-chart",
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": [
              {
                "color": "#ff0000",
                "discriminator": "Group 1"
              },
              {
                "color": "#00ff00",
                "discriminator": "Group 2"
              },
              {
                "color": "#0000ff",
                "discriminator": "Group 3"
              },
              {
                "color": "#ff00ff",
                "discriminator": "Group 4"
              },
              {
                "color": "#ffff00",
                "discriminator": "Group 5"
              },
              {
                "color": "#00ffff",
                "discriminator": "Group 6"
              }
            ]
          },
          "level": 0,
          "scheme": "Grouped"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/pieCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 500,
  "id": "colorsGroupedChart",
  "innerRadius": 0.1,
  "legend": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$caption"
        }
      ]
    }
  },
  "padding": {
    "bottom": 50
  },
  "outerRadius": 1,
  "ticks": {
    "textAnchor": "start"
  },
  "userResource": {
    "mode": "cors",
    "queryUrl": "currentUser.json"
  }
}
[/flw-addon-analytics]

### Weight color scale

### Aggregations color scale