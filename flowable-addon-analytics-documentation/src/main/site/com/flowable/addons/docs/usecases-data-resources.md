---
id: usecases-data-resources
title: Data resources
---

<a name="example"></a>
## Example data source

Throughout this documentation, we are going to use the subsequent data source as an example and show you how to map that very data source so that Flowable Addon Analytics is capable of rendering charts.

```json
[
  {
    "category": "lorem",
    "amount": 90,
    "gravity": 16,
    "group": "Group 1",
    "children": [
      {
        "category": "lorem 1",
        "amount": 20,
        "gravity": 2,
        "group": "Group 1",
        "children": [],
        "date": "21.01.2016"
      },
      {
        "category": "lorem 2",
        "amount": 30,
        "gravity": 10,
        "group": "Group 1",
        "children": [],
        "date": "26.02.2016"
      },
      {
        "category": "lorem 3",
        "amount": 40,
        "gravity": 4,
        "group": "Group 1",
        "children": [],
        "date": "07.03.2016"
      }
    ],
    "date": "21.01.2016"
  },
  ...
]
```

As you can see, this data source is of content type ``application/json; charset=utf-8`` and holds an array of arbitrary objects (datums) which **all feature the same properties**.

Furthermore, it covers several levels of aggregations. Aggregations are necessary if you plan to implement drilling capabilities in your chart or if you would like to create a 
stacked chart.

## Properties

The following table aims to give you an idea on the usage of each property you would have to map to your data source so that it can be used by Flowable Addon Analytics.

| Name              | Description
| ---               | ---
| \***caption**     | Captions are a very abstract concept and used in a variety of ways by the addon. Think of the captions as a categories into which you subdivide your data. Usually, captions are assigned to the X-axis of a chart with axes. *However, assigning captions to the X-axis is by no means an absolute necessity, especially because ot all charts feature axes and is only mentioned here for the sake of simplicity.* While a bar chart might be subdivided into categories such as e.g. countries, a line chart which also features axes often takes a time line as its X-axis. The captions in a bar chart might be the ``category`` property while in a line chart might be the ``date`` property in the example data source above. A pie chart, however, does not feature axes at all, but does very well feature captions. The captions are represented as arcs rather than ticks on an axis. Last but not least, captions do have a data type, a.k.a. a domain such as ``string``, ``numeric`` or ``date``. Depending on the domain, whichever property you might map as the caption might need parsing a/o formatting prior to be rendered onto the chart. For further information about captions, please refer to the [caption](basic-chart-definition.md#caption) section of the chart definition page.
| \***value**       | Values are, as the name suggests it, the properties that define the width, height a/o position of a particular element within the chart. E.g. while a value property defines the height of a bar within a bar chart, it defines the position of bends on a line within a line chart. Usually, captions are assigned to the Y-axis of a chart with axes. *However, assigning values to the Y-axis is by no means an absolute necessity, especially because ot all charts feature axes and is only mentioned here for the sake of simplicity.* The values might be the ``amount`` property in the example data source above. For example, the value is represented by the width of an arc within a pie chart. Values must be numeric. If you map a property that does not have a numeric domain, the rendering result is undefined. For further information about values, please refer to the [value](basic-chart-definition.md#value) section of the chart definition page.
| aggregations      | Aggregations are subdivisions in your data source. Aggregations are optional and only necessary if you wish to have drilling capabilities or stacked elements in your chart. They must themselves be arrays of arbitrary objects (datums) that all feature the same properties as their parent objects. That is because aggregated objects, just as their parent objects, must be mappable to captions and values, however, must not necessarily have the same domains as their parents. In fact, each level might have different captions that are, however, all available via the same property name. The aggregations might be the ``children`` property in the example data source above. For further information about aggregations, please refer to the [aggregations](basic-chart-definition.md#aggregations) section of the chart definition page.
| group             | Groups are just as captions a rather abstract concept. They classify several objects throughout the entire data source together. Think of groups as a means to assign common properties as opposed to captions whose goal it is to assign an individual property to each datum. The aggregations might be the ``group`` property in the example data source above. It covers datums that are grouped into two groups called ``Group 1`` and ``Group 2``. In the chart definition, you can e.g. later define that all datums classified as ``Group 1`` are rendered in red while the others are rendered in green. Finally, groups do have a data type, a.k.a. a domain such as ``string``, ``numeric`` or ``date``. Depending on the domain, whichever property you might map as the group might need parsing a/o formatting prior to be used within the chart. For further information about groups, please refer to the [group](basic-chart-definition.md#group) section of the chart definition page.
| weight            | Weights are additional values that might be used to represent a second dimension of values within your chart. The weights might be the ``gravity`` property in the example data source above. For example, within a scatter chart, the values might be assigned to the Y-axis and therefore determines the position of an element, however, the weights determines the diameters of each circle. It stands to reason that weights must have a numeric domain. If you map a property that does not have a numeric domain, the rendering result is undefined. For further information about weights, please refer to the [weight](basic-chart-definition.md#weight) section of the chart definition page.

## Mapping captions to the x-axis

If you want to map your mappable object ```caption``` to the x-axis of your chart, you can refer to the mappable object through the ```$caption``` 
expression.

```json
{
  "axes": {
    "x": {
      "mapping": {
        "items": [
          {
            "level": 0,
            "mapping": "$caption"
          }
        ]
      }
    }
  }
}
```

## Mapping groups to the legend

If you want to map your mappable object ```group``` to the legend of your chart, rather than the ```caption```s. That is, because you might have a large number of captions or 
a time axis with lots of dates or timestamps which simply does not make any sense to have in a legend.

In such cases, you can refer to the mappable object ```group``` through the ```$group``` expression.

```json
{
  "legend": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        },
        {
          "level": 1,
          "mapping": "$caption"
        }
      ]
    }
  }
}
```

You likely also want to map the mappable object ```group``` to the colors, so that the colors will be spread across the groups.

For example:
```json
{
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    },
    "scheme": {
      "items": [
        {
          "interpolation": {
            "items": []
          },
          "level": 0,
          "scheme": "Category10"
        }
      ]
    }
  }
}
```

