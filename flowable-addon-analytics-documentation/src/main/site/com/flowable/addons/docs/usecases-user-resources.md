---
id: usecases-user-resources
title: User resources, formatting and parsing
---

Flowable Addon Analytics allows you to automatically format numbers, dates, times as well as strings. Furthermore, it is also possible to 
define mappings in multiple languages, e.g. for the chart title.

All these configurations are bound to the user resource which defines what locale and what language shall be used. It is a simple JSON object 
that defines exactly two properties, as stated in [User resources](basic-user-resource.md).

Language and locale are independent from one another. It is possible to define the language as German (```de```), but the locale as English US 
(```en-US```).

## Parsing dates and times
If you have dates and / or times as properties in your datums, you likely need to format those. However, since you probably receive those as strings which might 
have a format that is either not a standard date, time or datetime format or which is provided in a format that is not useful for displaying in your chart (because 
you would like to show them in the user's native locale), you will have to parse these properties prior to formatting them.

To do so, you will have to provide any of ```dateParser```, ```timeParser``` and / or ```numberParser``` 
with any mappable object.

For example

```json
{
  "value": {
    "numberParser": ".2f"
  }
}
```

This would parse all values with a precision of two digits following the decimal point.

The syntax of the number parser can be found [here](https://github.com/d3/d3-format#locale_format).

```json
{
  "caption": {
    "domain": {
      "items": [
        {
          "domain": "date",
          "level": 0
        }
      ]
    },
    "dateParser": "%d.%m.%Y",
    "timeParser": "%H:%M"
  }
}
```

This would parse all captions according to the German date format which is day.month.year and the time format that consists of the hour and the minute.

## Change date and time formatting

If you would like to change the formatting dates or times used for ticks of axes, you can simply supply Flowable Addon Analytics with a change in the user object, like this:

```json
{
  "language": "en", // default
  "locale": "de-CH" // change
}
```

Since the formatting of the axes is bound to the user's locale, the dates shown on the time axis will be formatted according to the chosen locale, in this case Swiss-German (```de-CH```).

For example

[flw-addon-analytics]
{
  "axes": {
    "x": {
      "scaleType": "time",
      "ticks": {
        "interval": {
          "items": [
            {
              "level": 0,
              "type": "monthsInYear"
            }
          ]
        }
      }
    }
  },
  "caption": {
    "dateParser": "%d.%m.%Y",
    "domain": {
      "items": [
        {
          "domain": "date",
          "level": 0
        }
      ]
    },
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "date"
        }
      ]
    }
  },
  "chartContainerType": "axes-chart-container",
  "chartType": "area-chart",
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/areaCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 500,
  "legend": {
    "angle": -90
  },
  "id": "userResourceDateFormattingChart1",
  "userResource": {
    "mode": "cors",
    "queryUrl": "rest/user/response.json"
  }
}
[/flw-addon-analytics]

as opposed to

[flw-addon-analytics]
{
  "axes": {
    "x": {
      "scaleType": "time",
      "ticks": {
        "interval": {
          "items": [
            {
              "level": 0,
              "type": "monthsInYear"
            }
          ]
        }
      }
    }
  },
  "caption": {
    "dateParser": "%d.%m.%Y",
    "domain": {
      "items": [
        {
          "domain": "date",
          "level": 0
        }
      ]
    },
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "date"
        }
      ]
    }
  },
  "chartContainerType": "axes-chart-container",
  "chartType": "area-chart",
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/areaCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 500,
  "legend": {
    "angle": -90
  },
  "id": "userResourceDateFormattingChart2",
  "userResource": {
    "mode": "cors",
    "queryUrl": "rest/user2/response.json"
  }
}
[/flw-addon-analytics]

***Caution***: If you have a format defined in your chart definition, it will take preference over the locale from the user resource!
For example, if your captions are dates and you defined a ```dateFormat``` in the caption definition, the date format will take 
preference and only changing the locale will have no effect.

## Override number, date and time formats
Numbers, dates and times can be formatted in multiple ways. While dates and times are well defined depending on the locale used, numbers can be formatted in 
arbitrary ways.

You can override any format, whether it is a date, a time or a number. To do so, you will have to provide any of ```dateFormat```, ```timeformat``` and / or ```numberFormat``` 
with any mappable object.

For example

```json
{
  "value": {
    "numberFormat": ".2f"
  }
}
```

This would format all values with a precision of two digits following the decimal point.

The syntax of the number format can be found [here](https://github.com/d3/d3-format#locale_format).

```json
{
  "caption": {
    "domain": {
      "items": [
        {
          "domain": "date",
          "level": 0
        }
      ]
    },
    "dateFormat": "%d.%m.%Y",
    "timeFormat": "%H:%M"
  }
}
```

This would format all captions according to the German date format which is day.month.year and the time format that consists of the hour and the minute.

***Caution***: If you have a format defined in your chart definition, it will take preference over the locale from the user resource!
For example, if your captions are dates and you defined a ```dateFormat``` in the caption definition, the date format will take 
preference and only changing the locale will have no effect.

The syntax of the date and time formats can be found [here](https://github.com/d3/d3-time-format#locale_format).

## String formatting
TODO

## Change language used within chart

If you would like to change the language used for the chart title, you can simply supply Flowable Addon Analytics with a change in the 
user object, like this:

```json
{
  "language": "de", // change
  "locale": "en-US" // default
}
```

Since the language of the chart title is bound to the user's language, the German title (```de```) will be used.

For example

[flw-addon-analytics]
{
  "axes": {
    "x": {
      "scaleType": "time",
      "ticks": {
        "interval": {
          "items": [
            {
              "level": 0,
              "type": "monthsInYear"
            }
          ]
        }
      }
    }
  },
  "caption": {
    "dateParser": "%d.%m.%Y",
    "domain": {
      "items": [
        {
          "domain": "date",
          "level": 0
        }
      ]
    },
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "date"
        }
      ]
    }
  },
  "chartContainerType": "axes-chart-container",
  "chartType": "area-chart",
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/areaCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 500,
  "legend": {
    "angle": -90
  },
  "id": "userResourceDateLanguageChart1",
  "userResource": {
    "mode": "cors",
    "queryUrl": "rest/user/response.json"
  }
}
[/flw-addon-analytics]

as opposed to

[flw-addon-analytics]
{
  "axes": {
    "x": {
      "scaleType": "time",
      "ticks": {
        "interval": {
          "items": [
            {
              "level": 0,
              "type": "monthsInYear"
            }
          ]
        }
      }
    }
  },
  "caption": {
    "dateParser": "%d.%m.%Y",
    "domain": {
      "items": [
        {
          "domain": "date",
          "level": 0
        }
      ]
    },
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "date"
        }
      ]
    }
  },
  "chartContainerType": "axes-chart-container",
  "chartType": "area-chart",
  "colors": {
    "mapping": {
      "items": [
        {
          "level": 0,
          "mapping": "$group"
        }
      ]
    }
  },
  "dataResources": {
    "items": [
      {
        "level": 0,
        "dataResource": {
          "isEnabled": true,
          "mode": "cors",
          "queryUrl": "rest/areaCharts/response.json",
          "type": "rest"
        }
      }
    ]
  },
  "height": 500,
  "legend": {
    "angle": -90
  },
  "id": "userResourceDateLanguageChart2",
  "userResource": {
    "mode": "cors",
    "queryUrl": "rest/user2/response.json"
  }
}
[/flw-addon-analytics]