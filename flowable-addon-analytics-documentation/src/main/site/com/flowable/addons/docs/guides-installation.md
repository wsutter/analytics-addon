---
id: guides-installation
title: Installation
---
There are two different packages of Flowable Addon Analytics available, both hosted on the [Artifactory Repository](https://repo.edorasware.com/webapp/#/artifacts/browse/tree/General/edoras-npm-repo). 

### React build (@flowable/addon-analytics-react)

The react build is packaged as an ES6 UMD module. You can therefore import all or only single components of the addon into your project.

### Vanilla build (@flowable/addon-analytics-vanilla)

The vanilla build is packaged so that the variable "flwAddonAnalytics" is globally available through which instances of a chart can be created. This very documentation 
uses the vanilla build for presenting examples.

## Configuring .npmrc

In order to install you need to set up your credentials.

You need to add the edoras repository and your credentials to your `.npmrc` file. You can add these lines to the `.npmrc` located in your user directory.

```
@flowable:registry=https://repo.edorasware.com/api/npm/edoras-npm-repo/
//https://repo.edorasware.com/api/npm/edoras-npm-repo/:username=YOUR_USERNAME
//https://repo.edorasware.com/api/npm/edoras-npm-repo/:email=YOUR_EMAIL
//https://repo.edorasware.com/api/npm/edoras-npm-repo/:always-auth=true
//https://repo.edorasware.com/api/npm/edoras-npm-repo/:_auth=AUTH
```

You need to replace the `AUTH`, `YOUR_USERNAME` and `YOUR_EMAIL` with your personal data.

To get the `AUTH` value, please go to [Artifactory Repository](https://repo.edorasware.com/webapp/#/artifacts/browse/tree/General/edoras-npm-repo)
and click on "Set Me Up". Then write your password in the top-right and click on the (->) icon. After that you will find `AUTH` in the
"Using basic authentication" section in a line like this:

```
_auth = AUTH
```

## Installation

Once you have your credentials you can install the artifacts by means of `yarn` or `npm`, resp.

If your purpose is to embed it in your React-based app, install it with:

```
yarn add @flowable/analytics-addon-react
```

Otherwise, install it with:

```
yarn add @flowable/analytics-addon-vanilla
```

Optional: if you are using _TypeScript_ in your project, install the React types with:

```
yarn add @types/react --dev
```

Flowable Addon Analytics provides its own typings.d.ts that is pulled in automatically if you have a _TypeScript_ setup.