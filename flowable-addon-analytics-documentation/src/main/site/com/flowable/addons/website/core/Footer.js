"use strict";
var __extends =
  (this && this.__extends) ||
  (function() {
    var extendStatics = function(d, b) {
      extendStatics =
        Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array &&
          function(d, b) {
            d.__proto__ = b;
          }) ||
        function(d, b) {
          for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        };
      return extendStatics(d, b);
    };
    return function(d, b) {
      extendStatics(d, b);
      function __() {
        this.constructor = d;
      }
      d.prototype =
        b === null
          ? Object.create(b)
          : ((__.prototype = b.prototype), new __());
    };
  })();
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var packageJson = require("../package.json");
var React = require("react");
var Footer = /** @class */ (function(_super) {
  __extends(Footer, _super);
  function Footer() {
    return (_super !== null && _super.apply(this, arguments)) || this;
  }
  Footer.prototype.docUrl = function(doc, language) {
    var baseUrl = this.props.config.baseUrl;
    return baseUrl + "docs/" + (language ? language + "/" : "") + doc;
  };
  Footer.prototype.render = function() {
    return React.createElement(
      "footer",
      { className: "nav-footer", id: "footer" },
      React.createElement(
        "section",
        { className: "sitemap" },
        React.createElement(
          "div",
          null,
          React.createElement("h5", null, "Docs"),
          React.createElement(
            "a",
            { href: this.docUrl("introduction.html", this.props.language) },
            "Guides"
          )
        ),
        React.createElement(
          "div",
          null,
          React.createElement("h5", null, "Private"),
          React.createElement(
            "a",
            { href: packageJson.repository.url },
            "Source"
          ),
          React.createElement(
            "a",
            { href: packageJson.storybook.url },
            "Storybook"
          )
        )
      ),
      React.createElement(
        "section",
        { className: "copyright" },
        React.createElement(
          "a",
          { href: this.props.config.baseUrl, className: "nav-home" },
          this.props.config.footerIcon &&
            React.createElement(
              "div",
              null,
              React.createElement("img", {
                src: this.props.config.baseUrl + this.props.config.footerIcon,
                alt: this.props.config.title,
                width: "66",
                height: "58"
              })
            )
        ),
        this.props.config.copyright
      )
    );
  };
  return Footer;
})(React.Component);
module.exports = Footer;
//# sourceMappingURL=Footer.js.map
