/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import * as packageJson from "../package.json";
import * as React from "react";

class Footer extends React.Component {
  docUrl(doc: any, language: any) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + "docs/" + (language ? language + "/" : "") + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        <section className="sitemap">
          <div>
            <h5>Docs</h5>
            <a href={this.docUrl("introduction.html", this.props.language)}>
              Guides
            </a>
          </div>
          <div>
            <h5>Private</h5>
            <a href={packageJson.repository.url}>Source</a>
            <a href={packageJson.storybook.url}>Storybook</a>
          </div>
        </section>
        <section className="copyright">
          <a href={this.props.config.baseUrl} className="nav-home">
            {this.props.config.footerIcon && (
              <div>
                <img
                  src={this.props.config.baseUrl + this.props.config.footerIcon}
                  alt={this.props.config.title}
                  width="66"
                  height="58"
                />
              </div>
            )}
          </a>
          {this.props.config.copyright}
        </section>
      </footer>
    );
  }
}

module.exports = Footer;
