/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require("react");

const CompLibrary = require("../../core/CompLibrary.js");

const Container = CompLibrary.Container;

const siteConfig = require(`${process.cwd()}/siteConfig.js`);

class Users extends React.Component {
  render() {
    if ((siteConfig.users || []).length === 0) {
      return null;
    }

    const editUrl = `${siteConfig.repoUrl}/edit/master/website/siteConfig.js`;
    const showcase = siteConfig.users.map(user => (
      <a href={user.infoLink} key={user.infoLink}>
        <img
          src={user.image}
          alt={user.caption}
          title={user.caption}
          style={{ width: "100%", minWidth: 300 }}
        />
      </a>
    ));

    return (
      <div className="mainContainer">
        <Container padding={["bottom", "top"]}>
          <div className="showcaseSection">
            <div className="prose">
              <h1>Who is using addon?</h1>
              <p>This project is used by many companies</p>
            </div>
            <div className="logos">{showcase}</div>
            <p>Are you using this addon?</p>
            <a href={"mailto:info@flowable.com"} className="button">
              Contact us to have your company added here
            </a>
          </div>
        </Container>
      </div>
    );
  }
}

module.exports = Users;
