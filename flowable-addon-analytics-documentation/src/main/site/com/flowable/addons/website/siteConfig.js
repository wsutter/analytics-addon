"use strict";
exports.__esModule = true;
/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var packageJson = require("./package.json");
var FlowableAnalyticsRemarkablePlugin = require("./md-plugins/FlowableAnalytics");
var EmojiRemarkablePlugin = require("remarkable-emoji");
var users = [
    {
        caption: "Flowable",
        image: "img/flowable.svg",
        infoLink: "https://www.flowable.com",
        pinned: true
    }
];
var siteConfig = {
    title: packageJson.title,
    tagline: packageJson.tagline,
    url: packageJson.repository.url,
    baseUrl: "/",
    projectName: packageJson.name,
    headerLinks: [
        {
            doc: "guides-intro",
            label: "Guides"
        },
        {
            href: packageJson.storybook.url,
            label: "Storybook"
        },
        {
            search: true
        },
        {
            languages: true
        }
    ],
    users: users,
    headerIcon: "img/flowableAddonAnalytics.white.svg",
    footerIcon: "img/flowableAddonAnalytics.white.svg",
    favicon: "img/favicon/favicon.ico",
    colors: {
        primaryColor: "#1e87f0",
        secondaryColor: "#107ae5"
    },
    fonts: {
        montserrat: ["montserrat-medium-webfont", "sans-serif"]
    },
    copyright: packageJson.license,
    organizationName: packageJson.author,
    highlight: {
        theme: "default"
    },
    markdownPlugins: [FlowableAnalyticsRemarkablePlugin, EmojiRemarkablePlugin],
    ogImage: "img/flowable.svg",
    twitter: true,
    twitterUsername: packageJson.twitter.twitterUsername,
    twitterImage: "img/flowable.svg",
    scripts: [
        "/js/flwAddonAnalytics.js",
        "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.36.0/codemirror.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.36.0/mode/javascript/javascript.js",
        "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.36.0/addon/lint/json-lint.js"
    ],
    scrollToTop: true,
    repoUrl: packageJson.repository.url,
    onPageNav: "separate",
    usePrism: true
};
module.exports = siteConfig;
