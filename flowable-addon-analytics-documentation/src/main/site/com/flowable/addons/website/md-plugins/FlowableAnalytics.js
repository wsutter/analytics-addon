const uuid = require("uuid");
const camelCase = require("lodash.camelcase");

const TOKEN = "flw-addon-analytics";

const getLine = (state, line) => {
  const pos = state.bMarks[line] + state.blkIndent;
  const max = state.eMarks[line];

  return state.src.substr(pos, max - pos);
};

function parseBlock(state, startLine, endLine) {
  if (getLine(state, startLine) === `[${TOKEN}]`) {
    const startPgn = startLine + 1;
    let nextLine = startPgn;
    while (nextLine < endLine) {
      if (getLine(state, nextLine) === `[/${TOKEN}]`) {
        state.tokens.push({
          type: TOKEN,
          content: state.getLines(startPgn, nextLine, state.blkIndent, true),
          block: true,
          lines: [startLine, nextLine],
          level: state.level
        });
        state.line = nextLine + 1;
        return true;
      }
      nextLine++;
    }
  }
  return false;
}

const renderBlock = (md, content) => {
  debugger;
  const obj = JSON.parse(content);
  const definition = obj;
  const uid = uuid();
  const chartType = camelCase(JSON.parse(content).chartType);
  return `
  <div class="analytics-showcase">
  <div id="${uid}" class="analytics-demo"></div>
  <textarea id="definition-${uid}" class="analytics-definition" readonly>
    ${JSON.stringify(
    obj,
    null,
    2
  )}
  </textarea>
  </div>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/react/16.6.3/umd/react.production.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/react-dom/16.6.3/umd/react-dom.production.min.js"></script>
  <script>
  (function(){
    const chart = React.createElement(
      flwAddonAnalytics["${chartType}"],
    {
     config: ${JSON.stringify(definition)}
    });
    ReactDOM.render(chart, document.getElementById("${uid}"));
    const defEl = document.getElementById("definition-${uid}");
    CodeMirror.fromTextArea(defEl, {
      mode: {name: "javascript", jsonld: true}, 
      theme: "ambiance"
    });
  })()
  </script>`;
};

module.exports = md => {
  debugger;
  md.block.ruler.before("code", TOKEN, parseBlock, { alt: [] });
  md.renderer.rules[TOKEN] = (tokens, idx) =>
    renderBlock(md, tokens[idx].content, tokens[idx].block);
};
