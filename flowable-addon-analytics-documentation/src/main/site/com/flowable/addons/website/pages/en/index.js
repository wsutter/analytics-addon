/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
const React = require("react");

const CompLibrary = require("../../core/CompLibrary.js");

const MarkdownBlock = CompLibrary.MarkdownBlock;
/* Used to read markdown */
const Container = CompLibrary.Container;
const GridBlock = CompLibrary.GridBlock;

const siteConfig = require(`${process.cwd()}/siteConfig.js`);

function imgUrl(img) {
  return `${siteConfig.baseUrl}img/${img}`;
}

function docUrl(doc, language) {
  return `${siteConfig.baseUrl}docs/${language ? `${language}/` : ""}${doc}`;
}

function pageUrl(page, language) {
  return siteConfig.baseUrl + (language ? `${language}/` : "") + page;
}

class Button extends React.Component {
  render() {
    return (
      <div className="pluginWrapper buttonWrapper">
        <a className="button" href={this.props.href} target={this.props.target}>
          {this.props.children}
        </a>
      </div>
    );
  }
}

Button.defaultProps = {
  target: "_self"
};

const SplashContainer = props => (
  <div className="homeContainer">
    <div className="homeSplashFade">
      <div className="wrapper homeWrapper">{props.children}</div>
    </div>
  </div>
);

const ProjectTitle = () => (
  <h2 className="projectTitle">
    {siteConfig.title}
    <small>{siteConfig.tagline}</small>
  </h2>
);

const PromoSection = props => (
  <div className="section promoSection">
    <div className="promoRow">
      <div className="pluginRowBlock">{props.children}</div>
    </div>
  </div>
);

class HomeSplash extends React.Component {
  render() {
    const language = this.props.language || "";
    return (
      <SplashContainer>
        <div className="inner">
          <ProjectTitle />
          <PromoSection>
            <Button href="https://bamboo.edorasware.com/browse/FLOW-FAA/latest/artifact/JOB1/storybook-static/index.html">
              Try It Out
            </Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

const Block = props => (
  <Container
    padding={["bottom", "top"]}
    id={props.id}
    background={props.background}
  >
    <GridBlock align="center" contents={props.children} layout={props.layout} />
  </Container>
);

const Features = () => (
  <Block layout="threeColumn">
    {[
      {
        content: "",
        image: imgUrl("features/areaCharts.png"),
        imageAlign: "top",
        title: "area charts"
      },
      {
        content: "",
        image: imgUrl("features/barCharts.png"),
        imageAlign: "top",
        title: "bar charts"
      },
      {
        content: "",
        image: imgUrl("features/lineCharts.png"),
        imageAlign: "top",
        title: "line charts"
      },
      {
        content: "",
        image: imgUrl("features/gaugeCharts.png"),
        imageAlign: "top",
        title: "gauge charts"
      },
      {
        content: "",
        image: imgUrl("features/pieCharts.png"),
        imageAlign: "top",
        title: "pie charts"
      },
      {
        content: "",
        image: imgUrl("features/radarCharts.png"),
        imageAlign: "top",
        title: "radar charts"
      },
      {
        content: "",
        image: imgUrl("features/scatterCharts.png"),
        imageAlign: "top",
        title: "scatter charts"
      }
    ]}
  </Block>
);

const What = () => (
  <div
    className="productShowcaseSection paddingBottom"
    style={{ textAlign: "center" }}
  >
    <h2>What is it?</h2>
    <MarkdownBlock>
      This project is a commercial addon to Flowable Platform, Flowable Work or
      Flowable Engage and features interactive, declarative charts. Currently,
      the addon supports area, bar, line, gague, pie, radar and scatter charts.
    </MarkdownBlock>
  </div>
);

const How = () => (
  <Block background="light">
    {[
      {
        content:
          'Check out our <a href="https://bamboo.edorasware.com/browse/FLOW-FAA/latest/artifact/JOB1/storybook-static/index.html">storybook</a> and fiddle around with it. Change ' +
          "either the data sources or the definitions and see how the chart adapts " +
          "itself automatically. Furthermore, we encourage you to participate in " +
          "our Academy where you can learn how to use the addon in conjunction with " +
          "the Flowable Modeller and integrate it into new or existing forms.",
        image: imgUrl("flowableAddonAnalytics.svg"),
        imageAlign: "right",
        title: "How can I use it?"
      }
    ]}
  </Block>
);

const Why = () => (
  <Block background="dark">
    {[
      {
        content:
          "This addon is for you if you want to integrate interactive charts " +
          "into your forms. It allows you to not only present the data from various data sources " +
          "graphically on one chart but also drill down into specific elements of your charts.",
        image: imgUrl("flowableAddonAnalytics.white.svg"),
        imageAlign: "left",
        title: "Why should I use it?"
      }
    ]}
  </Block>
);

const Showcase = props => {
  if ((siteConfig.users || []).length === 0) {
    return null;
  }

  const showcase = siteConfig.users
    .filter(user => user.pinned)
    .map(user => (
      <a href={user.infoLink} key={user.infoLink}>
        <img
          src={user.image}
          alt={user.caption}
          title={user.caption}
          style={{ width: "100%", minWidth: 300 }}
        />
      </a>
    ));

  return (
    <div className="productShowcaseSection paddingBottom">
      <h2>Who is Using This?</h2>
      <p>This project is used by all these people</p>
      <div className="logos">{showcase}</div>
      <div className="more-users">
        <a className="button" href={pageUrl("users.html", props.language)}>
          More {siteConfig.title} Users
        </a>
      </div>
    </div>
  );
};

class Index extends React.Component {
  render() {
    const language = this.props.language || "";

    return (
      <div>
        <HomeSplash language={language} />
        <div className="mainContainer">
          <Features />
          <What />
          <How />
          <Why />
          <Showcase language={language} />
        </div>
      </div>
    );
  }
}

module.exports = Index;
