# flowable addon analytics

This project consists of various charts such as bar charts, pie charts, line charts etc., bundled as ''@flowable/addon-analytics''.
It can be used to create e.g. on a dashboard in collaboration with flowable core, flowable platform or flowable work as its backend.

Each chart is created as a React component based upon D3.js. Please refer to the documentation of ''flowable addon analytics'', 
React or D3.js for any further details.

Integration
-----------

Please refer to section ''Operator Guide'' in the addon's documentation if you would like to integrate the flowable addon analytics into your project.

Development
-----------

## Requirements

* **edoras-npm-repo**: Access to edoras-npm-repo
* **nodejs / yarn**: Install nodejs > 8.0.0 and yarn > 1.7.0 locally

## Know how (for developers)

You need to know about:

* @flowable/forms-react ^1.1.49
* typescript ^3.0.3
* react ^16.6.0
* d3.js ^5.4.0
* recharts ^1.3.3
* lodash ^4.17.0
* bluebird ^3.5.0
* inversify ^5.0.1

Also, it might be useful to know about

* spin.js ^3.1.0
* moment ^2.22.0
* node-sass ^4.9.0
* babel ^7.0.0
* webpack ^4.15.0

Flowable addon analytics was bootstrapped by means of [Create React App](https://github.com/facebookincubator/create-react-app).
For demo as well as testing purposes, it uses [storybook.js](https://storybook.js.org/).

Documentation
-------------

In case you further develop this addon, please do not forget to document your changes accordingly.

Want to help?
-------------

Want to file a bug, contribute some code, or improve documentation? Excellent!

## Bugs
Please file any bugs or new requirements in our [Addon board](https://jira.edorasware.com/projects/ADDON).

## Merge requests
In case you fix a bug or implement new requirements, please file a pull request in [Bitbucket](https://stash.edorasware.com/projects/SOLUTIONS/repos/edoras-addon-analytics/pull-requests) and add 
Walter Sutter as a reviewer.

## Contact
Feel free to contact [Walter Sutter](mailto:walter.sutter@flowable.com).