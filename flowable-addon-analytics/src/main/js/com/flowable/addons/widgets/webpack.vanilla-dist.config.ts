import * as CleanWebpackPlugin from "clean-webpack-plugin";
import CopyWebpackPlugin from "copy-webpack-plugin";
import * as MiniCSSExtractPlugin from "mini-css-extract-plugin";
import * as TerserPlugin from "terser-webpack-plugin";
import DtsGeneratorPlugin from "dts-generator-webpack-plugin";
import * as GeneratePackageJsonPlugin from "generate-package-json-webpack-plugin";
import { LicenseWebpackPlugin } from "license-webpack-plugin";
import * as CircularDependencyPlugin from "circular-dependency-plugin";
import * as EventPlugin from "webpack-event-plugin";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import * as webpack from "webpack";
import * as path from "path";
import * as fs from "fs-extra";
import { TsconfigPathsPlugin } from "tsconfig-paths-webpack-plugin";

const packageJson = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, "package.json"), "utf8")
);

const externals = {
  flwformsVanilla: "@flowable/forms-react"
};

let outFilenameIndex =
  process.argv.findIndex((argument: string) => {
    return argument === "--output-filename";
  }) + 1;

const getWebpackConfig = (name: string): webpack.Configuration => {
  return {
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx", ".scss", ".css"],
      plugins: [
        new TsconfigPathsPlugin({
          configFile: path.resolve(__dirname, "src", "tsconfig.json"),
          logLevel: "INFO"
        })
      ]
    },
    target: "web",
    entry: {
      flwAddonAnalytics: path.resolve(__dirname, "src/index"),
      "areaChart/index": path.resolve(
        __dirname,
        "src/factories/areaChartFactory"
      ),
      "barChart/index": path.resolve(
        __dirname,
        "src/factories/barChartFactory"
      ),
      "gaugeChart/index": path.resolve(
        __dirname,
        "src/factories/gaugeChartFactory"
      ),
      "heatmapChart/index": path.resolve(
        __dirname,
        "src/factories/heatmapChartFactory"
      ),
      "lineChart/index": path.resolve(
        __dirname,
        "src/factories/lineChartFactory"
      ),
      "pieChart/index": path.resolve(
        __dirname,
        "src/factories/pieChartFactory"
      ),
      "radarChart/index": path.resolve(
        __dirname,
        "src/factories/radarChartFactory"
      ),
      "scatterChart/index": path.resolve(
        __dirname,
        "src/factories/scatterChartFactory"
      )
    },
    externals: externals,
    mode: "production",
    output: {
      path: path.resolve(__dirname, "build/dist/"),
      filename: "[name].js",
      library: "flwAddonAnalytics",
      libraryTarget: "var",
      umdNamedDefine: true
    },
    optimization: {
      occurrenceOrder: true,
      noEmitOnErrors: true,
      minimize: true,
      minimizer: [
        new TerserPlugin({
          test: /\.js$/,
          exclude: /\/node_modules/,
          cache: true,
          parallel: true,
          terserOptions: {
            warnings: true,
            ie8: false,
            compress: {
              drop_console: true,
              drop_debugger: true,
              warnings: true
            },
            output: {
              comments: false,
              beautify: false
            }
          }
        })
      ],
      mergeDuplicateChunks: false
    },
    module: {
      rules: [
        {
          test: /\.html$/,
          exclude: [/node_modules/],
          use: {
            loader: "html-loader",
            options: {
              minimize: true
            }
          }
        },
        {
          test: /\.s?css$/,
          exclude: [/node_modules/],
          use: [
            {
              loader: "file-loader"
            },
            {
              loader: MiniCSSExtractPlugin.loader as string
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                minimize: true,
                importLoaders: 1
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        },
        {
          test: /\.tsx?$/,
          exclude: [
            /node_modules/,
            /test/,
            /mocks/,
            /\.(test|spec|e2e)\.tsx?$/
          ],
          use: [
            {
              loader: "ts-loader",
              options: {
                transpileOnly: true,
                logLevel: "info",
                silent: true
              }
            }
          ]
        },
        {
          test: /\.jsx$/,
          exclude: [
            /node_modules/,
            /test/,
            /mocks/,
            /\.(test|spec|e2e)\.jsx?$/
          ],
          use: [
            {
              loader: "babel-loader",
              options: {
                presets: [
                  [
                    "@babel/preset-env",
                    {
                      targets: {
                        browsers: [">0.25%", "ie >= 11", "not op_mini all"]
                      },
                      useBuiltIns: "usage",
                      modules: "umd",
                      debug: false
                    }
                  ],
                  ["@babel/preset-react"]
                ],
                plugins: [
                  "@babel/plugin-syntax-dynamic-import",
                  "transform-promise-to-bluebird"
                ],
                cacheDirectory: true
              }
            }
          ]
        },
        {
          test: /\.(png|jpg)$/,
          use: [
            {
              loader: "file-loader?name=images/[name].[ext]"
            }
          ]
        },
        {
          test: require.resolve("react"),
          use: [
            {
              loader: "expose-loader",
              options: "react"
            }
          ]
        },
        {
          test: require.resolve("react-dom"),
          use: [
            {
              loader: "expose-loader",
              options: "reactDom"
            }
          ]
        },
        {
          test: require.resolve("moment"),
          use: [
            {
              loader: "expose-loader",
              options: "moment"
            }
          ]
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(path.resolve(__dirname, "build"), {
        verbose: true
      }),
      new MiniCSSExtractPlugin({
        filename: "flwAddonAnalytics.min.css"
      }),
      new DtsGeneratorPlugin({
        name: name,
        project: path.resolve("."),
        out: path.resolve(__dirname, "build/dist"),
        exclude: [
          path.resolve(__dirname, "webpack.*"),
          path.resolve(__dirname, "node_modules/**/*"),
          path.resolve(__dirname, "test/**/*"),
          path.resolve(__dirname, "mocks/**/*"),
          path.resolve(__dirname, "jest*"),
          path.resolve(__dirname, "setupJest*"),
          path.resolve(__dirname, "licenses/**/*"),
          path.resolve(__dirname, "build/**/*")
        ],
        resolveModuleId: params => {
          if (params.currentModuleId.indexOf("build/dist") > -1) {
            return null;
          }

          return params.currentModuleId
            .replace("src", "@flowable/addon-analytics-vanilla")
            .replace("/index", "")
            .replace(/\\/g, "/");
        },
        resolveModuleImport: params => {
          if (
            params.isDeclaredExternalModule ||
            params.currentModuleId.indexOf("bluebird") > 0 ||
            params.importedModuleId.indexOf("bluebird") > 0
          ) {
            return null;
          }

          let returnValue =
            params.importedModuleId[0] === "."
              ? path.resolve(
                  params.currentModuleId,
                  "..",
                  params.importedModuleId
                )
              : params.importedModuleId;

          returnValue = returnValue
            .replace(
              "src/main/js/com/flowable/addons/widgets/src",
              "@flowable/addon-analytics-vanilla"
            )
            .replace(/.*@/, "@")
            .replace(/\\/g, "/");

          return returnValue;
        }
      }),
      new GeneratePackageJsonPlugin(
        {
          author: packageJson.author,
          browser: packageJson.browser,
          bugs: packageJson.bugs,
          dependencies: packageJson.dependencies,
          externals: packageJson.externals,
          files: packageJson.files,
          engines: {
            node: ">= 8.0.0"
          },
          homepage: packageJson.homepage,
          keywords: packageJson.keywords,
          license: packageJson.license,
          name: "@flowable/addon-analytics-vanilla",
          peerDependencies: packageJson.peerDependencies,
          publishConfig: {
            registry: "https://repo.edorasware.com/api/npm/edoras-npm-repo"
          },
          repository: packageJson.repository,
          types: "typings.d.ts",
          version: packageJson.version
        },
        path.resolve(__dirname + "/package.json")
      ),
      new LicenseWebpackPlugin({
        pattern: /.*/,
        licenseFilenames: [
          "LICENSE",
          "LICENSE.md",
          "LICENSE.txt",
          "license",
          "license.md",
          "license.txt"
        ],
        outputFilename: "LICENSE.md",
        perChunkOutput: true,
        suppressErrors: false,
        includePackagesWithoutLicense: true,
        unacceptablePattern: /GPL/,
        abortOnUnacceptableLicense: true,
        modulesDirectories: ["node_modules"],
        licenseTemplateDir: "licenses/license-list-data/text/"
      }),
      new CopyWebpackPlugin(
        [
          {
            from: path.resolve(__dirname, "README.md"),
            to: path.resolve(__dirname, "build/dist")
          }
        ],
        {
          debug: "warning"
        }
      ),
      new EventPlugin([
        {
          hook: "afterEmit",
          callback: compilation => {
            let declaration: string = fs.readFileSync(
              path.resolve(__dirname, "build/dist/" + name + ".d.ts"),
              "utf8"
            );

            let modifiedDeclaration = declaration.replace(
              new RegExp(/import Promise from ['"]bluebird['"];?/, "g"),
              ""
            );

            fs.writeFileSync(
              path.resolve(__dirname, "build/dist/typings.d.ts"),
              modifiedDeclaration,
              "utf8"
            );

            if (fs.exists(__dirname, "build/dist/" + name + ".d.ts")) {
              fs.unlink(
                path.resolve(__dirname, "build/dist/" + name + ".d.ts"),
                () =>
                  console.error(
                    "file " +
                      path.resolve(__dirname, "build/dist/" + name + ".d.ts") +
                      " could not be deleted"
                  )
              );
            }

            if (fs.exists(__dirname, "build/dist/usr")) {
              fs.remove(path.resolve(__dirname, "build/dist/usr"), () =>
                console.error(
                  "folder " +
                    path.resolve(__dirname, "build/dist/usr") +
                    " could not be deleted"
                )
              );
            }
          }
        }
      ]),
      new CircularDependencyPlugin({
        exclude: /.*inversify.*|node_modules/,
        failOnError: false,
        allowAsyncCycles: true,
        cwd: process.cwd()
      }),
      new BundleAnalyzerPlugin({
        analyzerMode: "static",
        reportFilename: "analyze.html",
        openAnalyzer: false
      })
    ]
  };
};

export default getWebpackConfig(
  process.argv[outFilenameIndex].replace(".js", "")
);
