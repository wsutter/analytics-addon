import "../scss/storybook.scss";
import "whatwg-fetch";
import * as React from "react";
import { CSSProperties } from "react";
import { _, FormControlled, Model } from "@flowable/forms-react";
import { JsonTree } from "react-editable-json-tree";
import { FormContainerComponentState } from "../src/state/formComponentState";
import { FormContainerComponentProps } from "../src/props/formComponentProps";
import { Definition } from "../src/entities/definition";
import { MockServerServiceInterface } from "../src/services/mockServerService.d";
import { analyticsAddonStorybookContainer } from "../inversify.config";
import { TYPES } from "../types.config";
import { ValueToChange } from "@flowable/forms-react/Model";
import { FormDefinitionService } from "../src/services/formDefinitionService";
import concat from "lodash.concat";
import flatten from "lodash.flatten";
import merge from "lodash.merge";
import without from "lodash.without";
import { fetchMock } from "../src/services/fetchMock";
import { User } from "../src/entities/user";

export class FormContainer extends React.Component<
  FormContainerComponentProps,
  FormContainerComponentState
> {
  private mockServerService: MockServerServiceInterface = analyticsAddonStorybookContainer.get<
    MockServerServiceInterface
  >(TYPES.MockServerService);

  private dataResourceURLs: Array<string | undefined> = [];
  private userResourceURL: string = "rest/user/response.json";
  private hasError: boolean = false;
  private isLoading: boolean = true;
  private payload: Model.Payload = {};

  constructor(props: FormContainerComponentProps) {
    super(props);

    let self = this;

    self.state = new FormContainerComponentState();
    self.payload = self.state.payload;

    self.onUpdateDataSources = self.onUpdateDataSources.bind(self);

    self.onUpdateUserSources = self.onUpdateUserSources.bind(self);

    self.onUpdateDefinitions = self.onUpdateDefinitions.bind(self);

    self.onUpdatePayload = self.onUpdatePayload.bind(self);
  }

  static getDerivedStateFromProps(
    props: FormContainerComponentProps,
    state: FormContainerComponentState
  ) {
    if (
      JSON.stringify(props.additionalData) !==
      JSON.stringify(state.additionalData)
    ) {
      state.additionalData = props.additionalData;
    }

    return state;
  }

  render() {
    let self = this;

    if (self.hasError) {
      return <h1>Something went wrong! Check the console.</h1>;
    } else if (self.isLoading) {
      return null;
    }

    let formLayout = FormDefinitionService.getFormLayout(
      self.state.definitions
    );

    return (
      <div>
        <FormControlled
          {...self.state}
          config={formLayout}
          Components={self.props.Components}
          payload={self.state.payload}
          additionalData={self.state.additionalData}
          onChange={self.onUpdatePayload}
          fetch={fetchMock as any}
        />

        <div className="flw__panel flw__flw-form flw__panel--showBorder__content-wrapper">
          <div className="flw__panel__row flw__flw-form__row">
            <div className="flw__panel__col--3 flw__flw-form__col--3">
              <div className="flw__textarea flw__component">
                <label className="flw__label flw__textarea__label flw__component__label">
                  data sources
                </label>
                <a
                  id="dataSourceDownload"
                  className="flw__script-button__button flw__component__button"
                  style={((): CSSProperties => {
                    return {
                      maxHeight: "40px"
                    };
                  })()}
                  download="dataSources.json"
                >
                  download data sources
                </a>
                <div id="dataSource">
                  <JsonTree
                    data={self.state.data}
                    isCollapsed={(keyPath, deep) => deep > 2}
                    addButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        +
                      </button>
                    }
                    cancelButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        x
                      </button>
                    }
                    editButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        edit
                      </button>
                    }
                    plusMenuElement={
                      <button className="flw__script-button__button flw__component__button">
                        +
                      </button>
                    }
                    minusMenuElement={
                      <button className="flw__script-button__button flw__component__button">
                        -
                      </button>
                    }
                    onFullyUpdate={self.onUpdateDataSources}
                  />
                </div>
              </div>
            </div>

            <div className="flw__panel__col--3 flw__flw-form__col--3">
              <div className="flw__textarea flw__component">
                <label className="flw__label flw__textarea__label flw__component__label">
                  user sources
                </label>
                <a
                  id="userSourceDownload"
                  className="flw__script-button__button flw__component__button"
                  style={((): CSSProperties => {
                    return {
                      maxHeight: "40px"
                    };
                  })()}
                  download="userSources.json"
                >
                  download user sources
                </a>
                <div id="userSource">
                  <JsonTree
                    data={self.state.user}
                    isCollapsed={(keyPath, deep) => deep > 1}
                    addButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        +
                      </button>
                    }
                    cancelButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        x
                      </button>
                    }
                    editButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        edit
                      </button>
                    }
                    plusMenuElement={
                      <button className="flw__script-button__button flw__component__button">
                        +
                      </button>
                    }
                    minusMenuElement={
                      <button className="flw__script-button__button flw__component__button">
                        -
                      </button>
                    }
                    onFullyUpdate={self.onUpdateUserSources}
                  />
                </div>
              </div>
            </div>

            <div className="flw__panel__col--3 flw__flw-form__col--3">
              <div className="flw__textarea flw__component">
                <label className="flw__label flw__textarea__label flw__component__label">
                  chart definitions
                </label>
                <a
                  id="definitionsDownload"
                  className="flw__script-button__button flw__component__button"
                  style={((): CSSProperties => {
                    return {
                      maxHeight: "40px"
                    };
                  })()}
                  download="chartDefinitions.json"
                >
                  download chart definitions
                </a>
                <div id="definitions">
                  <JsonTree
                    data={self.state.definitions}
                    isCollapsed={(keyPath, deep) => deep > 3}
                    addButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        +
                      </button>
                    }
                    cancelButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        x
                      </button>
                    }
                    editButtonElement={
                      <button className="flw__script-button__button flw__component__button">
                        edit
                      </button>
                    }
                    plusMenuElement={
                      <button className="flw__script-button__button flw__component__button">
                        +
                      </button>
                    }
                    minusMenuElement={
                      <button className="flw__script-button__button flw__component__button">
                        -
                      </button>
                    }
                    onFullyUpdate={self.onUpdateDefinitions}
                  />
                </div>
              </div>
            </div>

            <div className="flw__panel__col--3 flw__flw-form__col--3">
              <div className="flw__textarea flw__component">
                <label className="flw__label flw__textarea__label flw__component__label">
                  chart states
                </label>
                <a
                  id="chartPayloadDownload"
                  className="flw__script-button__button flw__component__button"
                  style={((): CSSProperties => {
                    return {
                      maxHeight: "40px"
                    };
                  })()}
                  download="chartStates.json"
                >
                  download chart states
                </a>
                <div id="payload">
                  <JsonTree
                    data={self.state.payload}
                    isCollapsed={(keyPath, deep) => deep > 3}
                    readOnly={true}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    let self = this;

    let extraSettings = self.props.definitions[0].extraSettings;
    let state = self.props.payload[extraSettings.id];
    self.dataResourceURLs = without(
      extraSettings.dataResources.items.map(item => {
        if (
          item.level === state.currentLevel ||
          extraSettings.chartType === "heatmap-chart"
        ) {
          return item.dataResource.queryUrl;
        }
      }),
      undefined
    );

    self.isLoading = true;

    let userPromise = self.mockServerService.get(self.userResourceURL);

    let dataPromises = self.dataResourceURLs.map((dataResourceURL: string) => {
      return self.mockServerService.get(dataResourceURL);
    });

    Promise.all(concat(userPromise, dataPromises)).then(
      (returnValues: Array<any>) => {
        let user: any = returnValues[0][0];
        returnValues.splice(0, 1);
        let data: any = flatten(merge(returnValues));

        self.isLoading = false;

        self.setState(
          {
            definitions: self.props.definitions,
            payload: self.props.payload,
            user: user,
            data: data
          },
          () => {
            self.refreshDownloadButtons();
          }
        );
      }
    );
  }

  componentDidCatch(error: any, errorInfo: any) {
    if (error !== undefined) {
      console.error(error);

      this.hasError = true;
    }

    if (errorInfo !== undefined) {
      console.info(errorInfo);
    }
  }

  onUpdateDataSources(newData: Array<Array<any>>): void {
    let self = this;

    let newPayload;

    if (!self.isLoading && !self.hasError) {
      self.isLoading = true;

      newPayload = Object.assign({}, this.state.payload);

      let charts = Object.keys(newPayload);

      charts.forEach((chart: string) => {
        newPayload[chart].shouldUpdate = true;
      });

      self.setState({
        payload: newPayload
      });

      let filteredData = newData.map((newDataArray: Array<any>) => {
        return newDataArray.filter((newDatum: any) => {
          return newDatum !== null;
        });
      });

      let promises: Array<Promise<Array<string>>> = self.dataResourceURLs.map(
        (dataResourceURL: string, index: number) => {
          charts.forEach((chart: string) => {
            newPayload[chart].shouldUpdate = false;
          });

          return self.mockServerService.put(
            dataResourceURL,
            JSON.stringify(filteredData[index])
          );
        }
      );

      Promise.all(promises).then(() => {
        self.isLoading = false;

        self.setState(
          {
            data: filteredData,
            payload: newPayload
          },
          () => {
            self.refreshDownloadButtons();
          }
        );
      });
    }
  }

  onUpdateUserSources(newUser: Array<User>): void {
    let self = this;

    let newPayload;

    if (!self.isLoading && !self.hasError) {
      self.isLoading = true;

      newPayload = Object.assign({}, this.state.payload);

      let charts = Object.keys(newPayload);

      charts.forEach((chart: string) => {
        newPayload[chart].shouldUpdate = true;
      });

      self.setState({
        payload: newPayload
      });

      charts.forEach((chart: string) => {
        newPayload[chart].shouldUpdate = false;
      });

      self.mockServerService.put(self.userResourceURL, JSON.stringify(newUser));

      self.isLoading = false;

      self.setState(
        {
          user: newUser,
          payload: newPayload
        },
        () => {
          self.refreshDownloadButtons();
        }
      );
    }
  }

  onUpdateDefinitions(definitions: Array<Definition>): void {
    let self = this;

    let newPayload;

    if (!self.isLoading && !self.hasError) {
      self.isLoading = true;

      newPayload = Object.assign({}, this.state.payload);

      let components = Object.keys(newPayload);

      components.forEach((component: string) => {
        newPayload[component].shouldUpdate = true;
      });

      self.setState({
        payload: newPayload
      });

      components.forEach((component: string) => {
        newPayload[component].shouldUpdate = false;
      });

      self.isLoading = false;

      self.setState(
        {
          definitions: definitions,
          payload: newPayload
        },
        () => {
          self.refreshDownloadButtons();
        }
      );
    }

    return newPayload;
  }

  onUpdatePayload(valueToChange: ValueToChange) {
    let self = this;

    if (!self.isLoading && !self.hasError) {
      self.payload = _.lazyCloneEdit(
        self.payload,
        valueToChange.$path,
        valueToChange.$value
      );

      self.refreshDownloadButtons();

      self.setState({
        payload: self.payload
      });
    }
  }

  refreshDownloadButtons() {
    let self = this;

    let dataSourceDownloadString =
      "data:text/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(self.state.data));

    let dataSourceDownload = document.getElementById("dataSourceDownload");
    if (dataSourceDownload === null) {
      throw new Error(
        "For some peculiar reason, dataSourceDownload anchor is not available!"
      );
    }
    dataSourceDownload.setAttribute("href", dataSourceDownloadString);

    let userSourceDownloadString =
      "data:text/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(self.state.user));

    let userSourceDownload = document.getElementById("userSourceDownload");
    if (userSourceDownload === null) {
      throw new Error(
        "For some peculiar reason, userSourceDownload anchor is not available!"
      );
    }
    userSourceDownload.setAttribute("href", userSourceDownloadString);

    let definitionsDownloadString =
      "data:text/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(self.state.definitions));

    let definitionsDownload = document.getElementById("definitionsDownload");
    if (definitionsDownload === null) {
      throw new Error(
        "For some peculiar reason, definitionsDownload anchor is not available!"
      );
    }
    definitionsDownload.setAttribute("href", definitionsDownloadString);

    let chartPayloadDownloadString =
      "data:text/json;charset=utf-8," +
      encodeURIComponent(JSON.stringify(self.state.payload));

    let chartPayloadDownload = document.getElementById("chartPayloadDownload");
    if (chartPayloadDownload === null) {
      throw new Error(
        "For some peculiar reason, chartPayloadDownload anchor is not available!"
      );
    }
    chartPayloadDownload.setAttribute("href", chartPayloadDownloadString);
  }
}
