export class Definition {
  id: string = "";
  type: string = "";
  size?: number;
  label: string = "";
  value: string = "";
  isVisible?: boolean;
  isRequired?: boolean;
  description: string = "";
  enabled?: boolean;
  extraSettings?: any;
}
