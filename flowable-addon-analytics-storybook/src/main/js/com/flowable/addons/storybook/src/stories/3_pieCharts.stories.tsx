import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import pieChart from "@flowable/addon-analytics-react/pieChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const simplePieChartDefinition = require("../../chartDefinitions/pieCharts/simplePieChart.json") as Array<
  Definition
>;
const simplePieChartState = require("../../chartStates/pieCharts/simplePieChart.json") as Model.Payload;
const fullyFledgedPieChartDefinition = require("../../chartDefinitions/pieCharts/fullyFledgedPieChart.json") as Array<
  Definition
>;
const fullyFledgedPieChartState = require("../../chartStates/pieCharts/simplePieChart.json") as Model.Payload;

const customColorsPieChartDefinition = require("../../chartDefinitions/pieCharts/customColorsPieChart.json") as Array<
  Definition
>;
const customColorsPieChartState = require("../../chartStates/pieCharts/customColorsPieChart.json") as Model.Payload;
const colorsByGroupsPieChartDefinition = require("../../chartDefinitions/pieCharts/colorsByGroupsPieChart.json") as Array<
  Definition
>;
const colorsByGroupsPieChartState = require("../../chartStates/pieCharts/colorsByGroupsPieChart.json") as Model.Payload;
const colorsByThresholdsPieChartDefinition = require("../../chartDefinitions/pieCharts/colorsByThresholdsPieChart.json") as Array<
  Definition
>;
const colorsByThresholdsPieChartState = require("../../chartStates/pieCharts/colorsByThresholdsPieChart.json") as Model.Payload;

const drillingSimplePieChartDefinition = require("../../chartDefinitions/pieCharts/drillingSimplePieChart.json") as Array<
  Definition
>;
const drillingSimplePieChartState = require("../../chartStates/pieCharts/drillingSimplePieChart.json") as Model.Payload;
const drillingByGroupsPieChartDefinition = require("../../chartDefinitions/pieCharts/drillingByGroupsPieChart.json") as Array<
  Definition
>;
const drillingByGroupsPieChartState = require("../../chartStates/pieCharts/drillingByGroupsPieChart.json") as Model.Payload;

const legendBottomPieChartDefinition = require("../../chartDefinitions/pieCharts/legendBottomPieChart.json") as Array<
  Definition
>;
const legendBottomPieChartState = require("../../chartStates/pieCharts/legendBottomPieChart.json") as Model.Payload;
const legendTopPieChartDefinition = require("../../chartDefinitions/pieCharts/legendTopPieChart.json") as Array<
  Definition
>;
const legendTopPieChartState = require("../../chartStates/pieCharts/legendTopPieChart.json") as Model.Payload;
const legendLeftPieChartDefinition = require("../../chartDefinitions/pieCharts/legendLeftPieChart.json") as Array<
  Definition
>;
const legendLeftPieChartState = require("../../chartStates/pieCharts/legendLeftPieChart.json") as Model.Payload;
const legendRightPieChartDefinition = require("../../chartDefinitions/pieCharts/legendRightPieChart.json") as Array<
  Definition
>;
const legendRightPieChartState = require("../../chartStates/pieCharts/legendRightPieChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

storiesOf("3) charts/pie charts", module)
  .add(
    simplePieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={simplePieChartDefinition}
          payload={{
            simplePieChart: simplePieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        simplePieChartDefinition[0].label +
        "\n" +
        simplePieChartDefinition[0].description
    }
  )
  .add(
    fullyFledgedPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={fullyFledgedPieChartDefinition}
          payload={{
            fullyFledgedPieChart: fullyFledgedPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        fullyFledgedPieChartDefinition[0].label +
        "\n" +
        fullyFledgedPieChartDefinition[0].description
    }
  );

storiesOf("3) charts/pie charts/colors", module)
  .add(
    customColorsPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={customColorsPieChartDefinition}
          payload={{
            customColorsPieChart: customColorsPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        customColorsPieChartDefinition[0].label +
        "\n" +
        customColorsPieChartDefinition[0].description
    }
  )
  .add(
    colorsByGroupsPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={colorsByGroupsPieChartDefinition}
          payload={{
            colorsByGroupsPieChart: colorsByGroupsPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByGroupsPieChartDefinition[0].label +
        "\n" +
        colorsByGroupsPieChartDefinition[0].description
    }
  )
  .add(
    colorsByThresholdsPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={colorsByThresholdsPieChartDefinition}
          payload={{
            colorsByThresholdsPieChart: colorsByThresholdsPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByThresholdsPieChartDefinition[0].label +
        "\n" +
        colorsByThresholdsPieChartDefinition[0].description
    }
  );

storiesOf("3) charts/pie charts/drilling", module)
  .add(
    drillingSimplePieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={drillingSimplePieChartDefinition}
          payload={{
            drillingSimplePieChart: drillingSimplePieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingSimplePieChartDefinition[0].label +
        "\n" +
        drillingSimplePieChartDefinition[0].description
    }
  )
  .add(
    drillingByGroupsPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={drillingByGroupsPieChartDefinition}
          payload={{
            drillingByGroupsPieChart: drillingByGroupsPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingByGroupsPieChartDefinition[0].label +
        "\n" +
        drillingByGroupsPieChartDefinition[0].description
    }
  );

storiesOf("3) charts/pie charts/legends & labels", module)
  .add(
    legendBottomPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={legendBottomPieChartDefinition}
          payload={{
            legendBottomPieChart: legendBottomPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendBottomPieChartDefinition[0].label +
        "\n" +
        legendBottomPieChartDefinition[0].description
    }
  )
  .add(
    legendTopPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={legendTopPieChartDefinition}
          payload={{
            legendTopPieChart: legendTopPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendTopPieChartDefinition[0].label +
        "\n" +
        legendTopPieChartDefinition[0].description
    }
  )
  .add(
    legendRightPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={legendRightPieChartDefinition}
          payload={{
            legendRightPieChart: legendRightPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendRightPieChartDefinition[0].label +
        "\n" +
        legendRightPieChartDefinition[0].description
    }
  )
  .add(
    legendLeftPieChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={pieChart}
          definitions={legendLeftPieChartDefinition}
          payload={{
            legendLeftPieChart: legendLeftPieChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendLeftPieChartDefinition[0].label +
        "\n" +
        legendLeftPieChartDefinition[0].description
    }
  );
