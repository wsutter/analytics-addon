import React from "react";
import { storiesOf } from "@storybook/react";
import { withDocs } from "storybook-readme";
import readme from "@flowable/addon-analytics-react/README.md";
import licenses from "@flowable/addon-analytics-react/LICENSE.md";
import ReactSVG from "react-svg";

storiesOf("1) about", module)
  .add(
    "read me",
    withDocs(readme, () => (
      // @ts-ignore
      <ReactSVG
        src="../assets/svg/flowable.svg"
        svgStyle={{ width: 633, height: 128 }}
      />
    ))
  )
  .add(
    "licenses",
    withDocs(licenses, () => (
      // @ts-ignore
      <ReactSVG
        src="../assets/svg/flowable.svg"
        svgStyle={{ width: 633, height: 128 }}
      />
    ))
  );
