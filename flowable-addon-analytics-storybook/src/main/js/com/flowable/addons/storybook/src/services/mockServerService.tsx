import { injectable } from "inversify";
import { MockServerServiceInterface } from "./mockServerService.d";
import { fetchMock } from "./fetchMock";

@injectable()
export class MockServerService implements MockServerServiceInterface {
  public get(url: string, parameters?: any): Promise<Array<string>> {
    url = this.amendUrl(url, parameters);

    let promise = ((): Promise<string> => {
      let promise: Promise<any> = fetchMock(url, {
        method: "GET",
        mode: "cors"
      }).then(response => {
        if (response === null) {
          throw "Could not retrieve data from mock server at " +
            url +
            ": " +
            JSON.stringify(response);
        }

        if (response.body === null) {
          throw "Could not read body from response of " +
            url +
            ": " +
            JSON.stringify(response);
        }

        return response.json();
      });

      return Promise.resolve<string>(promise).catch((error: any) => {
        throw "Could not resolve body from response of " + url + ": " + error;
      });
    })();

    return Promise.all([promise]).catch(() => {
      throw "Could not retrieve data from " + url;
    });
  }

  public put(
    url: string,
    body: string,
    parameters?: any
  ): Promise<Array<string>> {
    url = this.amendUrl(url, parameters);

    let promise = ((): Promise<string> => {
      let promise: Promise<any> = fetchMock(url, {
        method: "PUT",
        mode: "cors",
        body: body
      }).then(response => {
        if (response === null) {
          throw "Could not send data to mock server at " +
            url +
            ": " +
            JSON.stringify(response);
        }

        if (response.body === null) {
          throw "Could not read body from response of " +
            url +
            ": " +
            JSON.stringify(response);
        }

        return response.json();
      });

      return Promise.resolve<string>(promise).catch((error: any) => {
        throw "Could not resolve body from response of " + url + ": " + error;
      });
    })();

    return Promise.all([promise]).catch(() => {
      throw "Could not update " + url;
    });
  }

  private amendUrl(url: string, parameters?: any): string {
    let urlParameters: string = "";
    if (parameters !== undefined) {
      urlParameters = Object.keys(parameters)
        .map((key: string) => {
          return (
            encodeURIComponent(key) + "=" + encodeURIComponent(parameters[key])
          );
        })
        .join("&");

      url += "?" + urlParameters;
    }

    return url;
  }
}
