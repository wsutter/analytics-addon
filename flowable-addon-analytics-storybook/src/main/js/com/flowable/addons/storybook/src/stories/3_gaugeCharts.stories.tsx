import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import gaugeChart from "@flowable/addon-analytics-react/gaugeChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const simpleGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/simpleGaugeChart.json") as Array<
  Definition
>;
const simpleGaugeChartState = require("../../chartStates/gaugeCharts/simpleGaugeChart.json") as Model.Payload;
const fullyFledgedGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/fullyFledgedGaugeChart.json") as Array<
  Definition
>;
const fullyFledgedGaugeChartState = require("../../chartStates/gaugeCharts/fullyFledgedGaugeChart.json") as Model.Payload;

const customColorsGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/customColorsGaugeChart.json") as Array<
  Definition
>;
const customColorsGaugeChartState = require("../../chartStates/gaugeCharts/customColorsGaugeChart.json") as Model.Payload;
const colorsByGroupsGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/colorsByGroupsGaugeChart.json") as Array<
  Definition
>;
const colorsByGroupsGaugeChartState = require("../../chartStates/gaugeCharts/colorsByGroupsGaugeChart.json") as Model.Payload;
const colorsByThresholdsGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/colorsByThresholdsGaugeChart.json") as Array<
  Definition
>;
const colorsByThresholdsGaugeChartState = require("../../chartStates/gaugeCharts/colorsByThresholdsGaugeChart.json") as Model.Payload;

const drillingSimpleGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/drillingSimpleGaugeChart.json") as Array<
  Definition
>;
const drillingSimpleGaugeChartState = require("../../chartStates/gaugeCharts/drillingSimpleGaugeChart.json") as Model.Payload;
const drillingByGroupsGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/drillingByGroupsGaugeChart.json") as Array<
  Definition
>;
const drillingByGroupsGaugeChartState = require("../../chartStates/gaugeCharts/drillingByGroupsGaugeChart.json") as Model.Payload;

const legendBottomGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/legendBottomGaugeChart.json") as Array<
  Definition
>;
const legendBottomGaugeChartState = require("../../chartStates/gaugeCharts/legendBottomGaugeChart.json") as Model.Payload;
const legendLeftGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/legendLeftGaugeChart.json") as Array<
  Definition
>;
const legendLeftGaugeChartState = require("../../chartStates/gaugeCharts/legendLeftGaugeChart.json") as Model.Payload;
const legendRightGaugeChartDefinition = require("../../chartDefinitions/gaugeCharts/legendRightGaugeChart.json") as Array<
  Definition
>;
const legendRightGaugeChartState = require("../../chartStates/gaugeCharts/legendRightGaugeChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

// Stories
storiesOf("3) charts/gauge charts", module)
  .add(
    simpleGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={simpleGaugeChartDefinition}
          payload={{
            simpleGaugeChart: simpleGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        simpleGaugeChartDefinition[0].label +
        "\n" +
        simpleGaugeChartDefinition[0].description
    }
  )
  .add(
    fullyFledgedGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={fullyFledgedGaugeChartDefinition}
          payload={{
            fullyFledgedGaugeChart: fullyFledgedGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        fullyFledgedGaugeChartDefinition[0].label +
        "\n" +
        fullyFledgedGaugeChartDefinition[0].description
    }
  );

storiesOf("3) charts/gauge charts/colors", module)
  .add(
    customColorsGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={customColorsGaugeChartDefinition}
          payload={{
            customColorsGaugeChart: customColorsGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        customColorsGaugeChartDefinition[0].label +
        "\n" +
        customColorsGaugeChartDefinition[0].description
    }
  )
  .add(
    colorsByGroupsGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={colorsByGroupsGaugeChartDefinition}
          payload={{
            colorsByGroupsGaugeChart: colorsByGroupsGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByGroupsGaugeChartDefinition[0].label +
        "\n" +
        colorsByGroupsGaugeChartDefinition[0].description
    }
  )
  .add(
    colorsByThresholdsGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={colorsByThresholdsGaugeChartDefinition}
          payload={{
            colorsByThresholdsGaugeChart: colorsByThresholdsGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByThresholdsGaugeChartDefinition[0].label +
        "\n" +
        colorsByThresholdsGaugeChartDefinition[0].description
    }
  );

storiesOf("3) charts/gauge charts/drilling", module)
  .add(
    drillingSimpleGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={drillingSimpleGaugeChartDefinition}
          payload={{
            drillingSimpleGaugeChart: drillingSimpleGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingSimpleGaugeChartDefinition[0].label +
        "\n" +
        drillingSimpleGaugeChartDefinition[0].description
    }
  )
  .add(
    drillingByGroupsGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={drillingByGroupsGaugeChartDefinition}
          payload={{
            drillingByGroupsGaugeChart: drillingByGroupsGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingByGroupsGaugeChartDefinition[0].label +
        "\n" +
        drillingByGroupsGaugeChartDefinition[0].description
    }
  );

storiesOf("3) charts/gauge charts/legends & labels", module)
  .add(
    legendBottomGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={legendBottomGaugeChartDefinition}
          payload={{
            legendBottomGaugeChart: legendBottomGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendBottomGaugeChartDefinition[0].label +
        "\n" +
        legendBottomGaugeChartDefinition[0].description
    }
  )
  .add(
    legendRightGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={legendRightGaugeChartDefinition}
          payload={{
            legendRightGaugeChart: legendRightGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendRightGaugeChartDefinition[0].label +
        "\n" +
        legendRightGaugeChartDefinition[0].description
    }
  )
  .add(
    legendLeftGaugeChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={gaugeChart}
          definitions={legendLeftGaugeChartDefinition}
          payload={{
            legendLeftGaugeChart: legendLeftGaugeChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendLeftGaugeChartDefinition[0].label +
        "\n" +
        legendLeftGaugeChartDefinition[0].description
    }
  );
