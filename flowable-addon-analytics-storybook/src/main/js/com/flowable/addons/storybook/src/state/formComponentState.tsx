import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";

export class FormContainerComponentState {
  public definitions: Array<Definition> = [];
  public payload: Model.Payload = {} as Model.Payload;
  public user: any = {};
  public data: any = {};
  public additionalData: Model.Payload = {} as Model.Payload;
}
