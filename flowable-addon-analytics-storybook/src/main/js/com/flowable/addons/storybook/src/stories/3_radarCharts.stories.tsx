import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import radarChart from "@flowable/addon-analytics-react/radarChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const simpleRadarChartDefinition = require("../../chartDefinitions/radarCharts/simpleRadarChart.json") as Array<
  Definition
>;
const simpleRadarChartState = require("../../chartStates/radarCharts/simpleRadarChart.json") as Model.Payload;
const fullyFledgedRadarChartDefinition = require("../../chartDefinitions/radarCharts/fullyFledgedRadarChart.json") as Array<
  Definition
>;
const fullyFledgedRadarChartState = require("../../chartStates/radarCharts/simpleRadarChart.json") as Model.Payload;

const customColorsRadarChartDefinition = require("../../chartDefinitions/radarCharts/customColorsRadarChart.json") as Array<
  Definition
>;
const customColorsRadarChartState = require("../../chartStates/radarCharts/customColorsRadarChart.json") as Model.Payload;
const colorsByGroupsRadarChartDefinition = require("../../chartDefinitions/radarCharts/colorsByGroupsRadarChart.json") as Array<
  Definition
>;
const colorsByGroupsRadarChartState = require("../../chartStates/radarCharts/colorsByGroupsRadarChart.json") as Model.Payload;
const colorsByThresholdsRadarChartDefinition = require("../../chartDefinitions/radarCharts/colorsByThresholdsRadarChart.json") as Array<
  Definition
>;
const colorsByThresholdsRadarChartState = require("../../chartStates/radarCharts/colorsByThresholdsRadarChart.json") as Model.Payload;

const drillingSimpleRadarChartDefinition = require("../../chartDefinitions/radarCharts/drillingSimpleRadarChart.json") as Array<
  Definition
>;
const drillingSimpleRadarChartState = require("../../chartStates/radarCharts/drillingSimpleRadarChart.json") as Model.Payload;
const drillingByGroupsRadarChartDefinition = require("../../chartDefinitions/radarCharts/drillingByGroupsRadarChart.json") as Array<
  Definition
>;
const drillingByGroupsRadarChartState = require("../../chartStates/radarCharts/drillingByGroupsRadarChart.json") as Model.Payload;

const legendBottomRadarChartDefinition = require("../../chartDefinitions/radarCharts/legendBottomRadarChart.json") as Array<
  Definition
>;
const legendBottomRadarChartState = require("../../chartStates/radarCharts/legendBottomRadarChart.json") as Model.Payload;
const legendTopRadarChartDefinition = require("../../chartDefinitions/radarCharts/legendTopRadarChart.json") as Array<
  Definition
>;
const legendTopRadarChartState = require("../../chartStates/radarCharts/legendTopRadarChart.json") as Model.Payload;
const legendLeftRadarChartDefinition = require("../../chartDefinitions/radarCharts/legendLeftRadarChart.json") as Array<
  Definition
>;
const legendLeftRadarChartState = require("../../chartStates/radarCharts/legendLeftRadarChart.json") as Model.Payload;
const legendRightRadarChartDefinition = require("../../chartDefinitions/radarCharts/legendRightRadarChart.json") as Array<
  Definition
>;
const legendRightRadarChartState = require("../../chartStates/radarCharts/legendRightRadarChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

storiesOf("3) charts/radar charts", module)
  .add(
    simpleRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={simpleRadarChartDefinition}
          payload={{
            simpleRadarChart: simpleRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        simpleRadarChartDefinition[0].label +
        "\n" +
        simpleRadarChartDefinition[0].description
    }
  )
  .add(
    fullyFledgedRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={fullyFledgedRadarChartDefinition}
          payload={{
            fullyFledgedRadarChart: fullyFledgedRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        fullyFledgedRadarChartDefinition[0].label +
        "\n" +
        fullyFledgedRadarChartDefinition[0].description
    }
  );

storiesOf("3) charts/radar charts/colors", module)
  .add(
    customColorsRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={customColorsRadarChartDefinition}
          payload={{
            customColorsRadarChart: customColorsRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        customColorsRadarChartDefinition[0].label +
        "\n" +
        customColorsRadarChartDefinition[0].description
    }
  )
  .add(
    colorsByGroupsRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={colorsByGroupsRadarChartDefinition}
          payload={{
            colorsByGroupsRadarChart: colorsByGroupsRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByGroupsRadarChartDefinition[0].label +
        "\n" +
        colorsByGroupsRadarChartDefinition[0].description
    }
  )
  .add(
    colorsByThresholdsRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={colorsByThresholdsRadarChartDefinition}
          payload={{
            colorsByThresholdsRadarChart: colorsByThresholdsRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByThresholdsRadarChartDefinition[0].label +
        "\n" +
        colorsByThresholdsRadarChartDefinition[0].description
    }
  );

storiesOf("3) charts/radar charts/drilling", module)
  .add(
    drillingSimpleRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={drillingSimpleRadarChartDefinition}
          payload={{
            drillingSimpleRadarChart: drillingSimpleRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingSimpleRadarChartDefinition[0].label +
        "\n" +
        drillingSimpleRadarChartDefinition[0].description
    }
  )
  .add(
    drillingByGroupsRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={drillingByGroupsRadarChartDefinition}
          payload={{
            drillingByGroupsRadarChart: drillingByGroupsRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingByGroupsRadarChartDefinition[0].label +
        "\n" +
        drillingByGroupsRadarChartDefinition[0].description
    }
  );

storiesOf("3) charts/radar charts/legends & labels", module)
  .add(
    legendBottomRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={legendBottomRadarChartDefinition}
          payload={{
            legendBottomRadarChart: legendBottomRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendBottomRadarChartDefinition[0].label +
        "\n" +
        legendBottomRadarChartDefinition[0].description
    }
  )
  .add(
    legendTopRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={legendTopRadarChartDefinition}
          payload={{
            legendTopRadarChart: legendTopRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendTopRadarChartDefinition[0].label +
        "\n" +
        legendTopRadarChartDefinition[0].description
    }
  )
  .add(
    legendRightRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={legendRightRadarChartDefinition}
          payload={{
            legendRightRadarChart: legendRightRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendRightRadarChartDefinition[0].label +
        "\n" +
        legendRightRadarChartDefinition[0].description
    }
  )
  .add(
    legendLeftRadarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={radarChart}
          definitions={legendLeftRadarChartDefinition}
          payload={{
            legendLeftRadarChart: legendLeftRadarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendLeftRadarChartDefinition[0].label +
        "\n" +
        legendLeftRadarChartDefinition[0].description
    }
  );
