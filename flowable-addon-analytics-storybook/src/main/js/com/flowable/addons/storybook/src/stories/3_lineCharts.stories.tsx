import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import lineChart from "@flowable/addon-analytics-react/lineChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const simpleLineChartDefinition = require("../../chartDefinitions/lineCharts/simpleLineChart.json") as Array<
  Definition
>;
const simpleLineChartState = require("../../chartStates/lineCharts/simpleLineChart.json") as Model.Payload;
const fullyFledgedLineChartDefinition = require("../../chartDefinitions/lineCharts/fullyFledgedLineChart.json") as Array<
  Definition
>;
const fullyFledgedLineChartState = require("../../chartStates/lineCharts/simpleLineChart.json") as Model.Payload;

const customColorsLineChartDefinition = require("../../chartDefinitions/lineCharts/customColorsLineChart.json") as Array<
  Definition
>;
const customColorsLineChartState = require("../../chartStates/lineCharts/customColorsLineChart.json") as Model.Payload;
const colorsByGroupsLineChartDefinition = require("../../chartDefinitions/lineCharts/colorsByGroupsLineChart.json") as Array<
  Definition
>;
const colorsByGroupsLineChartState = require("../../chartStates/lineCharts/colorsByGroupsLineChart.json") as Model.Payload;
const colorsByThresholdsLineChartDefinition = require("../../chartDefinitions/lineCharts/colorsByThresholdsLineChart.json") as Array<
  Definition
>;
const colorsByThresholdsLineChartState = require("../../chartStates/lineCharts/colorsByThresholdsLineChart.json") as Model.Payload;

const drillingSimpleLineChartDefinition = require("../../chartDefinitions/lineCharts/drillingSimpleLineChart.json") as Array<
  Definition
>;
const drillingSimpleLineChartState = require("../../chartStates/lineCharts/drillingSimpleLineChart.json") as Model.Payload;
const drillingByGroupsLineChartDefinition = require("../../chartDefinitions/lineCharts/drillingByGroupsLineChart.json") as Array<
  Definition
>;
const drillingByGroupsLineChartState = require("../../chartStates/lineCharts/drillingByGroupsLineChart.json") as Model.Payload;

const legendBottomLineChartDefinition = require("../../chartDefinitions/lineCharts/legendBottomLineChart.json") as Array<
  Definition
>;
const legendBottomLineChartState = require("../../chartStates/lineCharts/legendBottomLineChart.json") as Model.Payload;
const legendTopLineChartDefinition = require("../../chartDefinitions/lineCharts/legendTopLineChart.json") as Array<
  Definition
>;
const legendTopLineChartState = require("../../chartStates/lineCharts/legendTopLineChart.json") as Model.Payload;
const legendLeftLineChartDefinition = require("../../chartDefinitions/lineCharts/legendLeftLineChart.json") as Array<
  Definition
>;
const legendLeftLineChartState = require("../../chartStates/lineCharts/legendLeftLineChart.json") as Model.Payload;
const legendRightLineChartDefinition = require("../../chartDefinitions/lineCharts/legendRightLineChart.json") as Array<
  Definition
>;
const legendRightLineChartState = require("../../chartStates/lineCharts/legendRightLineChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

storiesOf("3) charts/line charts", module)
  .add(
    simpleLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={simpleLineChartDefinition}
          payload={{
            simpleLineChart: simpleLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        simpleLineChartDefinition[0].label +
        "\n" +
        simpleLineChartDefinition[0].description
    }
  )
  .add(
    fullyFledgedLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={fullyFledgedLineChartDefinition}
          payload={{
            fullyFledgedLineChart: fullyFledgedLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        fullyFledgedLineChartDefinition[0].label +
        "\n" +
        fullyFledgedLineChartDefinition[0].description
    }
  );

storiesOf("3) charts/line charts/colors", module)
  .add(
    customColorsLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={customColorsLineChartDefinition}
          payload={{
            customColorsLineChart: customColorsLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        customColorsLineChartDefinition[0].label +
        "\n" +
        customColorsLineChartDefinition[0].description
    }
  )
  .add(
    colorsByGroupsLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={colorsByGroupsLineChartDefinition}
          payload={{
            colorsByGroupsLineChart: colorsByGroupsLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByGroupsLineChartDefinition[0].label +
        "\n" +
        colorsByGroupsLineChartDefinition[0].description
    }
  )
  .add(
    colorsByThresholdsLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={colorsByThresholdsLineChartDefinition}
          payload={{
            colorsByThresholdsLineChart: colorsByThresholdsLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByThresholdsLineChartDefinition[0].label +
        "\n" +
        colorsByThresholdsLineChartDefinition[0].description
    }
  );

storiesOf("3) charts/line charts/drilling", module)
  .add(
    drillingSimpleLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={drillingSimpleLineChartDefinition}
          payload={{
            drillingSimpleLineChart: drillingSimpleLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingSimpleLineChartDefinition[0].label +
        "\n" +
        drillingSimpleLineChartDefinition[0].description
    }
  )
  .add(
    drillingByGroupsLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={drillingByGroupsLineChartDefinition}
          payload={{
            drillingByGroupsLineChart: drillingByGroupsLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingByGroupsLineChartDefinition[0].label +
        "\n" +
        drillingByGroupsLineChartDefinition[0].description
    }
  );

storiesOf("3) charts/line charts/legends & labels", module)
  .add(
    legendBottomLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={legendBottomLineChartDefinition}
          payload={{
            legendBottomLineChart: legendBottomLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendBottomLineChartDefinition[0].label +
        "\n" +
        legendBottomLineChartDefinition[0].description
    }
  )
  .add(
    legendTopLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={legendTopLineChartDefinition}
          payload={{
            legendTopLineChart: legendTopLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendTopLineChartDefinition[0].label +
        "\n" +
        legendTopLineChartDefinition[0].description
    }
  )
  .add(
    legendRightLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={legendRightLineChartDefinition}
          payload={{
            legendRightLineChart: legendRightLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendRightLineChartDefinition[0].label +
        "\n" +
        legendRightLineChartDefinition[0].description
    }
  )
  .add(
    legendLeftLineChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={lineChart}
          definitions={legendLeftLineChartDefinition}
          payload={{
            legendLeftLineChart: legendLeftLineChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendLeftLineChartDefinition[0].label +
        "\n" +
        legendLeftLineChartDefinition[0].description
    }
  );
