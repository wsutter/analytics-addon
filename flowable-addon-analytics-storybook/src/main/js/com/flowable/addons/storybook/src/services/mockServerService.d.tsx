export interface MockServerServiceInterface {
  get(url: string, parameters?: any): Promise<Array<string>>;

  put(url: string, body: string, parameters?: any): Promise<Array<string>>;
}
