let CleanWebpackPlugin = require("clean-webpack-plugin");
let CopyWebpackPlugin = require("copy-webpack-plugin");
let MiniCssExtractPlugin = require("mini-css-extract-plugin");
let CircularDependencyPlugin = require("circular-dependency-plugin");

const path = require("path");

const cleanPlugin = new CleanWebpackPlugin(path.resolve(__dirname, "build"), {
  verbose: true
});

const copyPlugin = new CopyWebpackPlugin(
  [
    {
      from: path.resolve(__dirname, "assets/favicon"),
      to: path.resolve(__dirname, "build/dist")
    },
    {
      from: path.resolve(__dirname, "assets/svg"),
      to: path.resolve(__dirname, "build/dist/assets/svg")
    },
    {
      from: path.resolve(__dirname, "../servers/resources"),
      to: path.resolve(__dirname, "build/dist")
    },
    {
      from: path.resolve(
        __dirname,
        "./node_modules/@flowable/addon-analytics-react/*"
      ),
      to: path.resolve(__dirname, "build/dist"),
      flatten: true
    }
  ],
  {
    debug: "warning"
  }
);

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: "storybook.css"
});

const circularDependencyPlugin = new CircularDependencyPlugin({
  exclude: /node_modules/,
  failOnError: true,
  allowAsyncCycles: true,
  cwd: process.cwd()
});

module.exports = {
  entry: "./stories/index.stories.tsx",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".scss", ".css"]
  },
  mode: "production",
  optimization: {
    occurrenceOrder: true,
    noEmitOnErrors: true,
    minimize: false,
    mergeDuplicateChunks: false
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: MiniCssExtractPlugin.loader as string
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: false,
              minimize: false,
              importLoaders: 1
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: false
            }
          }
        ]
      },
      {
        test: /\.(woff2?|ttf|eot)?$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/"
            }
          }
        ]
      },
      {
        test: /\.tsx?$/,
        exclude: [/node_modules/, /test/, /mocks/, /\.(test|spec|e2e)\.tsx?$/],
        use: [
          {
            loader: "awesome-typescript-loader",
            options: {
              logLevel: "debug",
              silent: true,
              useCache: true
            }
          }
        ]
      },
      {
        test: /\.jsx$/,
        exclude: [/node_modules/, /test/, /mocks/, /\.(test|spec|e2e)\.jsx?$/],
        use: [
          {
            loader: "babel-loader",
            options: {
              cacheDirectory: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    cleanPlugin,
    copyPlugin,
    miniCssExtractPlugin,
    circularDependencyPlugin
  ]
};
