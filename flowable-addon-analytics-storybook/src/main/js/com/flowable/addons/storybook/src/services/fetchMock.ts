import { _ } from "@flowable/forms-react";

type Resp = {
  json: () => Promise<any>;
  body: any;
};

const cache = {};

function fetchMock(url: string, opts: any, onProgress?: any): Promise<Resp> {
  if (!opts || !opts.method || opts.method === "GET") {
    if (cache[url]) {
      return Promise.resolve({
        json: () => Promise.resolve(cache[url]),
        body: cache[url],
        headers: [],
        ok: true
      });
    } else {
      return _.futch(url, opts, onProgress).then(response => {
        response.json().then(data => (cache[url] = data));

        return response;
      });
    }
  } else if (opts.method === "PUT") {
    cache[url] = JSON.parse(opts.body);

    return Promise.resolve({
      json: () => Promise.resolve({ success: true }),
      body: cache[url],
      headers: [],
      ok: true
    });
  }
  return Promise.reject();
}

export { fetchMock };
