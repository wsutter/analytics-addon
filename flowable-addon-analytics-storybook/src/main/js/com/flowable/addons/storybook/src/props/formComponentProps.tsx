import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";

export class FormContainerComponentProps {
  public Components: any = {};
  public definitions: Array<Definition> = [];
  public payload: Model.Payload = {};
  public additionalData: Model.Payload = {};
}
