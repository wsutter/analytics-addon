import { FormDefinitionServiceInterface } from "./formDefinitionService.d";
import { Rows } from "../entities/rows";
import { Definition } from "../entities/definition";
import { Columns } from "../entities/columns";

export class FormDefinitionService implements FormDefinitionServiceInterface {
  public static getFormLayout(definitions: any): Rows {
    return Object.assign(new Rows(), {
      rows: [
        {
          cols: FormDefinitionService.getColumns(definitions as Array<
            Definition
          >)
        }
      ]
    });
  }

  public static getColumns(definitions: Array<Definition>): Array<Definition> {
    return definitions.map(definition => {
      let size = definition.size;
      return FormDefinitionService.getDefaultControl(
        Object.assign(new Columns(), definition, { size })
      );
    });
  }

  public static getDefaultControl(object: any): Definition {
    return Object.assign(new Definition(), object);
  }
}
