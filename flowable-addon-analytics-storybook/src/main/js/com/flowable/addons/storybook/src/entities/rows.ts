import { FormLayout, Row } from "@flowable/forms-react/Model";

export class Rows implements FormLayout {
  rows: Array<Row> = [];
}
