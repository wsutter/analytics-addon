import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import heatmapChart from "@flowable/addon-analytics-react/heatmapChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const simpleHeatmapChartDefinition = require("../../chartDefinitions/heatmapCharts/simpleHeatmapChart.json") as Array<
  Definition
>;
const simpleHeatmapChartState = require("../../chartStates/heatmapCharts/simpleHeatmapChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

storiesOf("3) charts/heatmap charts", module).add(
  simpleHeatmapChartDefinition[0].label,
  () => {
    return (
      <FormContainer
        Components={heatmapChart}
        definitions={simpleHeatmapChartDefinition}
        payload={{
          simpleHeatmapChart: simpleHeatmapChartState
        }}
        additionalData={additionalData}
      />
    );
  },
  {
    notes:
      simpleHeatmapChartDefinition[0].label +
      "\n" +
      simpleHeatmapChartDefinition[0].description
  }
);
