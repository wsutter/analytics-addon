import React from "react";

import merge from "lodash.merge";
import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import barChart from "@flowable/addon-analytics-react/barChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";

const simpleBarChartDefinition = require("../../chartDefinitions/barCharts/simpleBarChart.json") as Array<
  Definition
>;
const simpleBarChartState = require("../../chartStates/barCharts/simpleBarChart.json") as Model.Payload;
const fullyFledgedBarChartDefinition = require("../../chartDefinitions/barCharts/fullyFledgedBarChart.json") as Array<
  Definition
>;
const fullyFledgedBarChartState = require("../../chartStates/barCharts/fullyFledgedBarChart.json") as Model.Payload;

const customColorsBarChartDefinition = require("../../chartDefinitions/barCharts/customColorsBarChart.json") as Array<
  Definition
>;
const customColorsBarChartState = require("../../chartStates/barCharts/customColorsBarChart.json") as Model.Payload;
const colorsByGroupsBarChartDefinition = require("../../chartDefinitions/barCharts/colorsByGroupsBarChart.json") as Array<
  Definition
>;
const colorsByGroupsBarChartState = require("../../chartStates/barCharts/colorsByGroupsBarChart.json") as Model.Payload;
const colorsByThresholdsBarChartDefinition = require("../../chartDefinitions/barCharts/colorsByThresholdsBarChart.json") as Array<
  Definition
>;
const colorsByThresholdsBarChartState = require("../../chartStates/barCharts/colorsByThresholdsBarChart.json") as Model.Payload;

const drillingSimpleBarChartDefinition = require("../../chartDefinitions/barCharts/drillingSimpleBarChart.json") as Array<
  Definition
>;
const drillingSimpleBarChartState = require("../../chartStates/barCharts/drillingSimpleBarChart.json") as Model.Payload;
const drillingByGroupsBarChartDefinition = require("../../chartDefinitions/barCharts/drillingByGroupsBarChart.json") as Array<
  Definition
>;
const drillingByGroupsBarChartState = require("../../chartStates/barCharts/drillingByGroupsBarChart.json") as Model.Payload;

const legendBottomBarChartDefinition = require("../../chartDefinitions/barCharts/legendBottomBarChart.json") as Array<
  Definition
>;
const legendBottomBarChartState = require("../../chartStates/barCharts/legendBottomBarChart.json") as Model.Payload;
const legendTopBarChartDefinition = require("../../chartDefinitions/barCharts/legendTopBarChart.json") as Array<
  Definition
>;
const legendTopBarChartState = require("../../chartStates/barCharts/legendTopBarChart.json") as Model.Payload;
const legendLeftBarChartDefinition = require("../../chartDefinitions/barCharts/legendLeftBarChart.json") as Array<
  Definition
>;
const legendLeftBarChartState = require("../../chartStates/barCharts/legendLeftBarChart.json") as Model.Payload;
const legendRightBarChartDefinition = require("../../chartDefinitions/barCharts/legendRightBarChart.json") as Array<
  Definition
>;
const legendRightBarChartState = require("../../chartStates/barCharts/legendRightBarChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

// Stories
storiesOf("3) charts/bar charts", module)
  .add(
    simpleBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={simpleBarChartDefinition}
          payload={{
            simpleBarChart: simpleBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        simpleBarChartDefinition[0].label +
        "\n" +
        simpleBarChartDefinition[0].description
    }
  )
  .add(
    fullyFledgedBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={fullyFledgedBarChartDefinition}
          payload={{
            fullyFledgedBarChart: fullyFledgedBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        fullyFledgedBarChartDefinition[0].label +
        "\n" +
        fullyFledgedBarChartDefinition[0].description
    }
  );

storiesOf("3) charts/bar charts/colors", module)
  .add(
    customColorsBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={customColorsBarChartDefinition}
          payload={{
            customColorsBarChart: customColorsBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        customColorsBarChartDefinition[0].label +
        "\n" +
        customColorsBarChartDefinition[0].description
    }
  )
  .add(
    colorsByGroupsBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={colorsByGroupsBarChartDefinition}
          payload={{
            colorsByGroupsBarChart: colorsByGroupsBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByGroupsBarChartDefinition[0].label +
        "\n" +
        colorsByGroupsBarChartDefinition[0].description
    }
  )
  .add(
    colorsByThresholdsBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={colorsByThresholdsBarChartDefinition}
          payload={{
            colorsByThresholdsBarChart: colorsByThresholdsBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByThresholdsBarChartDefinition[0].label +
        "\n" +
        colorsByThresholdsBarChartDefinition[0].description
    }
  );

storiesOf("3) charts/bar charts/drilling", module)
  .add(
    drillingSimpleBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={drillingSimpleBarChartDefinition}
          payload={{
            drillingSimpleBarChart: drillingSimpleBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingSimpleBarChartDefinition[0].label +
        "\n" +
        drillingSimpleBarChartDefinition[0].description
    }
  )
  .add(
    drillingByGroupsBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={drillingByGroupsBarChartDefinition}
          payload={{
            drillingByGroupsBarChart: drillingByGroupsBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingByGroupsBarChartDefinition[0].label +
        "\n" +
        drillingByGroupsBarChartDefinition[0].description
    }
  );

storiesOf("3) charts/bar charts/legends & labels", module)
  .add(
    legendBottomBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={legendBottomBarChartDefinition}
          payload={{
            legendBottomBarChart: legendBottomBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendBottomBarChartDefinition[0].label +
        "\n" +
        legendBottomBarChartDefinition[0].description
    }
  )
  .add(
    legendTopBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={legendTopBarChartDefinition}
          payload={{
            legendTopBarChart: legendTopBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendTopBarChartDefinition[0].label +
        "\n" +
        legendTopBarChartDefinition[0].description
    }
  )
  .add(
    legendRightBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={legendRightBarChartDefinition}
          payload={{
            legendRightBarChart: legendRightBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendRightBarChartDefinition[0].label +
        "\n" +
        legendRightBarChartDefinition[0].description
    }
  )
  .add(
    legendLeftBarChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={barChart}
          definitions={legendLeftBarChartDefinition}
          payload={{
            legendLeftBarChart: legendLeftBarChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendLeftBarChartDefinition[0].label +
        "\n" +
        legendLeftBarChartDefinition[0].description
    }
  );
