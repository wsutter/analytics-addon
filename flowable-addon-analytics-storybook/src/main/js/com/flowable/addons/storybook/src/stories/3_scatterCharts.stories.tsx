import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import scatterChart from "@flowable/addon-analytics-react/scatterChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const simpleScatterChartDefinition = require("../../chartDefinitions/scatterCharts/simpleScatterChart.json") as Array<
  Definition
>;
const simpleScatterChartState = require("../../chartStates/scatterCharts/simpleScatterChart.json") as Model.Payload;
const fullyFledgedScatterChartDefinition = require("../../chartDefinitions/scatterCharts/fullyFledgedScatterChart.json") as Array<
  Definition
>;
const fullyFledgedScatterChartState = require("../../chartStates/scatterCharts/simpleScatterChart.json") as Model.Payload;

const customColorsScatterChartDefinition = require("../../chartDefinitions/scatterCharts/customColorsScatterChart.json") as Array<
  Definition
>;
const customColorsScatterChartState = require("../../chartStates/scatterCharts/customColorsScatterChart.json") as Model.Payload;
const colorsByGroupsScatterChartDefinition = require("../../chartDefinitions/scatterCharts/colorsByGroupsScatterChart.json") as Array<
  Definition
>;
const colorsByGroupsScatterChartState = require("../../chartStates/scatterCharts/colorsByGroupsScatterChart.json") as Model.Payload;
const colorsByThresholdsScatterChartDefinition = require("../../chartDefinitions/scatterCharts/colorsByThresholdsScatterChart.json") as Array<
  Definition
>;
const colorsByThresholdsScatterChartState = require("../../chartStates/scatterCharts/colorsByThresholdsScatterChart.json") as Model.Payload;

const drillingSimpleScatterChartDefinition = require("../../chartDefinitions/scatterCharts/drillingSimpleScatterChart.json") as Array<
  Definition
>;
const drillingSimpleScatterChartState = require("../../chartStates/scatterCharts/drillingSimpleScatterChart.json") as Model.Payload;
const drillingByGroupsScatterChartDefinition = require("../../chartDefinitions/scatterCharts/drillingByGroupsScatterChart.json") as Array<
  Definition
>;
const drillingByGroupsScatterChartState = require("../../chartStates/scatterCharts/drillingByGroupsScatterChart.json") as Model.Payload;

const legendBottomScatterChartDefinition = require("../../chartDefinitions/scatterCharts/legendBottomScatterChart.json") as Array<
  Definition
>;
const legendBottomScatterChartState = require("../../chartStates/scatterCharts/legendBottomScatterChart.json") as Model.Payload;
const legendTopScatterChartDefinition = require("../../chartDefinitions/scatterCharts/legendTopScatterChart.json") as Array<
  Definition
>;
const legendTopScatterChartState = require("../../chartStates/scatterCharts/legendTopScatterChart.json") as Model.Payload;
const legendLeftScatterChartDefinition = require("../../chartDefinitions/scatterCharts/legendLeftScatterChart.json") as Array<
  Definition
>;
const legendLeftScatterChartState = require("../../chartStates/scatterCharts/legendLeftScatterChart.json") as Model.Payload;
const legendRightScatterChartDefinition = require("../../chartDefinitions/scatterCharts/legendRightScatterChart.json") as Array<
  Definition
>;
const legendRightScatterChartState = require("../../chartStates/scatterCharts/legendRightScatterChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

storiesOf("3) charts/scatter charts", module)
  .add(
    simpleScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={simpleScatterChartDefinition}
          payload={{
            simpleScatterChart: simpleScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        simpleScatterChartDefinition[0].label +
        "\n" +
        simpleScatterChartDefinition[0].description
    }
  )
  .add(
    fullyFledgedScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={fullyFledgedScatterChartDefinition}
          payload={{
            fullyFledgedScatterChart: fullyFledgedScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        fullyFledgedScatterChartDefinition[0].label +
        "\n" +
        fullyFledgedScatterChartDefinition[0].description
    }
  );

storiesOf("3) charts/scatter charts/colors", module)
  .add(
    customColorsScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={customColorsScatterChartDefinition}
          payload={{
            customColorsScatterChart: customColorsScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        customColorsScatterChartDefinition[0].label +
        "\n" +
        customColorsScatterChartDefinition[0].description
    }
  )
  .add(
    colorsByGroupsScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={colorsByGroupsScatterChartDefinition}
          payload={{
            colorsByGroupsScatterChart: colorsByGroupsScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByGroupsScatterChartDefinition[0].label +
        "\n" +
        colorsByGroupsScatterChartDefinition[0].description
    }
  )
  .add(
    colorsByThresholdsScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={colorsByThresholdsScatterChartDefinition}
          payload={{
            colorsByThresholdsScatterChart: colorsByThresholdsScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByThresholdsScatterChartDefinition[0].label +
        "\n" +
        colorsByThresholdsScatterChartDefinition[0].description
    }
  );

storiesOf("3) charts/scatter charts/drilling", module)
  .add(
    drillingSimpleScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={drillingSimpleScatterChartDefinition}
          payload={{
            drillingSimpleScatterChart: drillingSimpleScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingSimpleScatterChartDefinition[0].label +
        "\n" +
        drillingSimpleScatterChartDefinition[0].description
    }
  )
  .add(
    drillingByGroupsScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={drillingByGroupsScatterChartDefinition}
          payload={{
            drillingByGroupsScatterChart: drillingByGroupsScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingByGroupsScatterChartDefinition[0].label +
        "\n" +
        drillingByGroupsScatterChartDefinition[0].description
    }
  );

storiesOf("3) charts/scatter charts/legends & labels", module)
  .add(
    legendBottomScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={legendBottomScatterChartDefinition}
          payload={{
            legendBottomScatterChart: legendBottomScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendBottomScatterChartDefinition[0].label +
        "\n" +
        legendBottomScatterChartDefinition[0].description
    }
  )
  .add(
    legendTopScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={legendTopScatterChartDefinition}
          payload={{
            legendTopScatterChart: legendTopScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendTopScatterChartDefinition[0].label +
        "\n" +
        legendTopScatterChartDefinition[0].description
    }
  )
  .add(
    legendRightScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={legendRightScatterChartDefinition}
          payload={{
            legendRightScatterChart: legendRightScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendRightScatterChartDefinition[0].label +
        "\n" +
        legendRightScatterChartDefinition[0].description
    }
  )
  .add(
    legendLeftScatterChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={scatterChart}
          definitions={legendLeftScatterChartDefinition}
          payload={{
            legendLeftScatterChart: legendLeftScatterChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendLeftScatterChartDefinition[0].label +
        "\n" +
        legendLeftScatterChartDefinition[0].description
    }
  );
