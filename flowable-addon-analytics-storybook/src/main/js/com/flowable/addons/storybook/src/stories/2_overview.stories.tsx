import React from "react";
import { storiesOf } from "@storybook/react";
import { FormControlled } from "@flowable/forms-react";
import * as Analytics from "@flowable/addon-analytics-react";
import { Definition } from "../entities/definition";
import { FormDefinitionService } from "../services/formDefinitionService";

const simpleChartsDefinition = require("../../chartDefinitions/overview/simpleCharts.json") as Array<
  Definition
>;
const fullyFledgedChartsDefinition = require("../../chartDefinitions/overview/fullyFledgedCharts.json") as Array<
  Definition
>;

storiesOf("2) overview", module)
  .add("simple charts", () => {
    let formLayout = FormDefinitionService.getFormLayout(
      simpleChartsDefinition
    );

    return (
      <div>
        <FormControlled
          Components={Analytics}
          config={formLayout}
          payload={{}}
          onChange={() => {}}
        />
      </div>
    );
  })
  .add("fully fledged charts", () => {
    let formLayout = FormDefinitionService.getFormLayout(
      fullyFledgedChartsDefinition
    );

    return (
      <div>
        <FormControlled
          Components={Analytics}
          config={formLayout}
          payload={{}}
          onChange={() => {}}
        />
      </div>
    );
  });
