import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import * as Analytics from "@flowable/addon-analytics-react";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const lineChartFormDefinition = require("../../chartDefinitions/integrationTests/lineChartForm.json") as Array<
  Definition
>;
const lineChartState = require("../../chartStates/integrationTests/lineChart.json") as Model.Payload;

const pieChartFormDefinition = require("../../chartDefinitions/integrationTests/pieChartForm.json") as Array<
  Definition
>;
const pieChartState = require("../../chartStates/integrationTests/pieChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

storiesOf("4) integration tests/line chart forms", module).add(
  lineChartFormDefinition[0].label,
  () => {
    return (
      <FormContainer
        Components={Analytics}
        definitions={lineChartFormDefinition}
        payload={{
          lineChart: lineChartState
        }}
        additionalData={additionalData}
      />
    );
  },
  {
    notes:
      lineChartFormDefinition[0].label +
      "\n" +
      lineChartFormDefinition[0].description
  }
);

storiesOf("4) integration tests/pie chart forms", module).add(
  pieChartFormDefinition[0].label,
  () => {
    return (
      <FormContainer
        Components={Analytics}
        definitions={pieChartFormDefinition}
        payload={{
          pieChart: pieChartState
        }}
        additionalData={additionalData}
      />
    );
  },
  {
    notes:
      pieChartFormDefinition[0].label +
      "\n" +
      pieChartFormDefinition[0].description
  }
);
