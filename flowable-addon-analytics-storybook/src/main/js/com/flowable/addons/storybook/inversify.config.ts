import "reflect-metadata";
import { Container } from "inversify";
import { TYPES } from "./types.config";
import { MockServerService } from "./src/services/mockServerService";
import { MockServerServiceInterface } from "./src/services/mockServerService.d";

const analyticsAddonStorybookContainer = new Container();

analyticsAddonStorybookContainer
  .bind<MockServerServiceInterface>(TYPES.MockServerService)
  .to(MockServerService)
  .inSingletonScope();

export { analyticsAddonStorybookContainer };
