import "@babel/runtime/regenerator";
import { addDecorator, configure } from "@storybook/react";
import { withOptions } from "@storybook/addon-options";

addDecorator(
  withOptions({
    /**
     * name to display in the top left corner
     * @type {String}
     */
    name: "@flowable/addon-analytics-storybook",
    /**
     * URL for name in top left corner to link to
     * @type {String}
     */
    url: "#",
    /**
     * show story component as full screen
     * @type {Boolean}
     */
    goFullScreen: false,
    /**
     * display panel that shows a list of stories
     * @type {Boolean}
     */
    showStoriesPanel: true,
    /**
     * display panel that shows addon configurations
     * @type {Boolean}
     */
    showAddonPanel: true,
    /**
     * display floating search box to search through stories
     * @type {Boolean}
     */
    showSearchBox: false,
    /**
     * show addon panel as a vertical panel on the right
     * @type {Boolean}
     */
    addonPanelInRight: false,
    /**
     * sorts stories
     * @type {Boolean}
     */
    sortStoriesByKind: true,
    /**
     * id to select an addon panel
     * @type {String}
     */
    selectedAddonPanel: undefined, // The order of addons in the "Addon panel" is the same as you import them in 'addons.js'. The first panel will be opened by default as you run Storybook,
    theme: {
      mainBackground: "#1F2A44",
      mainBorder: "#E1523D",
      mainBorderRadius: 4,
      mainFill: "#ffffff",
      barFill: "#A4BCC2",
      inputFill: "#ffffff",
      mainTextFace: '"montserrat-medium-webfont", sans-serif',
      mainTextColor: "#ffffff",
      mainTextSize: 20,
      dimmedTextColor: "#A4BCC2",
      highlightTextColor: "#E1523D",
      successColor: "green",
      failColor: "red",
      warnColor: "orange",
      monoTextFace: '"montserrat-medium-webfont", sans-serif',
      layoutMargin: 5,
      overlayBackground:
        "linear-gradient(to bottom right, rgba(233, 233, 233, 0.6), rgba(255, 255, 255, 0.8))"
    }
  })
);

// automatically import all files ending in *.stories.js
const req = require.context("./src/stories", true, /.stories.js$/);

configure(() => {
  req.keys().forEach(filename => req(filename));
}, module);
