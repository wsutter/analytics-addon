import React from "react";

import { addDecorator, storiesOf } from "@storybook/react";
import { withNotes } from "@storybook/addon-notes";
import areaChart from "@flowable/addon-analytics-react/areaChart";
import { FormContainer } from "../../components/formContainer";
import { Definition } from "../entities/definition";
import { Model } from "@flowable/forms-react";
import merge from "lodash.merge";

const simpleAreaChartDefinition = require("../../chartDefinitions/areaCharts/simpleAreaChart.json") as Array<
  Definition
>;
const simpleAreaChartState = require("../../chartStates/areaCharts/simpleAreaChart.json") as Model.Payload;
const fullyFledgedAreaChartDefinition = require("../../chartDefinitions/areaCharts/fullyFledgedAreaChart.json") as Array<
  Definition
>;
const fullyFledgedAreaChartState = require("../../chartStates/areaCharts/fullyFledgedAreaChart.json") as Model.Payload;

const customColorsAreaChartDefinition = require("../../chartDefinitions/areaCharts/customColorsAreaChart.json") as Array<
  Definition
>;
const customColorsAreaChartState = require("../../chartStates/areaCharts/customColorsAreaChart.json") as Model.Payload;
const colorsByGroupsAreaChartDefinition = require("../../chartDefinitions/areaCharts/colorsByGroupsAreaChart.json") as Array<
  Definition
>;
const colorsByGroupsAreaChartState = require("../../chartStates/areaCharts/colorsByGroupsAreaChart.json") as Model.Payload;
const colorsByThresholdsAreaChartDefinition = require("../../chartDefinitions/areaCharts/colorsByThresholdsAreaChart.json") as Array<
  Definition
>;
const colorsByThresholdsAreaChartState = require("../../chartStates/areaCharts/colorsByThresholdsAreaChart.json") as Model.Payload;

const drillingSimpleAreaChartDefinition = require("../../chartDefinitions/areaCharts/drillingSimpleAreaChart.json") as Array<
  Definition
>;
const drillingSimpleAreaChartState = require("../../chartStates/areaCharts/drillingSimpleAreaChart.json") as Model.Payload;
const drillingByGroupsAreaChartDefinition = require("../../chartDefinitions/areaCharts/drillingByGroupsAreaChart.json") as Array<
  Definition
>;
const drillingByGroupsAreaChartState = require("../../chartStates/areaCharts/drillingByGroupsAreaChart.json") as Model.Payload;

const legendBottomAreaChartDefinition = require("../../chartDefinitions/areaCharts/legendBottomAreaChart.json") as Array<
  Definition
>;
const legendBottomAreaChartState = require("../../chartStates/areaCharts/legendBottomAreaChart.json") as Model.Payload;
const legendTopAreaChartDefinition = require("../../chartDefinitions/areaCharts/legendTopAreaChart.json") as Array<
  Definition
>;
const legendTopAreaChartState = require("../../chartStates/areaCharts/legendTopAreaChart.json") as Model.Payload;
const legendLeftAreaChartDefinition = require("../../chartDefinitions/areaCharts/legendLeftAreaChart.json") as Array<
  Definition
>;
const legendLeftAreaChartState = require("../../chartStates/areaCharts/legendLeftAreaChart.json") as Model.Payload;
const legendRightAreaChartDefinition = require("../../chartDefinitions/areaCharts/legendRightAreaChart.json") as Array<
  Definition
>;
const legendRightAreaChartState = require("../../chartStates/areaCharts/legendRightAreaChart.json") as Model.Payload;

const functions = {
  stringify: (toBeStringified: string) => {
    return JSON.stringify(toBeStringified);
  }
};

const additionalData = merge(functions);

addDecorator(withNotes);

storiesOf("3) charts/area charts", module)
  .add(
    simpleAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={simpleAreaChartDefinition}
          payload={{
            simpleAreaChart: simpleAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        simpleAreaChartDefinition[0].label +
        "\n" +
        simpleAreaChartDefinition[0].description
    }
  )
  .add(
    fullyFledgedAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={fullyFledgedAreaChartDefinition}
          payload={{
            fullyFledgedAreaChart: fullyFledgedAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        fullyFledgedAreaChartDefinition[0].label +
        "\n" +
        fullyFledgedAreaChartDefinition[0].description
    }
  );

storiesOf("3) charts/area charts/colors", module)
  .add(
    customColorsAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={customColorsAreaChartDefinition}
          payload={{
            customColorsAreaChart: customColorsAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        customColorsAreaChartDefinition[0].label +
        "\n" +
        customColorsAreaChartDefinition[0].description
    }
  )
  .add(
    colorsByGroupsAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={colorsByGroupsAreaChartDefinition}
          payload={{
            colorsByGroupsAreaChart: colorsByGroupsAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByGroupsAreaChartDefinition[0].label +
        "\n" +
        colorsByGroupsAreaChartDefinition[0].description
    }
  )
  .add(
    colorsByThresholdsAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={colorsByThresholdsAreaChartDefinition}
          payload={{
            colorsByThresholdsAreaChart: colorsByThresholdsAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        colorsByThresholdsAreaChartDefinition[0].label +
        "\n" +
        colorsByThresholdsAreaChartDefinition[0].description
    }
  );

storiesOf("3) charts/area charts/drilling", module)
  .add(
    drillingSimpleAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={drillingSimpleAreaChartDefinition}
          payload={{
            drillingSimpleAreaChart: drillingSimpleAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingSimpleAreaChartDefinition[0].label +
        "\n" +
        drillingSimpleAreaChartDefinition[0].description
    }
  )
  .add(
    drillingByGroupsAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={drillingByGroupsAreaChartDefinition}
          payload={{
            drillingByGroupsAreaChart: drillingByGroupsAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        drillingByGroupsAreaChartDefinition[0].label +
        "\n" +
        drillingByGroupsAreaChartDefinition[0].description
    }
  );

storiesOf("3) charts/area charts/legends & labels", module)
  .add(
    legendBottomAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={legendBottomAreaChartDefinition}
          payload={{
            legendBottomAreaChart: legendBottomAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendBottomAreaChartDefinition[0].label +
        "\n" +
        legendBottomAreaChartDefinition[0].description
    }
  )
  .add(
    legendTopAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={legendTopAreaChartDefinition}
          payload={{
            legendTopAreaChart: legendTopAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendTopAreaChartDefinition[0].label +
        "\n" +
        legendTopAreaChartDefinition[0].description
    }
  )
  .add(
    legendRightAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={legendRightAreaChartDefinition}
          payload={{
            legendRightAreaChart: legendRightAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendRightAreaChartDefinition[0].label +
        "\n" +
        legendRightAreaChartDefinition[0].description
    }
  )
  .add(
    legendLeftAreaChartDefinition[0].label,
    () => {
      return (
        <FormContainer
          Components={areaChart}
          definitions={legendLeftAreaChartDefinition}
          payload={{
            legendLeftAreaChart: legendLeftAreaChartState
          }}
          additionalData={additionalData}
        />
      );
    },
    {
      notes:
        legendLeftAreaChartDefinition[0].label +
        "\n" +
        legendLeftAreaChartDefinition[0].description
    }
  );
