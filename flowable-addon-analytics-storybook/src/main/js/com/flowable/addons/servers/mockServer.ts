import * as express from 'express';
import {NextFunction, Request, Response} from 'express';
import * as fs from 'fs';
import * as bodyParser from 'body-parser';
import * as xxHash from 'xxhashjs';

let isUndefined = require('lodash.isundefined');

let mockServer = express();

let parameters: any = {};
for (let i = 0; i < process.argv.slice(2).length; i += 2) {

  parameters[process.argv.slice(2)[i]] = process.argv.slice(2)[i + 1];

}

let port = 3000;
if (parameters['-p'] !== undefined) {

  port = parameters['-p'];

}

let cache: any = {};

mockServer.use(
  (request: Request, response: Response, next: NextFunction) => {

    response.header(
      'Access-Control-Allow-Origin',
      '*');

    response.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept');

    next();

  }
);

mockServer.use(
  bodyParser.json());

mockServer.listen(port);

mockServer.get(
  '/rest/addons/index/find/query/(:chartType)?',
  (request: Request, response: Response, next: NextFunction) => {

    let cacheKey = xxHash
      .h32(
        request.originalUrl,
        0xABCD)
      .toString(16);

    let data = cache[cacheKey];

    if (isUndefined(data)) {

      if (!isUndefined(request.params['chartType'])) {

        data = cache[cacheKey] = fs.readFileSync(
          './resources/rest/addons/index/find/query/' + request.params['chartType'] + '/response.json');

      } else {

        data = cache[cacheKey] = fs.readFileSync(
          './resources/rest/addons/index/find/query/' + request.query._index + '/' + request.query._type + '/' + request.query.group + '/response.json');

      }

    }

    response
      .status(200)
      .contentType('application/json')
      .send(
        data);

    next();

  }
);

mockServer.put(
  '/rest/addons/index/find/query/(:chartType)?',
  (request: Request, response: Response) => {

    if (request.get('Content-Type') !== 'application/json') {

      response
        .status(406)
        .contentType('application/json')
        .send({
          'success': false
        });

    } else {

      try {

        JSON.parse(
          JSON.stringify(
            request.body));

        let cacheKey = xxHash
          .h32(
            request.originalUrl,
            0xABCD)
          .toString(16);

        cache[cacheKey] = request.body;

        response
          .status(200)
          .contentType('application/json')
          .send({
            'success': true
          });

      } catch (e) {

        response
          .status(400)
          .contentType('application/json')
          .send({
            'success': false
          });

      }

    }

  }
);

mockServer.get(
  '/rest/:resource',
  (request: Request, response: Response, next: NextFunction) => {

    let cacheKey = xxHash
      .h32(
        request.originalUrl,
        0xABCD)
      .toString(16);

    let data = cache[cacheKey];

    if (isUndefined(data)) {

      data = cache[cacheKey] = fs.readFileSync(
        './resources/rest/' + request.params['resource'] + '/response.json')
        .toString();

    }

    response
      .status(200)
      .contentType('application/json')
      .send(
        data);

    next();

  }
);

mockServer.put(
  '/rest/:resource',
  (request: Request, response: Response) => {

    if (request.get('Content-Type') !== 'application/json') {

      response
        .status(406)
        .contentType('application/json')
        .send({
          'success': false
        });

    } else {

      try {

        JSON.parse(
          JSON.stringify(
            request.body));

        let cacheKey = xxHash
          .h32(
            request.originalUrl,
            0xABCD)
          .toString(16);

        cache[cacheKey] = request.body;

        response
          .status(200)
          .contentType('application/json')
          .send({
            'success': true
          });

      } catch (e) {

        response
          .status(400)
          .contentType('application/json')
          .send({
            'success': false
          });

      }

    }

  }
);

mockServer.get(
  '/maps/:map',
  (request: Request, response: Response, next: NextFunction) => {

    let cacheKey = xxHash
      .h32(
        request.originalUrl,
        0xABCD)
      .toString(16);

    let data = cache[cacheKey];

    if (isUndefined(data)) {

      data = cache[cacheKey] = fs.readFileSync(
        './resources/maps/' + request.params['map']);

    }

    response
      .status(200)
      .contentType('application/json')
      .send(
        data);

    next();

  }
);

mockServer.put(
  '/rest/currentUser',
  (request: Request, response: Response) => {

    if (request.get('Content-Type') !== 'application/json') {

      response
        .status(406)
        .contentType('application/json')
        .send({
          'success': false
        });

    } else {

      try {

        JSON.parse(
          JSON.stringify(
            request.body));

        let cacheKey = xxHash
          .h32(
            request.originalUrl,
            0xABCD)
          .toString(16);

        cache[cacheKey] = request.body;

        response
          .status(200)
          .contentType('application/json')
          .send({
            'success': true
          });

      } catch (e) {

        response
          .status(400)
          .contentType('application/json')
          .send({
            'success': false
          });

      }

    }

  }
);

mockServer.put(
  '/rest/reset',
  (request: Request, response: Response, next: NextFunction) => {

    if (request.get('Content-Type') !== 'application/json') {

      response
        .status(406)
        .contentType('application/json')
        .send({
          'success': false
        });

    } else {

      try {

        JSON.parse(
          JSON.stringify(
            request.body));

        if (request.body['reset']) {

          cache = {};

          response
            .status(200)
            .contentType('application/json')
            .send({
              'success': true
            });

        } else {

          response
            .status(400)
            .contentType('application/json')
            .send({
              'success': false
            });

        }

      } catch (e) {

        response
          .status(400)
          .contentType('application/json')
          .send({
            'success': false
          });

      }

    }

    next();

  }
);

console.log(`Mock server is running on port: ${port}`);