(window.webpackJsonpflwAddonAnalytics=window.webpackJsonpflwAddonAnalytics||[]).push([[14],{400:function(e,t,l){"use strict";l.r(t);var a,i=l(12),o=l(5),r=l(11),n=l(1),s=l(40),c=l.n(s),d=l(54),p=l.n(d),h=l(53),g=l.n(h),u=l(425),m=l(260),y=l(654),v=l(432),f=l(433),S=l(674),b=l(677),C=l(673),k=l(518),E=l(69),w=l(17),D=l(2),x=l(261),A=l(32),R=l(23),P=l(437),M=l(429),z=l(42),U=l(51),O=l(655);let $=class extends u.a{constructor(e){super(e),this.state=(new class extends m.a{constructor(){super(...arguments),this.colorScale={},this.thresholdInPercentages=[]}deserialize(e,t){return super.deserialize(e,t),this}}).deserialize(this,e.extraSettings.state)}get ticksVisibility(){return this.props.extraSettings.ticksVisibility}get innerRadius(){return this.props.extraSettings.innerRadius}get outerRadius(){return this.props.extraSettings.outerRadius}static getDerivedStateFromProps(e,t){return JSON.stringify(e.formControlled)!==JSON.stringify(t.props)&&(t.props=e.formControlled),t}shouldComponentUpdate(e,t){return!E._.equals(this.props,e)||t.shouldUpdate}render(){let e=this;if(e.state.shouldUpdate){let t=(t,l,a,r,s)=>{let c,d,p,g,m,S=e.hashService.getAggregationsHash(e,t),b=e.value.angle;if(void 0===e.value.font){let e=new w.a;c=e.family,d=e.size,p=e.color}else c=e.value.font.family,d=e.value.font.size,p=e.value.font.color;return g=void 0===e.innerRadius?Math.floor(100*(l+1)):Math.floor(100*(l+e.innerRadius)),m=void 0===e.outerRadius?Math.floor(100*(l+2)):Math.floor(100*(l+e.outerRadius)),n.createElement(y.a,{id:"analytics-pie-chart-pie-"+S,key:"analytics-pie-chart-pie-"+S,data:t,nameKey:h,dataKey:u,className:"analytics-pie-chart-pie",animationDuration:1e3*e.transition.duration,animationEasing:e.transition.algorithm,paddingAngle:e.padding.angle,innerRadius:g,outerRadius:m,startAngle:a,endAngle:r,cx:e.chartWidth/2,cy:e.chartHeight/2,label:e.value.isVisible,layout:e.alignment,onMouseEnter:l=>{if(e.tooltips.isEnabled&&e.tooltips.isVisible){let a=t[l.index];e.tooltipWrapper.style.display="block",e.tooltipWrapper.style.visibility="visible",e.tooltipWrapper.style.zIndex="1000";let i=e.hashService.getElementHash(e,a),o={analyticsChart:e,datum:a,index:a.index,hash:i},r=n.createElement(M.a,Object.assign({},o,{key:"analytics-chart-tooltip-"+o.hash}));return z.render(r,e.tooltipWrapper)}return null},onMouseLeave:()=>{e.tooltips.isEnabled&&e.tooltips.isVisible&&(e.tooltipWrapper.style.display="none",e.tooltipWrapper.style.visibility="hidden")}},n.createElement(v.a,{fill:p.toString(),stroke:"none",position:e.value.position,angle:b,dataKey:u,content:l=>{let a=t[l.index];return e.value.isVisible&&e.chartMode!==x.a.Stacked?e.expressionResolverService.resolve(e,{expression:"{{$value}}"},i,a,a.index,e.analyticsDataFactory.format).expression:(e.value.isVisible&&(e.chartMode,x.a.Stacked),null)}}),t.map((t,a)=>{let r,h=e.colors.getColorScheme(i);if(h===A.a.Grouped)r=e.state.colorScale(e.colors.mappedObject.getMapping(e,t,a)).toString();else if(h===A.a.Threshold){let l=e.colors.getScaleDomainIndexByCondition(e,t,a);r=e.state.colorScale(l)}else r=s[t.rootIndex];let g=o.a(r);if(null===g)throw new D.a("For some peculiar reason, cellFillColor is null!");g=e.colors.brightenColor(g,e.colors.k*(1+l));let u=e.hashService.getElementHash(e,t);return n.createElement(f.a,{id:"analytics-pie-chart-sector-"+u,className:"analytics-element analytics-element-"+u,style:(()=>({cursor:e.drilling.getCursorStyle(e,t,a),opacity:e.elements.opacity}))(),key:"analytics-pie-chart-sector-"+u,stroke:null!==e.elements.strokes.color?e.elements.strokes.color.toString():"#000000",strokeWidth:e.elements.strokes.isVisible?e.elements.strokes.width:0,fill:g.toString(),angle:b,fontFamily:c,fontSize:d,color:p.toString(),onClick:e.onEventArcClick.bind(e,t,a),onMouseEnter:()=>{e.colors.getColorScheme(e.state.currentLevel)!==A.a.Threshold&&e.colors.darkenColorOfElementByDatum(e,t)},onMouseLeave:()=>{e.colors.getColorScheme(e.state.currentLevel)!==A.a.Threshold&&e.colors.brightenColorOfElementByDatum(e,t)}})}))},l=(a,i,o,r,n)=>{let s=[],c=(r+=360*a.accumulatedPercentage)+360*i[i.length-1].accumulatedPercentage+360*i[i.length-1].percentage;return s.push(t(i,o,r,c,n)),s.push(i.map((t,a)=>{if(t.hasAggregations(e)&&e.chartMode===x.a.Stacked&&t.level<e.state.currentLevel+e.shownLevels){let a=t.getAggregations(e);return l(t,a,o+1,r,n)}return null})),s},a=()=>{let e;return e=l(r,s,0,0,d),e=c()(e),e=p()(e),g()(e)};const i=e.state.currentLevel;let r=e.analyticsDataFactory.createData(e,e.state.data.getAggregations(e));const s=r.getAggregations(e);let d=s.map((t,l)=>e.state.colorScale(l).toString());const h=e.caption.mapping.getMapping(i),u=e.value.mapping.getMapping(i);let m=e.legend.getLegendPayload(e),R=e.getToolBar(),U=a();return n.createElement("div",null,n.createElement(S.a,{key:"analytics-chart-container-"+e.id,id:"analytics-chart-container-"+e.id,className:"analytics-chart-container",width:"100%",height:e.chartContainerHeight},n.createElement(b.a,{id:"analytics-pie-chart-"+e.id,className:"analytics-chart analytics-pie-chart analytics-chart-"+e.id,width:e.chartWidth,height:e.chartHeight,margin:{left:e.margin.left+e.padding.left,right:e.margin.right+e.padding.right,top:e.margin.top+e.padding.top,bottom:e.margin.bottom+e.padding.bottom},style:(()=>({backgroundColor:e.background.color}))(),throttleDelay:1e3*e.transition.delay},n.createElement("text",{key:"analytics-chart-title-"+e.id,id:"analytics-"+e.id+"-chart-title",className:"analytics-chart-title",style:{color:null!==e.titles.font.color?e.titles.font.color.toString():"#000000",fontFamily:e.titles.font.family,fontSize:e.titles.font.size,visibility:e.titles.isVisible?"visible":"hidden"},transform:e.getTitleTransformation(),textAnchor:e.titles.textAnchor},e.titles.getTitle(e)),e.tooltips.isEnabled&&e.tooltips.isVisible?n.createElement(C.a,{key:"analytics-chart-tooltip-"+e.id,id:"analytics-chart-tooltip-"+e.id,className:"analytics-chart-tooltip",content:()=>null,cursor:!1}):null,e.legend.isEnabled&&e.legend.isVisible?n.createElement(k.a,{key:"analytics-chart-legend-"+e.id,className:"analytics-chart-legend",align:e.legend.horizontalAlignment,iconSize:e.legend.swatchSize,iconType:e.legend.swatchType,payload:m,content:t=>{let l={analyticsChart:e,legendProps:t};return n.createElement(P.a,Object.assign({},l))}}):null,U)),n.createElement("div",{id:"analytics-chart-toolbar-"+e.id},n.createElement(E.FormControlled,{config:R,payload:{},onChange:()=>{},onEvent:e.onEvent})))}return null}componentDidMount(){let e=this;window.addEventListener("resize",e.resizeChart.bind(e,e.id));let t=e.userService.getUser(e),l=e.dataSourceService.retrieveDataFromResources(e);Promise.all([t,l]).then(t=>{let l=t[0],a=t[1];if(!E._.equals(l,e.state.user)||!E._.equals(U.a.purgeRecursions(a),U.a.purgeRecursions(e.state.data))){e.setState({user:l,data:a,shouldUpdate:!1});let t=e.colors.getColorScale(e),i=e.colors.getThresholdsInPercentages(e);Promise.all([t,i]).then(t=>{let l=t[0],a=t[1];e.setState({colorScale:l,thresholdInPercentages:a,shouldUpdate:!0})})}})}componentDidUpdate(e,t){let l=this,a=l.dataSourceService.retrieveDataFromResources(l);Promise.all([a]).then(e=>{let t=e[0];if(!E._.equals(U.a.purgeRecursions(t),U.a.purgeRecursions(l.state.data))){l.setState({data:t,shouldUpdate:!1});let e=l.colors.getColorScale(l);Promise.all([e]).then(e=>{let t=e[0];l.setState({colorScale:t,shouldUpdate:!1},()=>{l.props.formControlled.onChange({$path:R.a.ShouldUpdate,$value:!1})})})}})}drillingCallback(e){let t=this,l=t.colors.getColorScale(t);Promise.all([l]).then(l=>{let a=l[0];t.setState({colorScale:a,shouldUpdate:!(t.drilling.drillDown.selectOnly||t.drilling.drillUp.selectOnly)},()=>{let l=U.a.purgeRecursions(e.data),a=U.a.purgeRecursions(e.selection),i=[];void 0===t.drilling.drillDown.selectOnly||t.drilling.drillDown.selectOnly||(i=e.drillings.map(e=>U.a.purgeRecursions(e))),t.props.formControlled.onChange({$path:R.a.Data,$value:l}),t.props.formControlled.onChange({$path:R.a.Drillings,$value:i}),t.props.formControlled.onChange({$path:R.a.Selection,$value:a}),t.props.formControlled.onChange({$path:R.a.CurrentLevel,$value:e.currentLevel}),t.props.formControlled.onChange({$path:R.a.ShouldUpdate,$value:!0})})})}onEventArcClick(e,t){let l=this;l.drilling.isDrillDownPossible(l,e,t)?l.drilling.drillDown.doDrillDown(l,e,e=>{e.shouldUpdate=!1,l.setState(e,l.drillingCallback.bind(l,e))}):l.drilling.isDrillAcrossPossible(l,e,t)&&l.drilling.drillAcross.doDrillAcross(l,e,t)}};$=i.a([Object(r.injectable)(),i.b("design:paramtypes",["function"==typeof(a=void 0!==O.PieChartComponentProps&&O.PieChartComponentProps)?a:Object])],$),t.default=$},655:function(e,t){}}]);