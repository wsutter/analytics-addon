# flowable analytics addon 

This is the flowable analytics addon Project. It consists of the following modules:

* flowable-addon-analytics (Addon)
* flowable-addon-analytics-distribution (Packaged addon)
* flowable-addon-analytics-storybook (Demo)
* flowable-addon-analytics-palettes (Palettes)
* flowable-addon-analytics-documentation (Documentation)
        
# Development

You need to know about:

* Typescript ~2.8.0
* React ~16.3.0
* D3.js ~5.4.0
* Lodash (single methods used only)
* Bluebird ^3.5.0
* Sass

Also, it might come in handy if you know about:

* Webpack ^3.12.0

Please refer to the ''flowable analytics addon User Guide'', ''flowable analytics addon Modeller Guide'' and ''flowable analytics addon Developer Guide'' section in the documentation if you would like to futher develop the Analytics Addon. It is vitally important to go through the documentation prior to making any changes!

To run a live development environment, please execute `yarn run start` from the folder `flowable-addon-analytics-storybook/src/main/js/com/flowable/addons/storybook`. This will start 
both, the storybook on a webpack-dev-server as well as the mock-server.

# Deployment

### Requirements
* **edoras-npm-repo**: Access to edoras-npm-repo and follow the instructions under ''Set me up''
* **Maven**: Install Maven > 3.3.x locally
* **Yarn**: Install Yarn > 1.7.0 locally
* **Nodejs**: Install Node.js >= 8.11.x locally

### Build
Simply run `mvn clean install` in the root folder. This will install all modules as JAR / WAR packages 
in your local Maven repository. If you add the profile `make-frontend-build`, Maven will trigger yarn which builds 
the frontend sources and integrates it into the resulting WAR file.

Careful when building with IntelliJ:
Since IntelliJ constantly updates its index even during a Maven build, you are strongly advised to configure IntelliJ 
does not automatically detect generated-source files! You can find such an option in the settings of IntelliJ if you 
search for Maven.

But why not simply use the terminal... ;)

### Run
You can either add the resulting JAR artifacts of the modules `flowable-addon-analytics-distribution` and `flowable-addon-analytics-palettes` 
as dependencies to your edoras 2.x project or flowable platform / work project. The latter is only needed if you want to 
be able to create form models which integrate analytics widgets using either the edoras or flowable modeller. 

Furthermore, you can deploy the WAR artifact of the module `flowable-addon-analytics-storybook` to your favourite web 
container and start it. If you do that, make sure to also start the mock-server which is shipped along in the same WAR artifact, 
available as tgz in our repository (see @flowable/addon-analytics-storybook-mock-server) or of course straight out of the 
source code at your disposal.

More detailed information about both tools can be found in the documentation.

Want to help?
-------------

Want to file a bug, contribute some code, or improve documentation? Excellent!

## Bugs
Please file any bugs or new requirements in our [Addon board](https://jira.edorasware.com/projects/ADDON).

## Merge requests
In case you fix a bug or implement new requirements, please file a pull request in [Bitbucket](https://stash.edorasware.com/projects/SOLUTIONS/repos/edoras-addon-analytics-maven-wrapper/pull-requests) and add Walter Sutter as a reviewer.

## Contact
Feel free to contact [Walter Sutter](mailto:walter.sutter@flowable.com).
